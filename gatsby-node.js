const path = require('path')
const { spawn } = require('child_process')

exports.onPostBuild = (pages) => {
  const assetDirs = ['assets', 'uploads', 'images', 'Document-Library', 'Image-Library']
  for (const dir of assetDirs) {
    // const dirPath = path.resolve('..', dir)
    const dirPath = path.resolve(dir)
    spawn('cp', ['-a', dirPath, 'public'])
  }
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /scrollmagic/,
            use: loaders.null()
          },
          {
            test: /scrollmagic-plugin-gsap/,
            use: loaders.null()
          }
        ]
      }
    })
  }
}
