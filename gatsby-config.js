const { getMarkdownToHtmlPlugin } = require('./helpers')
const collections = [
  { name: '_pages' },
  { name: '_pages-default' },
  { name: '_bios' }
]

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Beta | PGA.org`,
    siteUrl: `https://beta.pga.org`,
    description: `Beta description`,
    author: 'PGA of America'
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-catch-links`,
    `gatsby-transformer-remark`,
    `gatsby-transformer-yaml`,
    `gatsby-transformer-json`,
    {
      resolve: '@uptimeventures/gatsby-source-rss',
      options: {
        feeds: ['https://feeds.blubrry.com/feeds/fairwaytales.xml']
      }
    },
    // {
    //   resolve: `gatsby-plugin-intercom`,
    //   options: {
    //     appId: 'akw9jgf3'
    //   }
    // },
    // {
    //   resolve: `gatsby-plugin-google-tagmanager`,
    //   options: {
    //     id: 'GTM-NDJBH8L',

    //     // Include GTM in development.
    //     // Defaults to false meaning GTM will only be loaded in production.
    //     includeInDevelopment: true
    //     // Specify optional GTM environment details.
    //     // gtmAuth: 'YOUR_GOOGLE_TAGMANAGER_ENVIROMENT_AUTH_STRING',
    //     // gtmPreview: 'YOUR_GOOGLE_TAGMANAGER_ENVIROMENT_PREVIEW_NAME',
    //     // dataLayerName: 'YOUR_DATA_LAYER_NAME'
    //   }
    // },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'data',
        path: `${__dirname}/_data`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    ...getMarkdownToHtmlPlugin(collections),
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Playfair Display', 'Montserrat']
        }
      }
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {}
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png` // This path is relative to the root of the site.
      }
    }
  ]
}
