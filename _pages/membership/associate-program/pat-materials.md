---
title: PAT Materials
sidebar_label: PAT Materials
layout: default
order_number: 30
---
<h3 id="host-professionals-and-examiners">Host Professionals and Examiners</h3>

<p>Click on the links below to access information and forms you will need to conduct your Playing Ability Test.  Please return your Examiner Reporting Form and PAT Worksheet to Jacob Arnold at the National office.  You may fax your forms to (561) 624-8439 or email them to:  <a href="mailto:jarnold@pgahq.com?subject=PAT%20Materials">jarnold@pgahq.com</a>. Scorecards should be sent to your Section Office.<br> </p>

<p><strong>Changes Effective 1/29/2019:</strong></p>

<p><a href="/Document-Library/pat-hard-card.pdf">PAT Hard Card</a></p>

<p><a href="/Document-Library/pat-rules-regulations.pdf">Playing Ability Test Rules and Regulations Manual</a></p>

<p><br><strong>Changes Effective 7/1/2017:</strong></p>

<p><a href="/Document-Library/pat-yardage-memo-senior.pdf">Playing Ability Test Yardage Memo</a></p>

<p><br><strong>PAT Forms:</strong></p>

<p><a href="/Document-Library/pat-reporting-form.pdf">PAT Reporting Form</a></p>

<p><a href="/Document-Library/pat-test-site-info-form.pdf">PAT Test Site Information Form</a></p>

<p><strong>Policies:</strong></p>

<p><a href="/Document-Library/pat-information.pdf">Playing Ability Test Information</a></p>

<p><a href="/Document-Library/pat-rules-policies-guidelines-pace-of-play.pdf">Rules, Policies, &amp; Guidelines Governing Pace of Play</a></p>

<p><strong>Need Help?</strong></p>

<p>If you have any questions or are unable to access any of the files, please contact: Jacob Arnold, PGA - (561) 624-8406;  <a href="mailto:jarnold@pgahq.com">jarnold@pgahq.com</a>.</p>