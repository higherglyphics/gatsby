---
title: The PGM Associate Program
layout: default
order_number: 1
sidebar_label: Associate Program
permalink: /membership/associate-program/
---
<p><strong>Take the steps to become a PGA Member and build your career in golf.</strong></p>

<p><img src="/uploads/path-to-membership/pgm-associate.jpg" alt="" class="img-fluid"></p>

<p>To apply for membership with the PGA, you must first complete the PGM Associate Program by meeting the milestones listed below.</p>

<h3 id="background-check">Background Check</h3>

<p>All individuals registering or re-registering as an associate must complete a Background Check. Employment Screening Associates (ESA) performs this service at a reasonable cost, paid by the prospective member during an online screening session.</p>

<p><em>(If you have been convicted of a felony, misdemeanor or equivalent, you should submit documentation to the PGA Membership Department to determine your eligibility for PGA Membership prior to proceeding. Per the PGA Code of Ethics, certain transgressions are cause for permanent preclusion to PGA Membership.)</em></p>

<p><a href="https://www.emplscreen.com/pga.asp" class="btn btn-outline-info" target="_blank">Get Started</a></p>

<h3 id="qualifying-test">Qualifying Test</h3>

<p>Applicants need to purchase and review the five qualifying level courses: Introduction to the PGM and the Golf Profession, PGA History, PGA Constitution, Rules of Golf and Career Enhancement to be eligible to register for the qualifying test at the PGA-Partnered test company with local test centers throughout the country.</p>

<p><a href="https://apps.pga.org/pgapgm30/index.cfm" class="btn btn-outline-info" target="_blank">Register Now</a></p>

<h3 id="player-ability-test-pat">Player Ability Test (PAT)</h3>

<p>One of the following must be completed within eight (8) years prior to registering into the PGA Professional Golf Management Program:</p>

<ul>
  <li>Pass the 36-hole Playing Ability Test</li>
  <li>Attempt the PAT at least once within the eight (8) years prior to registering into the PGA PGM Program.</li>
</ul>

<p>Within that time frame, shoot one 18-hole score in a PAT that is equal to or less than the PAT target score for 18- holes, plus 5 strokes. Note: Each PAT score has a validity date of eight years.</p>

<p>The 36-hole PAT remains valid during the Acceptable Progress period. In order to pass the 36-hole PAT, you must achieve a 36-hole score within 15 shots of the course rating. For example, if the course rating is 72, the target score for the 36 holes would be 159 (72 x 2 - 144 +15 - 159).</p>

<p><img src="/uploads/table-image.jpg" alt="" class="img-fluid"></p>

<p>This competition is normally conducted in one day. Fewer than 20% of those taking the test achieve a passing score, therefore, it is highly recommended that you work diligently on your game prior to registering for the PAT. To register by phone, please contact PGA Membership Services at 1-800-474-2776.</p>

<p><a href="/membership/associate-program/pat-policies-and-procedures/" class="btn btn-outline-info">View PAT Policies &amp; Procedures</a> <a href="http://apps.pga.org/patinfo/patsearch.cfm" class="btn btn-outline-info" target="_blank">Schedule Your PAT</a></p>

<h3 id="eligible-employment">Eligible Employment</h3>

<p>To be eligible to register as an associate, individuals must be employed in an eligible position, within one of the approved classifications. While many traditional golf positions may be top of mind, there are many non-traditional jobs that are also eligible positions including things like tournament director, sales &amp; marketing, hospitality and even real estate. NOTE: Participation in an amateur event will forfeit all work experience credits earned prior to the event.</p>

<p><a href="/membership/associate-program/associate-registration-eligibility/" class="btn btn-outline-info" target="_blank">See Eligible Positions</a> <a href="http://jobs.pga.org" class="btn btn-outline-info" target="_blank">Find a Job</a></p>

<h3 id="associated-costs">Associated Costs</h3>

<p><strong>Fees</strong> - must be current on all fees and costs associated with the application process. We also have a fee calculator available for your use.</p>

<p><strong>Portal Access to Online Course - $200</strong></p>

<p>Valid for 6 months – If the Qualifying Test is not passed within 6 months of the purchase date a renewal fee of $200 will be required.</p>

<p><strong>Qualifying Level Test Fee at Test Centers - $40</strong></p>

<p>A passing score is valid for 12 months – Failure to register as an associate within that time period will invalidate the test and require re-purchase of the online access and re-test.</p>

<p><a href="/Document-Library/associated-costs-3.0.pdf" class="btn btn-outline-info" target="_blank">See All Fees and Costs</a> <a href="http://jobfinder.pga.org/helpwanted/empcenter/pgaandyou/getdues.cfm" class="btn btn-outline-info" target="_blank">View Fee Calculator</a></p>

<h3 id="apply-to-the-pgm-associate-program">Apply to the PGM Associate Program</h3>

<p>An associate registration form verifying employment must be signed and submitted by you before you are admitted to the PGA PGM 3.0 Associate Program.</p>

<p><a href="/Document-Library/form-202-associate-app.pdf">Download the Registration Form</a></p>

<h2 id="pga-pgm-30-associate-program-overview">PGA PGM 3.0 Associate Program Overview</h2>

<p>To be elected to membership once admitted to the PGA PGM 3.0 Associate Program, Associates will have to meet the appropriate milestones and requirements:</p>

<p><strong>Acceptable Progress</strong></p>

<ul>
  <li>
<strong>Level 1</strong> must be successfully completed within two years of the Level 1 Start Date</li>
  <li>
<strong>Level 2</strong> must be successfully completed within two years of the Level 2 Start Date</li>
  <li>
<strong>Level 3</strong> and election to PGA Membership must be successfully accomplished within eight years of the Level 1 Start Date</li>
</ul>

<p>Associates have eight years from their Level 1 Start Date to graduate from the PGA PGM Program, meet all election requirements and become elected to PGA Membership. Failure to complete Level 1 or 2 within two years of the Level’s start date will result in suspension. Failure to complete Level 1 or 2 within four years of the Level’s start date will result in termination from the PGA PGM Program.</p>

<p><strong>Education</strong> - must have a high school diploma or be at least 18 years of age and have the equivalent of a high school education. Please contact one of the following companies to submit your diploma, certificate or degree if your education was completed in a country other than the United States of America:</p>

<table>
  <tbody>
    <tr>
      <td>
<strong><a href="http://www.jsilny.com/" target="_blank">Josef Silny &amp; Associates, Inc.</a></strong><br>7101 SW 102 Avenue<br>Miami, FL 33173<br>Phone: (305) 273-1616<br>Fax: (305) 273-1338<br>Translation Fax: (305) 273-1984</td>
      <td>
<strong><a href="http://www.wes.org/" target="_blank">World Education Services, Inc.</a></strong><br>Attention: Documentation Center<br>P.O. Box 5087<br>Bowling Green Station<br>New York, NY 10274-5087<br>Phone: (212) 966-6311<br>Fax: (212) 739-6100</td>
    </tr>
  </tbody>
</table>

<p><strong>Citizenship</strong> - An individual must be either a U.S. Citizen, resident alien or temporary resident alien to be elected to PGA Membership.</p>

<p><strong>Employment</strong> - A total of 30 work experience credits must be earned in addition to fulfilling the PGA PGM Program. (One credit per month while eligibly employed full time.) 12 credits are awarded for a four year college degree and six credits for a two year college degree.</p>

<p><strong>Amateur Status: Work experience earned prior to participating in an amateur event will be forfeited.</strong></p>

<p><strong>*Note</strong>: All applicants are required to read and write in English to successfully complete the PGA Professional Golf Management Program. *</p>

<p>If you have any questions concerning this information or requirements to become a PGA Member, call PGA Membership Services at (800) 474-2776.</p>