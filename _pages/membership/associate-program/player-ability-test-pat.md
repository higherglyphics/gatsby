---
title: Player Ability Test (PAT)
sidebar_label: Player Ability Test (PAT)
layout: default
permalink:
---
<p>You must complete one of the following within eight (8) years prior to registering into the PGA Professional Golf Management Program:</p>

<ul>
  <li>Pass the 36-hole Playing Ability Test</li>
  <li>Attempt the PAT at least once within the eight (8) years prior to registering into the PGA PGM Program.</li>
</ul>

<p>Within that time frame, shoot one 18-hole score in a PAT that is equal to or less than the PAT target score for 18- holes, plus 5 strokes. Note: Each PAT score has a validity date of eight years.</p>

<p>The 36-hole PAT remains valid during the Acceptable Progress period. In order to pass the 36-hole PAT, you must achieve a 36-hole score within 15 shots of the course rating. For example, if the course rating is 72, the target score for the 36 holes would be 159 (72 x 2 - 144 +15 - 159).</p>

<p><img src="/uploads/table-image.jpg" alt="" class="img-fluid"></p>

<p>This competition is normally conducted in one day. Fewer than 20% of those taking the test achieve a passing score, therefore, it is highly recommended that you work diligently on your game prior to registering for the PAT.</p>

<p><a href="http://apps.pga.org/patinfo/patsearch.cfm" target="_blank">Click here</a> to schedule your PAT, or call PGA Membership Services at 1-800-474-2776.</p>