---
title: Associate Registration Eligibility
sidebar_label: Associate Registration Eligibility
layout: default
order_number: 30
---
<p>Registering as an associate is your first step to achieving your career goals in golf. In order to be a eligible to register as an associate, you must be employed in one of the following classifications:</p>

<table>
  <tbody>
    <tr>
      <td>Classification</td>
      <td>Description</td>
    </tr>
    <tr>
      <td>Member: A-1<br>Associate: B-1</td>
      <td>
<strong>Head Professional at a PGA Recognized Golf Course</strong><br><br>The term “Head Golf Professional” shall refer to an individual whose primary employment is: the ownership and operation of a golf shop at a PGA Recognized Golf Facility; or the supervision and direction of the golf shop and supervision of teaching at a “PGA Recognized Golf Facility.”</td>
    </tr>
    <tr>
      <td>Member: A-2<br>Associate: B-2</td>
      <td>
<strong>Head Professional at a PGA Recognized Golf Range</strong><br><br>The term “Head Golf Professional” shall refer to an individual whose primary employment is: the ownership and operation of a golf shop at a PGA Recognized Golf Facility; or the supervision and direction of the golf shop and supervision of teaching at a “PGA Recognized Golf Facility.”</td>
    </tr>
    <tr>
      <td>Member: A-3<br>Associate: Not Applicable</td>
      <td><strong>Exempt PGA Tour, Champions Tour, Nationwide Tour, LPGA Tour and Futures Tour players</strong></td>
    </tr>
    <tr>
      <td>Member: A-4<br>Associate: B-4</td>
      <td>
<strong>Director of Golf at PGA Recognized Golf Facilities</strong><br><br>The term “Director of Golf” shall refer to an individual who directs the total golf operation of a PGA Recognized Golf Facility, including the golf shop, golf range, golf car operations (if applicable) and supervision of the Head Golf Professional.</td>
    </tr>
    <tr>
      <td>Member: A-5<br>Associate: Not Applicable</td>
      <td><strong>Past Presidents of the Association</strong></td>
    </tr>
    <tr>
      <td>Member: A-6<br>Associate: B-6</td>
      <td>
<strong>Golf Instructor at a PGA Recognized Facility</strong><br><br>Individuals employed at PGA Recognized Golf Facilities, PGA Recognized Golf Schools, PGA Recognized Indoor Facilities as either golf instructors, supervisors of golf instructors or individuals who instruct PGA Professionals How to Teach.</td>
    </tr>
    <tr>
      <td>Member: A-7<br>Associate: B-7</td>
      <td>
<strong>Head Professional at a PGA Recognized Facility Under Construction</strong><br><br>Individuals employed as Directors of Golf or Head Golf Professionals at PGA Recognized Golf Facilities under construction.</td>
    </tr>
    <tr>
      <td>Member: A-8<br>Associate: B-8</td>
      <td>
<strong>Assistant Golf Professional at a PGA Recognized Facility</strong><br><br>The term “Assistant Golf Professional” shall refer to an individual who is primarily employed at a PGA Recognized Golf Facility and spends at least 50% of the time working on club repair, merchandising, handicapping records, inventory control, bookkeeping and tournament.</td>
    </tr>
    <tr>
      <td>Member: A-9<br>Associate: B-9</td>
      <td>
<strong>Employed in Professional Positions in Management, Development, Ownership Operation and/or Financing of Facilities</strong><br><br>Individuals who are employed in professional positions in management, development, ownership, operation and/or financing of facilities. (Employment at more than two facilities: Individuals who are involved in the management of more than two facilities, regardless of positions, titles or responsibilities shall be classified A-9 or B-9.)</td>
    </tr>
    <tr>
      <td>Member: A-10<br>Associate: B-10</td>
      <td>
<strong>Golf Clinician</strong><br><br>The term “Golf Clinician” shall refer to an individual whose main source of income is golf shows or clinics.</td>
    </tr>
    <tr>
      <td>Member: A-11<br>Associate: B-11</td>
      <td>
<strong>Golf Administrator</strong><br><br>Individuals who are employed by the Association, a Section or the PGA Tour in an administrative capacity and individuals who are employed full-time as employees of golf associations recognized by the Board of Directors.</td>
    </tr>
    <tr>
      <td>Member: A-12<br>Associate: B-12</td>
      <td>
<strong>College or University Golf Coach</strong><br><br>Individuals who are employed as golf coaches at accredited colleges, universities and junior colleges.</td>
    </tr>
    <tr>
      <td>Member: A-13<br>Associate: B-13</td>
      <td>
<strong>General Manager</strong><br><br>Individuals who are employed as General Managers/Directors of Club Operations who have successfully completed the requirements set forth by the PGA Board of Directors. (General Managers/Directors of Club Operations shall manage the entire golf facility including golf operations, golf course maintenance, club house administration, food and beverage operation and other recreational activities at the facility.)</td>
    </tr>
    <tr>
      <td>Member: A-14<br>Associate: B-14</td>
      <td>
<strong>Director of Instruction at a PGA Recognized Facility</strong><br><br>The term “Director of Instruction” shall refer to an individual who is managing, supervising and directing the total teaching program at a PGA Recognized Golf School or PGA Recognized Facility.</td>
    </tr>
    <tr>
      <td>Member: A-15<br>Associate: B-15</td>
      <td>
<strong>Ownership or Management of a Retail Golf Facility</strong><br><br>Individuals whose primary employment is ownership or management of golf products or services at a “PGA Recognized Retail Facility” provided such employment specifically excludes primary employment as a clerk.</td>
    </tr>
    <tr>
      <td>Member: A-16<br>Associate: B-16</td>
      <td>
<strong>Golf Course Architect</strong><br><br>Individuals who are primarily employed in the design of golf courses as architects or individuals who are primarily employed in an ownership or management capacity as golf course builders.</td>
    </tr>
    <tr>
      <td>Member: A-17<br>Associate: B-17</td>
      <td>
<strong>Golf Course Superintendent</strong><br><br>Individuals primarily employed in the management of all activities in relation to maintenance, operation and management of a golf course. Individuals in this classification are required to satisfy the criteria of either a Golf Course Superintendent or Assistant Golf Course Superintendent as defined by the Golf Course Superintendent’s Association of America.</td>
    </tr>
    <tr>
      <td>Member: A-18<br>Associate: B-18</td>
      <td>
<strong>Golf Media</strong><br><br>Individuals primarily employed in the reporting, editing, writing or publishing of golf-related publications in any form of media (inclusive of, but not necessarily limited to, newspapers, magazines, the Internet) or in the broadcasting or commentating about golf events on network television, cable networks, the Internet or any other form of related media.</td>
    </tr>
    <tr>
      <td>Member: A-19<br>Associate: B-19</td>
      <td>
<strong>Golf Manufacturer Management</strong><br><br>Individuals primarily employed in an executive, administrative or supervisory position with a golf industry manufacturer or golf industry distributor.</td>
    </tr>
    <tr>
      <td>Member: A-20<br>Associate: B-20</td>
      <td>
<strong>Golf Manufacturer Sales Representative</strong><br><br>Individuals primarily employed by one or more golf manufacturing or distributing companies involved in the wholesale sales and distribution of golf merchandise or golf-related supplies to golf facilities, retail stores or any other golf outlets.</td>
    </tr>
    <tr>
      <td>Member: A-21<br>Associate: B-21</td>
      <td>
<strong>Tournament Coordinator/Director for Organizations, Businesses or Associations</strong><br><br>Individuals primarily employed in the coordination, planning and implementation of golf events for organizations, businesses, or associations.</td>
    </tr>
    <tr>
      <td>Member: A-22<br>Associate: B-22</td>
      <td>
<strong>Rules Official</strong><br><br>Individuals primarily employed in the provision of services as a rules official for recognized golf associations, recognized golf tours or recognized golf events.</td>
    </tr>
    <tr>
      <td>Member: A-23<br>Associate: B-23</td>
      <td>
<strong>Club Fitting/Club Repair</strong><br><br>Individuals primarily employed in the business of club fitting must use a recognized fitting system or a comparable system, must have all the necessary equipment normally associated with club fitting and must have access to a PGA Recognized Golf Range or a range at a PGA Recognized Golf Course to monitor ball flight. Individuals primarily employed in club repair must have an established place of business with all necessary equipment normally associated with club repair, or must service one or more golf tours or series of golf events.</td>
    </tr>
  </tbody>
</table>