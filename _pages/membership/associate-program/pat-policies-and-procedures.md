---
title: PAT Policies and Procedures
sidebar_label: PAT Policies and Procedures
layout: default
order_number: 20
---
<h3 id="registrations">Registrations</h3>

<ul>
  <li>Registrations are taken in order of receipt at the PGA of America National Office and <a href="http://apps.pga.org/patinfo/patsearch.cfm">online</a>. There is a 14-day registration deadline for every PAT and an individual may be registered for only two PATs at any one time. The registration fee, paid at the time of registration, is $100.00 for each PAT and the registrant is responsible for all additional on-site fees.</li>
  <li>For schedule information or to register with an American Express, MasterCard or Visa, <a href="http://apps.pga.org/patinfo/patsearch.cfm">click here</a>, or call Membership Services at (800) 474-2776.</li>
</ul>

<h3 id="equipment">Equipment</h3>

<p>All individuals must use USGA Conforming Ball and Driver Head Lists which are continuously updated. Please see below for the most current list.</p>

<ul>
  <li><a href="http://www.usga.org/ConformingGolfBall/gball_list.pdf" target="_blank">Approved Golf Ball List </a></li>
  <li>
<a href="http://www.usga.org/ConformingGolfClub/conforming_golf_club.asp" target="_blank">Approved Driver Head List</a><br> </li>
</ul>

<h3 id="waiting-list-policy">Waiting List Policy</h3>

<ul>
  <li>If a PAT becomes full before the deadline date, a wait list will be established. At the registration deadline, a wait list report including the wait list registrants in order of registration is sent to the Section Office. To determine your wait list status, you may contact the National Office prior to the deadline and the Section Office after the deadline. If there are any late cancellations or no shows the day of play, the Section will fill any remaining spots from the wait list, in wait list order, from those who present themselves on site. If a spot does not become available off the wait list, an individual should contact the National Office at (800) 474-2776 to have the fees transferred to another available PAT or have funds refunded.</li>
</ul>

<h3 id="transfers-refunds-and-cancellations">Transfers, Refunds, and Cancellations</h3>

<ul>
  <li>There are no transfers or refunds from a PAT inside the 14-day registration deadline unless it is a cancellation after passing a previous 36-hole PAT (this does not include meeting the 18 hole minimum PAT requirement) or off a wait list. An individual who passes the 36-hole PAT and is signed up for another, must contact the National Office at (800) 474-2776, within three business days of passing the 36-hole PAT to have the funds refunded. A competitor, who is unable to participate in a PAT and does not notify the National Office and Section Office at least 72 hours prior to the PAT, will be suspended from playing in another PAT for 90 days.</li>
  <li>A competitor who withdraws during play or no cards (does not complete and submit a card for two 18 hole rounds) will be suspended from competing in another PAT for 90 days from the date of that PAT. If that competitor is signed up for another PAT within the suspension period, that individual will be canceled from that PAT and the funds will be refunded.</li>
  <li>An individual, who cancels, withdraws or no cards due to injury or illness may submit medical documentation to the National Office no later than three business days following the PAT for review for refund, and/or waiver of suspension.</li>
  <li>If a PAT is canceled for not meeting the minimum registrations, you will be notified by the National Office. If a PAT is canceled due to weather, you should contact the Section Office.</li>
</ul>

<h3 id="dress-code">Dress Code</h3>

<ul>
  <li>Female participants must wear slacks, culottes, walking shorts or golf skirts, which constitute acceptable clothing worn by women in connection with participation in professional golf tournaments.</li>
  <li>Male participants must wear slacks and participants shall not wear shorts anywhere on club property. Jeans are not to be considered slacks.</li>
</ul>

<h3 id="the-competition">The Competition</h3>

<ul>
  <li>In order for the competition to take place, the course must have a minimum USGA course rating 68.0 for both men and women. Men under age 50 will play from a minimum of 6,350 yards to a maximum of 6,700 yards, while Women under age 50 will play from a minimum of 5,400 yards to a maximum of 5,700 yards. Men age 50 and over will play from 94% of the Men’s under age 50 yardage and Women age 50 and over will play from 94% of the Women’s under age 50 yardage, while retaining the Men’s and Women’s under age 50 target score.</li>
  <li>Target score is determined by multiplying the USGA course rating by two (2) and adding fifteen strokes. (After multiplying, any remaining fraction is dropped.)</li>
  <li>“Lift, clean and place” cannot be invoked during a PAT. If conditions do not warrant playing the ball down, the PAT must be canceled and should be rescheduled. If a PAT is played under “lift, clean and place,” that PAT will be invalidated and all scores will be null and void.</li>
</ul>

<h3 id="golf-car">Golf Car</h3>

<ul>
  <li>Contestants’ clubs must be transported on a golf car; no more than two cars per grouping. Only players may ride in a golf car.</li>
  <li>Only players may ride in a golf car and no more than two players are allowed to ride in the golf car at one time.</li>
  <li>When one golf car is employed by two players, the golf car and any person or thing in it is always deemed to be the equipment of the player whose ball is involved, except when the golf car is being moved by one of the players employing it, the golf car and its contents are considered the equipment of that player.</li>
  <li>Spectator golf cars are not allowed. Only official golf cars authorized by the PAT Examiner will be allowed on the course.</li>
  <li>It is imperative that all players return their golf cars to the golf car staging area immediately upon completion of their round. Do not leave them at the various courses or their respective practice areas or parking lot.</li>
</ul>

<h3 id="other-policies">Other Policies</h3>

<ul>
  <li>To enter into the PGA PGM Program, the registrant must attempt the PAT at least once within eight years prior to registration. Within that time frame, shoot one 18-hole score in a PAT that is equal to or less than the PAT target score for 18-holes, plus 5 strokes. Each PAT score has a validity date of eight years. Any registrant, who enters the PGA PGM Program without successfully passing the PAT, must pass the 36-hole PAT prior to registering for Level 3 testing. The 36-hole PAT remains valid during the Acceptable Progress period. <strong><a href="/membership/associate-program/">Click here</a></strong> for more information on the PGA PGM Program.</li>
  <li>The PGA reserves the right to preclude an individual from participating in a PAT based on prior PAT scores that were unreasonably above the target score.</li>
  <li>There must be at least ten registrants in a PAT at the deadline in order for the PAT to be held unless approved by both the National Office and Section Office.</li>
  <li>Tee times are sent by the Section prior to the PAT. If you do not receive this listing, you should contact the Section Office.</li>
  <li>A photo identification is required at the PAT site, lack thereof is cause for disqualification.</li>
  <li>If allowed by both the Section and golf course, caddies are permitted.</li>
  <li>Devices that measure distance are permitted. If the device measures other factors they must be disabled.</li>
  <li>The use of alcohol, legalized marijuana and any illegal substance is prohibited during play of the PAT.</li>
  <li>For conduct unbecoming a golf professional during a PAT, the registrant may be assessed a two stroke penalty. A second offense will result in disqualification. PAT participants are advised that actions determined by the PGA of America to be construed as conduct unbecoming a golf professional in any way connected with the PAT (without limitation unprofessional behavior displayed or unprofessional actions of any nature or description committed during practice rounds, the PAT test, or committed subsequent to the completion of a PAT) may result in the suspension of such an individual from participating in PATs for a period up to two (2) years at the sole discretion of the PGA of America.</li>
  <li>To preserve the integrity of the game and our PGA competitions, please be reminded that each player has the duty and responsibility to protect the rest of the field from questionable practices and Rules infractions. If you are doubtful as to the rights or procedure for either yourself or a fellow competitor, seek the counsel of the PAT Examiner.</li>
  <li>Scores from the first two competition rounds of a collegiate golf tournament (Division I, II, III, NAIA, and Junior College) may be used to satisfy the 36-hole PGA Playing Ability Test requirement.  Playing Ability Test validity would extend eight years past the date of passing.  Applicant would be responsible for submitting documentation of scores, yardage and course rating to verify Playing Ability Test eligibility, and all PGA minimum standards for the Playing Ability Test must be met. <a href="/Document-Library/pat-collegiate-event.pdf" target="_blank"><strong>Click here</strong></a> to send us your scores.</li>
  <li>The PAT schedule and deadlines are subject to revision. For an <strong><a href="http://apps.pga.org/patinfo/patsearch.cfm" target="_blank">updated schedule visit PGA.org</a></strong>.  All deadlines are based on Eastern Time. For any questions regarding PAT policies, contact the Membership Services Department at (800) 474-2776.</li>
</ul>