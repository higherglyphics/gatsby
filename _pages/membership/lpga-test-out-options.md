---
title: LPGA Test-Out Options
sidebar_label: LPGA Test-Out Options
layout: default
order_number: 10
permalink: /membership/lpga-test-out-options/
---
<p class="present-before-paste">The following informational flyers provide an overview of the Reciprocity Test-Out options available for either <strong>current or former LPGA Class A Tour Player members</strong> or <strong>current LPGA Class A Teaching and Club Professional division members</strong>. This option is not available for reciprocation of LPGA Class B or LPGA apprentice member requirements (such individuals will be required to complete the PGA Professional Golf Management Program in its entirety, including the PAT).</p>

<p class="present-before-paste">Each applicant will be evaluated on the merits of her membership status with the LPGA to determine if she qualifies for and will be approved to work toward membership in the PGA of America.</p>

<p class="present-before-paste">Should you be interested in this opportunity, please complete the History Sheet that is a part of the flyer below and return it to the PGA along with a copy of the front and back of your current LPGA membership card.</p>

<ul>
  <li>
<a href="/Document-Library/lpga_current_classa_teachingclubpro.pdf" target="_blank" class="present-before-paste">Current LPGA Class A Teaching/Club Professional Members</a> are offered the opportunity to utilize the reciprocity process of testing out of the pre-Qualifying Level and Level 1, 2 and 3 curriculum provided they have successfully passed the PGA’s Playing Ability Test.</li>
  <li>
<a href="/Document-Library/lpga_current_former_classa_tourplayer.pdf" target="_blank" class="present-before-paste">Current or Former LPGA Class A Tour Player Members</a> will be given the opportunity to utilize the reciprocity process of testing out of the Pre-Qualifying Level and Level 1, 2 and 3 curriculum.</li>
</ul>