---
title: How to Become a PGA Member
secondary_title: Become a PGA Member
layout: default-jumbo
permalink: /membership/
order_number: 1
sidebar_label: Pathways to Membership
---
<div class="membership-pathways-landing-wpr">
    <div class="jumbotron jumbotron-fluid jumbotron-custom-md editable">
      <p><img class="jumbotron-image" data-src="/uploads/path-to-membership/hero-membership.jpg" alt="" src="/uploads/path-to-membership/hero-membership.jpg"></p>
      <div class="jumbotron-body">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-9 col-sm-12">
              <h1>How to Become a PGA Member</h1>
              <p>There are two primary pathways that lead to PGA Membership:<br>the PGA Professional Golf Management Program and <br>the PGA Golf Management University Program</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container membership-pathways-wpr">
        <div class="row my-4">
          <div class="col-md-6 mem-cta-wpr">
            <div class="tiles-wrapper tiles-stretch">
              <div class="tiles">
<img data-src="/uploads/path-to-membership/pgm-associate.jpg" class="img-fluid w-100 cta-image" alt="" src="/uploads/path-to-membership/pgm-associate.jpg" width="800" height="528" data-cms-original-src="/uploads/path-to-membership/pgm-associate.jpg" data-cms-original-data-src="/uploads/path-to-membership/pgm-associate.jpg">
                <div class="tiles-body">
                  <h3>The PGM Associate Program <i class="icon icon-arrow-right"></i>
</h3>
</div>
              </div>
              <div class="tiles-footer">
                <p class="mb-0">To become a PGA Member, you'll need to become a registered Associate before completing the PGA Professional Golf Management (PGA PGM) Program–an award-winning educational program designed for aspiring PGA Professionals that focuses on the People, the Business and the Game.</p>
            </div>
            <a class="tiles-link pointer" href="/membership/associate-program/"></a>
            </div>
          </div>
          <div class="col-md-6 mem-cta-wpr">
            <div class="tiles-wrapper tiles-stretch">
              <div class="tiles">
<img data-src="/uploads/path-to-membership/pgm-university.jpg" class="img-fluid w-100 cta-image" alt="" src="/uploads/path-to-membership/pgm-university.jpg" width="800" height="528" data-cms-original-src="/uploads/path-to-membership/pgm-university.jpg" data-cms-original-data-src="/uploads/path-to-membership/pgm-university.jpg">
                <div class="tiles-body">
                  <h3>The University Program <i class="icon icon-arrow-right"></i>
</h3>
</div>
              </div>
              <div class="tiles-footer">
                <p class="mb-0">The PGA Golf Management University Program, accredited by The Professional Golfers’ Association of America (PGA), is a college degree program designed to attract and educate bright, highly motivated men and women to service all aspects of the industry and produce PGA Members.</p>
            </div>
            <a class="tiles-link pointer" href="/membership/university-program/"></a>
            </div>
          </div>
        </div>
    <p><br>If you are a current or former LPGA Member, you may be eligible for the test-out option. <a href="/membership/lpga-test-out-options/" class="text-info">Read more here</a>.</p>    
    </div>
</div>