---
title: PGA Associate Acceptable Progress Tracking
sidebar_label: Acceptable Progress Tracking
layout: default
permalink:
---
<p>Are you on the right path?</p>

<p>Acceptable Progress in the PGA PGM Program is defined by the successful completion of each Level and matriculation into the subsequent Level. Please read the Acceptable Progress Policy before testing the progress of any apprentice.</p>

<ul>
  <li>Level 1 must be successfully completed within two years of the Level 1 Start Date</li>
  <li>Level 2 must be successfully completed within two years of the Level 2 Start Date</li>
  <li>Level 3 and election to PGA Membership must be successfully accomplished within eight years of the Level 1 Start Date</li>
</ul>

<p>Individuals who have not successfully completed the Level at the end of their two-year suspension will be terminated from the PGA PGM Program. Associates have eight years from their Level 1 Start Date to graduate from the PGA PGM Program, meet all election requirements and become elected to PGA Membership.</p>