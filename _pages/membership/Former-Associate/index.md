---
title: Former Associate? Are You Ready to Re-Register?
sidebar_label: Re-Register
layout: default
permalink: /membership/Former-Associate/
---
<h4 id="are-you-ready-to-re-register-and-continue-the-process-to-become-a-pga-member">Are you ready to re-register and continue the process to become a PGA Member?</h4>

<p><br>Here’s how to begin:</p>

<ol>
  <li>Review the <a href="https://beta.pga.org/membership/Former-Associate/pga-associate-acceptable-progress-tracking/" target="_blank">Acceptable Progress Policy</a>.  </li>
  <li>Check your current status by <a href="http://jobfinder.pga.org/helpwanted/empcenter/pgaandyou/kitlookup.cfm" target="_blank">entering your former associate number or social security number and last name here</a>.</li>
  <li>Fill out the application and necessary forms below:<br>- <a href="/Document-Library/form-202-associate-app.pdf">Associate Application</a><br>- <a href="/Document-Library/form-300-associate-emp-verify.pdf">Associate Change Form</a><br>- <a href="http://pdf.pgalinks.com/professionals/education/pgm/wekits/Associated-Costs-3.0.pdf">Associated Costs PGA PGM</a> (Program 3.0)<br>- <a href="/Document-Library/form-302-membership-application.pdf">Application for Membership</a><br>- <a href="http://apps.pga.org/fees/">Calculate the prorated fees here</a>
</li>
  <li>Contact Membership Services at (800) 474-2776 for any assistance.</li>
</ol>