---
title: The PGA Golf Management University Program
layout: default
order_number: 1
sidebar_label: University Program
permalink: /membership/university-program/
---
<p class="present-before-paste"><strong>Earn a degree while becoming an expert in the game and business of golf</strong></p>

<p><img src="/uploads/path-to-membership/content-pgm-university.jpg" alt="" class="img-fluid"></p>

<p class="present-before-paste">The PGA Golf Management University Program provides extensive classroom courses, internship experience and opportunities for player development in a four-and-one-half to five year program for aspiring PGA Professionals. Offered at 18 universities nationwide, the University Program allows students to earn a degree in areas such as Marketing, Business Administration, Hospitality Administration, Recreation and Park Management, providing them with the knowledge and skills to succeed in the golf industry.</p>

<ul>
  <li>Program is accredited by the PGA of America</li>
  <li>Students earn a bachelor’s degree in a golf industry compatible major</li>
  <li>Upon program completion, students can apply for PGA Membership</li>
  <li>Graduates currently have a 100% employment placement rate</li>
</ul>

<p> </p>

<div class="embed-responsive"><iframe src="https://player.vimeo.com/video/136400718?title=0&amp;byline=0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" class="embed-responsive-item" width="640" height="360" frameborder="0"></iframe></div>

<p class="present-before-paste">Each participating university has specific entrance requirements. Contact the university’s dedicated PGA Golf Management University Program Director with questions about specific university curriculum, entrance requirements and financial information.</p>

<p class="present-before-paste"><a href="/membership/university-program/participating-universities/">View Participating Universities</a></p>

<p class="present-before-paste"> </p>