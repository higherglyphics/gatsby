---
title: Participating Universities
layout: default
sidebar_label: Participating Universities
universities:
  - title: Campbell University (1999)
    url: 'https://business.campbell.edu/undergraduate-programs/pga-golf-management/'
    image: /uploads/campbelllogo.jpg
    director: 'Mr. Ken Jones, PGA, Director'
    address1: P.O. Box 218
    address2: Lundy-Fetterman Bldg.
    address3: 165 Dr. McKoy Road
    address4: 'Buies Creek, NC 27506'
    phone1: (910) 893-1395
    phone2: (800) 334-4111 ext. 1395
    phone3: (910) 814-4798 - fax
    email: jonesk@campbell.edu
    video_link: 'https://player.vimeo.com/video/311229752'
  - title: Clemson University (2001)
    url: >-
      http://www.clemson.edu/cbshs/departments/prtm/academics/undergraduate-degrees/pga-golf-management/index.html
    image: /uploads/clemsonlogo.jpg
    director: 'Mr. Rick Lucas, PGA, Director'
    address1: 263 Lehotsky Hall
    address2: 'Clemson, SC 29634-0701'
    phone1: (864) 656-0112
    phone2: (864) 656-2226 - fax
    email: rlucas@clemson.edu
    video_link: 'https://player.vimeo.com/video/311232026'
  - title: Coastal Carolina University (1999)
    url: 'http://www.coastal.edu/business/pga/'
    image: /uploads/cculogo.jpg
    director: 'Mr. Will Mann, PGA, Director'
    address1: 'E. Craig Wall, Sr. School of Business'
    address2: P.O. Box 261954
    address3: 'Conway, SC 29528-6054'
    phone1: (843) 349-6632
    phone2: (843) 349-2881 - fax
    email: wmann1@coastal.edu
    video_link: 'https://player.vimeo.com/video/311228883'
  - title: Eastern Kentucky University (2006)
    url: 'http://www.pgm.eku.edu/'
    image: /uploads/ekulogo.jpg
    director: 'Ms. Kim Kincer, Director College of Business and Technology'
    director2: 'Dept. of Management, Marketing, and Administrative Communication'
    address1: 'Richmond, KY 40475'
    phone1: (859) 622 4976
    email: kim.kincer@eku.edu
    video_link: 'https://player.vimeo.com/video/310339709'
  - title: Ferris State University (1975)
    url: 'http://ferris.edu/business/program/pga-golf-management/'
    image: /uploads/ferrislogo.jpg
    director: 'Mr. Aaron Waltz, PGA, Director'
    address1: 1506 Knollview Drive
    address2: 'Big Rapids, MI 49307-2290'
    phone1: (231) 591-2380
    phone2: (231) 591-2839 - fax
    email: AaronWaltz@ferris.edu
    video_link: 'https://player.vimeo.com/video/310342402'
  - title: Florida Gulf Coast University (2005)
    url: 'http://www.fgcu.edu/CoB/pgmbs/'
    image: /uploads/fgculogo.jpg
    director: 'Ms. Tara McKenna, PGA, Director'
    address1: 10501 FGCU Blvd South
    address2: Sugden Building Room 238 C
    address3: 'Ft. Myers, FL 33965'
    phone1: (239) 590-7717
    email: tmckenna@fgcu.edu
    video_link: 'https://player.vimeo.com/video/310339020'
  - title: Methodist University (1999)
    url: 'http://www.methodist.edu/academics/programs/pgm'
    image: /uploads/methodistlogo.jpg
    director: 'Mr. Jerry Hogge, PGA, Director'
    address1: 5400 Ramsey Street
    address2: 'Fayetteville, NC 28311-1420'
    phone1: (910) 630-7144
    phone2: (910) 630-7254 - fax
    email: Jhogge@methodist.edu
    video_link:
  - title: Mississippi State University (1985)
    url: 'https://www.pgagmu.msstate.edu'
    image: /uploads/msulogo.jpg
    director: 'Mr. Jeff Adkerson, PGA, Director'
    address1: P.O. Box 6217
    address2: 309 McCool Hall
    address3: 'Mississippi State, MS 39762-5513'
    phone1: (662) 325-1990
    phone2: (662) 325-1779 - fax
    email: jadkerson@business.msstate.edu
    video_link: 'https://player.vimeo.com/video/310341659'
  - title: New Mexico State University (1987)
    url: 'http://business.nmsu.edu/academics/undergraduate/golf/'
    image: /uploads/nmsulogo.jpg
    director: 'Mr. Pat Gavin, PGA, Director'
    address1: P.O. Box 30001/Dept. PGM
    address2: 'University Ave., Business Complex'
    address3: 'Las Cruces, NM 88003-8001'
    phone1: (575) 646-7686
    phone2: (575) 646-1467 - fax
    email: Pgavin@nmsu.edu
    video_link: 'https://player.vimeo.com/video/310348128'
  - title: North Carolina State University (2002)
    url: 'http://cnr.ncsu.edu/prtm/current-undergraduates/ug-programs/pgm/'
    image: /uploads/ncstatelogo.jpg
    director: 'Dr. Robb Wade, PGA, Director'
    department1: 'Department of Parks, Recreation & Tourism Management'
    department2: PGA Golf Management University Program
    address1: Campus Box 8004
    address2: 4023 Biltmore Hall
    address3: 'Raleigh, NC 27695-8004'
    phone1: (919) 515-8792
    phone2: (919) 513-7219 - fax
    email: robb_wade@ncsu.edu
    video_link: 'https://player.vimeo.com/video/312808723'
  - title: Penn State University (1990)
    url: 'http://www.hhd.psu.edu/rptm/pgm'
    image: /uploads/penn-state-2c-dec2015-300x112.jpg
    director: 'Dr. Burch Wilkes, Director'
    address1: 801 Ford Building
    address2: 'University Park, PA 16802'
    phone1: (814) 863-8987
    phone2: (814) 863-8992 - fax
    email: gbw104@psu.edu
    video_link: 'https://player.vimeo.com/video/306084618'
  - title: Sam Houston State University (2005)
    url: 'http://www.shsu.edu/pga/'
    image: /uploads/shsulogo.jpg
    director: 'Dr.. Richard Ballinger, PGA, Director'
    address1: Box 2056
    address2: 'Huntsville, TX 77341-2056'
    phone1: (936) 294-4810
    phone2: (936) 294-3612 - fax
    email: rmb002@shsu.edu
    video_link: 'https://player.vimeo.com/video/310347531'
  - title: University of Central Oklahoma (2008)
    url: 'http://business.uco.edu/degrees/undergraduate-degrees/pga-golf-management/'
    image: /uploads/ucologo.jpg
    director: 'Ms. Lindsey Cook, PGA'
    deraptment1: University of Central Oklahoma
    deraptment2: College of Business
    address1: '100 N. University Dr., Box 115'
    address2: 'Edmond, OK 73034'
    phone1: (405) 974-5247
    phone2: (405) 974-3821 - fax
    email: Lcook6@uco.edu
    video_link: 'https://player.vimeo.com/video/311227113'
  - title: University of Colorado Colorado Springs (2003)
    url: 'https://www.uccs.edu/business/programs/bachelors/pga-golf-management'
    image: /uploads/uccs-150.jpg
    director: 'Mr. Mark Bacheldor, PGA'
    director2: 'Director, PGA Golf Management'
    address1: 313 Dwire Hall
    address2: 'Colorado Springs, CO 80918'
    phone1: (719) 255-4100
    phone2: (719)-255-3244 - fax
    email: mbacheld@uccs.edu
    video_link: 'https://player.vimeo.com/video/310342966'
  - title: University of Idaho (2002)
    url: 'http://www.uidaho.edu/pga'
    image: /uploads/uidahologo.jpg
    director: 'Mr. Cole Mize, PGA, Director'
    deraptment1: PGA Golf Management University Program
    deraptment2: College of Business and Economics
    address1: P.O. Box 443161
    address2: 'Moscow, ID 83844-3161'
    phone1: (208) 885-9773
    phone2: (208) 885-8939 - fax
    email: colem@uidaho.edu
    video_link: 'https://player.vimeo.com/video/310343888'
  - title: University of Maryland Eastern Shore (2008)
    url: 'http://www.umes.edu/pgm/Index.aspx?id=11994'
    image: /uploads/umeslogo.jpg
    director: 'Mr. Billy Dillon, PGA, Director'
    deraptment1: PGA Golf Management University Program
    address1: 'Kiah Hall, 2100'
    address2: 'Princess Anne, MD 21853'
    phone1: (410) 651-7790
    phone2: (410) 651-8163 - fax
    email: wcdillon@umes.edu
    video_link: 'https://player.vimeo.com/video/310346166'
  - title: 'University of Nebraska, Lincoln (2004)'
    url: 'http://pgm.unl.edu/'
    image: /uploads/unebraskalogo.jpg
    director: 'Dr. Dann Husmann, Director'
    deraptment1: PGA Golf Management University Program
    deraptment2: University of Nebraska-Lincoln
    address1: 203H Keim Hall
    address2: 'Lincoln, NE 68583-0953'
    phone1: 402-472-PGMP (7467)
    phone2: (402) 472-4104 - fax
    email: pgm@unl.edu
    video_link: 'https://player.vimeo.com/video/310340722'
  - title: University of Nevada-Las Vegas (2002)
    url: 'http://www.unlv.edu/pga'
    image: /uploads/unlvlogo.jpg
    director: 'Dr. Chris Cain, PGA, Director'
    director2: William F. Harrah  College of Hospitality
    address1: 4505 Maryland Parkway Box 6021
    address2: 'Las Vegas, NV 89154-6021'
    phone1: (702)-895-3865
    phone2: (702)-774-8994 - fax
    email: Christopher.Cain@unlv.edu
    video_link: 'https://player.vimeo.com/video/311228108'
---
<p class="present-before-paste"><strong>Earn a degree while becoming an “Expert in the Game and Business of Golf”</strong></p>

<p><img src="/uploads/path-to-membership/content-pgm-university.jpg" alt="" class="img-fluid"></p>

<p class="present-before-paste">Each of the participating universities has specific entrance requirements. To find out more about a specific program, please contact the PGA Golf Management University Program Director listed below.</p>

<section><div class="container"><div class="row justify-content-center">
<div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/campbelllogo.jpg" alt="Campbell University (1999)" width="110" class="mr-3" src="/uploads/campbelllogo.jpg"><h4 class="title-serif"><a href="https://business.campbell.edu/undergraduate-programs/pga-golf-management/">Campbell University (1999)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Ken Jones, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:jonesk@campbell.edu">jonesk@campbell.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(910) 893-1395</li>
<li>(800) 334-4111 ext. 1395</li>
<li>(910) 814-4798 - fax</li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>P.O. Box 218</li>
<li>Lundy-Fetterman Bldg.</li>
<li>165 Dr. McKoy Road</li>
<li>Buies Creek, NC 27506</li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#campbell-university-1999"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="campbell-university-1999" tabindex="-1" role="dialog" aria-labelledby="campbell-university-1999" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/311229752"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="https://business.campbell.edu/undergraduate-programs/pga-golf-management/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/clemsonlogo.jpg" alt="Clemson University (2001)" width="110" class="mr-3" src="/uploads/clemsonlogo.jpg"><h4 class="title-serif"><a href="http://www.clemson.edu/cbshs/departments/prtm/academics/undergraduate-degrees/pga-golf-management/index.html">Clemson University (2001)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Rick Lucas, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:rlucas@clemson.edu">rlucas@clemson.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(864) 656-0112</li>
<li>(864) 656-2226 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>263 Lehotsky Hall</li>
<li>Clemson, SC 29634-0701</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#clemson-university-2001"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="clemson-university-2001" tabindex="-1" role="dialog" aria-labelledby="clemson-university-2001" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/311232026"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.clemson.edu/cbshs/departments/prtm/academics/undergraduate-degrees/pga-golf-management/index.html"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/cculogo.jpg" alt="Coastal Carolina University (1999)" width="110" class="mr-3" src="/uploads/cculogo.jpg"><h4 class="title-serif"><a href="http://www.coastal.edu/business/pga/">Coastal Carolina University (1999)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Will Mann, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:wmann1@coastal.edu">wmann1@coastal.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(843) 349-6632</li>
<li>(843) 349-2881 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>E. Craig Wall, Sr. School of Business</li>
<li>P.O. Box 261954</li>
<li>Conway, SC 29528-6054</li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#coastal-carolina-university-1999"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="coastal-carolina-university-1999" tabindex="-1" role="dialog" aria-labelledby="coastal-carolina-university-1999" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/311228883"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.coastal.edu/business/pga/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/ekulogo.jpg" alt="Eastern Kentucky University (2006)" width="110" class="mr-3" src="/uploads/ekulogo.jpg"><h4 class="title-serif"><a href="http://www.pgm.eku.edu/">Eastern Kentucky University (2006)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Ms. Kim Kincer, Director College of Business and Technology</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:kim.kincer@eku.edu">kim.kincer@eku.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(859) 622 4976</li>
<li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>Richmond, KY 40475</li>
<li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#eastern-kentucky-university-2006"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="eastern-kentucky-university-2006" tabindex="-1" role="dialog" aria-labelledby="eastern-kentucky-university-2006" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310339709"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.pgm.eku.edu/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/ferrislogo.jpg" alt="Ferris State University (1975)" width="110" class="mr-3" src="/uploads/ferrislogo.jpg"><h4 class="title-serif"><a href="http://ferris.edu/business/program/pga-golf-management/">Ferris State University (1975)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Aaron Waltz, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:AaronWaltz@ferris.edu">AaronWaltz@ferris.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(231) 591-2380</li>
<li>(231) 591-2839 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>1506 Knollview Drive</li>
<li>Big Rapids, MI 49307-2290</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#ferris-state-university-1975"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="ferris-state-university-1975" tabindex="-1" role="dialog" aria-labelledby="ferris-state-university-1975" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310342402"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://ferris.edu/business/program/pga-golf-management/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/fgculogo.jpg" alt="Florida Gulf Coast University (2005)" width="110" class="mr-3" src="/uploads/fgculogo.jpg"><h4 class="title-serif"><a href="http://www.fgcu.edu/CoB/pgmbs/">Florida Gulf Coast University (2005)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Ms. Tara McKenna, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:tmckenna@fgcu.edu">tmckenna@fgcu.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(239) 590-7717</li>
<li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>10501 FGCU Blvd South</li>
<li>Sugden Building Room 238 C</li>
<li>Ft. Myers, FL 33965</li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#florida-gulf-coast-university-2005"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="florida-gulf-coast-university-2005" tabindex="-1" role="dialog" aria-labelledby="florida-gulf-coast-university-2005" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310339020"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.fgcu.edu/CoB/pgmbs/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/methodistlogo.jpg" alt="Methodist University (1999)" width="110" class="mr-3" src="/uploads/methodistlogo.jpg"><h4 class="title-serif"><a href="http://www.methodist.edu/academics/programs/pgm">Methodist University (1999)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Jerry Hogge, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:Jhogge@methodist.edu">Jhogge@methodist.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(910) 630-7144</li>
<li>(910) 630-7254 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>5400 Ramsey Street</li>
<li>Fayetteville, NC 28311-1420</li>
<li>
<li>
</ul>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.methodist.edu/academics/programs/pgm"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/msulogo.jpg" alt="Mississippi State University (1985)" width="110" class="mr-3" src="/uploads/msulogo.jpg"><h4 class="title-serif"><a href="https://www.pgagmu.msstate.edu">Mississippi State University (1985)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Jeff Adkerson, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:jadkerson@business.msstate.edu">jadkerson@business.msstate.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(662) 325-1990</li>
<li>(662) 325-1779 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>P.O. Box 6217</li>
<li>309 McCool Hall</li>
<li>Mississippi State, MS 39762-5513</li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#mississippi-state-university-1985"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="mississippi-state-university-1985" tabindex="-1" role="dialog" aria-labelledby="mississippi-state-university-1985" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310341659"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="https://www.pgagmu.msstate.edu"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/nmsulogo.jpg" alt="New Mexico State University (1987)" width="110" class="mr-3" src="/uploads/nmsulogo.jpg"><h4 class="title-serif"><a href="http://business.nmsu.edu/academics/undergraduate/golf/">New Mexico State University (1987)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Pat Gavin, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:Pgavin@nmsu.edu">Pgavin@nmsu.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(575) 646-7686</li>
<li>(575) 646-1467 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>P.O. Box 30001/Dept. PGM</li>
<li>University Ave., Business Complex</li>
<li>Las Cruces, NM 88003-8001</li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#new-mexico-state-university-1987"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="new-mexico-state-university-1987" tabindex="-1" role="dialog" aria-labelledby="new-mexico-state-university-1987" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310348128"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://business.nmsu.edu/academics/undergraduate/golf/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/ncstatelogo.jpg" alt="North Carolina State University (2002)" width="110" class="mr-3" src="/uploads/ncstatelogo.jpg"><h4 class="title-serif"><a href="http://cnr.ncsu.edu/prtm/current-undergraduates/ug-programs/pgm/">North Carolina State University (2002)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Dr. Robb Wade, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:robb_wade@ncsu.edu">robb_wade@ncsu.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(919) 515-8792</li>
<li>(919) 513-7219 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>Campus Box 8004</li>
<li>4023 Biltmore Hall</li>
<li>Raleigh, NC 27695-8004</li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#north-carolina-state-university-2002"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="north-carolina-state-university-2002" tabindex="-1" role="dialog" aria-labelledby="north-carolina-state-university-2002" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/312808723"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://cnr.ncsu.edu/prtm/current-undergraduates/ug-programs/pgm/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/penn-state-2c-dec2015-300x112.jpg" alt="Penn State University (1990)" width="110" class="mr-3" src="/uploads/penn-state-2c-dec2015-300x112.jpg"><h4 class="title-serif"><a href="http://www.hhd.psu.edu/rptm/pgm">Penn State University (1990)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Dr. Burch Wilkes, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:gbw104@psu.edu">gbw104@psu.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(814) 863-8987</li>
<li>(814) 863-8992 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>801 Ford Building</li>
<li>University Park, PA 16802</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#penn-state-university-1990"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="penn-state-university-1990" tabindex="-1" role="dialog" aria-labelledby="penn-state-university-1990" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/306084618"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.hhd.psu.edu/rptm/pgm"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/shsulogo.jpg" alt="Sam Houston State University (2005)" width="110" class="mr-3" src="/uploads/shsulogo.jpg"><h4 class="title-serif"><a href="http://www.shsu.edu/pga/">Sam Houston State University (2005)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Dr.. Richard Ballinger, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:rmb002@shsu.edu">rmb002@shsu.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(936) 294-4810</li>
<li>(936) 294-3612 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>Box 2056</li>
<li>Huntsville, TX 77341-2056</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#sam-houston-state-university-2005"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="sam-houston-state-university-2005" tabindex="-1" role="dialog" aria-labelledby="sam-houston-state-university-2005" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310347531"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.shsu.edu/pga/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/ucologo.jpg" alt="University of Central Oklahoma (2008)" width="110" class="mr-3" src="/uploads/ucologo.jpg"><h4 class="title-serif"><a href="http://business.uco.edu/degrees/undergraduate-degrees/pga-golf-management/">University of Central Oklahoma (2008)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Ms. Lindsey Cook, PGA</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:Lcook6@uco.edu">Lcook6@uco.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(405) 974-5247</li>
<li>(405) 974-3821 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>100 N. University Dr., Box 115</li>
<li>Edmond, OK 73034</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-central-oklahoma-2008"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-central-oklahoma-2008" tabindex="-1" role="dialog" aria-labelledby="university-of-central-oklahoma-2008" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/311227113"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://business.uco.edu/degrees/undergraduate-degrees/pga-golf-management/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/uccs-150.jpg" alt="University of Colorado Colorado Springs (2003)" width="110" class="mr-3" src="/uploads/uccs-150.jpg"><h4 class="title-serif"><a href="https://www.uccs.edu/business/programs/bachelors/pga-golf-management">University of Colorado Colorado Springs (2003)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Mark Bacheldor, PGA</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:mbacheld@uccs.edu">mbacheld@uccs.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(719) 255-4100</li>
<li>(719)-255-3244 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>313 Dwire Hall</li>
<li>Colorado Springs, CO 80918</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-colorado-colorado-springs-2003"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-colorado-colorado-springs-2003" tabindex="-1" role="dialog" aria-labelledby="university-of-colorado-colorado-springs-2003" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310342966"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="https://www.uccs.edu/business/programs/bachelors/pga-golf-management"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/uidahologo.jpg" alt="University of Idaho (2002)" width="110" class="mr-3" src="/uploads/uidahologo.jpg"><h4 class="title-serif"><a href="http://www.uidaho.edu/pga">University of Idaho (2002)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Cole Mize, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:colem@uidaho.edu">colem@uidaho.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(208) 885-9773</li>
<li>(208) 885-8939 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>P.O. Box 443161</li>
<li>Moscow, ID 83844-3161</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-idaho-2002"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-idaho-2002" tabindex="-1" role="dialog" aria-labelledby="university-of-idaho-2002" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310343888"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.uidaho.edu/pga"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/umeslogo.jpg" alt="University of Maryland Eastern Shore (2008)" width="110" class="mr-3" src="/uploads/umeslogo.jpg"><h4 class="title-serif"><a href="http://www.umes.edu/pgm/Index.aspx?id=11994">University of Maryland Eastern Shore (2008)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Mr. Billy Dillon, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:wcdillon@umes.edu">wcdillon@umes.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(410) 651-7790</li>
<li>(410) 651-8163 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>Kiah Hall, 2100</li>
<li>Princess Anne, MD 21853</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-maryland-eastern-shore-2008"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-maryland-eastern-shore-2008" tabindex="-1" role="dialog" aria-labelledby="university-of-maryland-eastern-shore-2008" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310346166"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.umes.edu/pgm/Index.aspx?id=11994"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/unebraskalogo.jpg" alt="University of Nebraska, Lincoln (2004)" width="110" class="mr-3" src="/uploads/unebraskalogo.jpg"><h4 class="title-serif"><a href="http://pgm.unl.edu/">University of Nebraska, Lincoln (2004)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Dr. Dann Husmann, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:pgm@unl.edu">pgm@unl.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>402-472-PGMP (7467)</li>
<li>(402) 472-4104 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>203H Keim Hall</li>
<li>Lincoln, NE 68583-0953</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-nebraska-lincoln-2004"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-nebraska-lincoln-2004" tabindex="-1" role="dialog" aria-labelledby="university-of-nebraska-lincoln-2004" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/310340722"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://pgm.unl.edu/"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> <div class="col-lg-6 col-md-6"><div class="card card-stretch"><div class="card-body d-flex flex-column justify-content-between">
<div class="d-flex align-items-center">
<img data-src="/uploads/unlvlogo.jpg" alt="University of Nevada-Las Vegas (2002)" width="110" class="mr-3" src="/uploads/unlvlogo.jpg"><h4 class="title-serif"><a href="http://www.unlv.edu/pga">University of Nevada-Las Vegas (2002)</a></h4>
</div>
<div class="card-info">
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-user.svg"></div>
<ul class="list-unstyled ml-3"><li>Dr. Chris Cain, PGA, Director</li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-envelop.svg"></div>
<ul class="list-unstyled ml-3"><li><a href="mailto:Christopher.Cain@unlv.edu">Christopher.Cain@unlv.edu</a></li></ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-phone.svg"></div>
<ul class="list-unstyled ml-3">
<li>(702)-895-3865</li>
<li>(702)-774-8994 - fax</li>
<li>
</ul>
</div>
<div class="card-line">
<div><img width="15" class="fill-blue" src="/assets/images/icons/icon-building.svg"></div>
<ul class="list-unstyled ml-3">
<li>4505 Maryland Parkway Box 6021</li>
<li>Las Vegas, NV 89154-6021</li>
<li>
<li>
</ul>
</div>
<a class="btn-link btn-block text-uppercase mt-3 pointer" data-toggle="modal" data-target="#university-of-nevada-las-vegas-2002"> Watch Video <i class="icon icon-arrow-right"></i> </a> 




<div class="modal fade modal__university-video" id="university-of-nevada-las-vegas-2002" tabindex="-1" role="dialog" aria-labelledby="university-of-nevada-las-vegas-2002" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="close fa fa-times fa-2x text-navy" data-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body">
        <iframe class="embed-responsive embed-responsive-16by9" frameborder="0" src="https://player.vimeo.com/video/311228108"></iframe>
      </div>
    </div>
  </div>
</div>
<a target="_blank" class="btn-link btn-block text-uppercase mt-3" href="http://www.unlv.edu/pga"> Read more <i class="icon icon-arrow-right"></i> </a>
</div>
</div></div></div> </div></div></section>