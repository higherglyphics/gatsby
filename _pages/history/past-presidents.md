---
title:  PGA of America Past Presidents
layout: default
order_number: 3
sidebar_label: Past Presidents
---
<div>
    <p>Since the founding of the PGA of America on April 10, 1916, a total of 40 individuals have served as President of the Association. Since 1969, the President of the PGA of America serves a two-year term.</p>
</div>

<div>
    <select class="form-control select-narrow" onchange="location=this.options[this.selectedIndex].value">
        <option>Jump to a Year</option>
        
        <option value="#year-2016-2018">2016-2018</option>
        
        <option value="#year-2014-2016">2014-2016</option>
        
        <option value="#year-2013-2014">2013-2014</option>
        
        <option value="#year-2011-2012">2011-2012</option>
        
        <option value="#year-2009-2010">2009-2010</option>
        
        <option value="#year-2007-2008">2007-2008</option>
        
        <option value="#year-2005-2006">2005-2006</option>
        
        <option value="#year-2003-2004">2003-2004</option>
        
        <option value="#year-2001-2002">2001-2002</option>
        
        <option value="#year-1999-2000">1999-2000</option>
        
        <option value="#year-1997-1998">1997-1998</option>
        
        <option value="#year-1995-1996">1995-1996</option>
        
        <option value="#year-1993-1994">1993-1994</option>
        
        <option value="#year-1991-1992">1991-1992</option>
        
        <option value="#year-1989-1990">1989-1990</option>
        
        <option value="#year-1987-1988">1987-1988</option>
        
        <option value="#year-1985-1986">1985-1986</option>
        
        <option value="#year-1983-1984">1983-1984</option>
        
        <option value="#year-1981-1982">1981-1982</option>
        
        <option value="#year-1979-1980">1979-1980</option>
        
        <option value="#year-1977-1978">1977-1978</option>
        
        <option value="#year-1975-1976">1975-1976</option>
        
        <option value="#year-1973-1974">1973-1974</option>
        
        <option value="#year-1971-1972">1971-1972</option>
        
        <option value="#year-1969-1970">1969-1970</option>
        
        <option value="#year-1966-1968">1966-1968</option>
        
        <option value="#year-1964-1965">1964-1965</option>
        
        <option value="#year-1961-1963">1961-1963</option>
        
        <option value="#year-1958-1960">1958-1960</option>
        
        <option value="#year-1955-1957">1955-1957</option>
        
        <option value="#year-1952-1954">1952-1954</option>
        
        <option value="#year-1949-1951">1949-1951</option>
        
        <option value="#year-1942-1948">1942-1948</option>
        
        <option value="#year-1940-1941">1940-1941</option>
        
        <option value="#year-1933-1939">1933-1939</option>
        
        <option value="#year-1931-1932">1931-1932</option>
        
        <option value="#year-1927-1930">1927-1930</option>
        
        <option value="#year-1921-1926">1921-1926</option>
        
        <option value="#year-1919-1920">1919-1920</option>
        
        <option value="#year-1916-1919">1916-1919</option>
        
    </select>
</div>

<hr class="history">

<div class="collapse-row" id="year-2016-2018">
    <div class="collapse-header">
        <div class="head">2016-2018</div>
        <div class="sub-head">Paul K. Levy - Southern California PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2016-2018" aria-expanded="true" aria-controls="collapseCopy2016-2018" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse show" id="collapseCopy2016-2018">Paul K. Levy is a former President of Club Operations and Development for Sunrise Company, where he served for 18 years, and oversaw properties in Nevada, California, Colorado and Texas. Levy also was the CEO and General Manager at Toscana Country Club in Indian Wells, California.<br><br>Born in New Orleans, Levy is a 1983 graduate of Louisiana State University, where he was a golf team member. He was elected to PGA Membership in 1986 and has served in a leadership capacity at both the Section and national levels</div>
</div>

<div class="collapse-row" id="year-2014-2016">
    <div class="collapse-header">
        <div class="head">2014-2016</div>
        <div class="sub-head">Derek Sprague - Northeastern New York PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2014-2016" aria-expanded="false" aria-controls="collapseCopy2014-2016" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2014-2016">Born in Malone, New York, Derek Sprague served for 27 years as the General Manager and Director of Golf at his hometown course, Malone Golf Club. In 2016, Sprague was named Managing Director of Liberty National Golf Club in Jersey City, New Jersey. In November 2017, Sprague became the PGA General Manager at TPC Sawgrass in Ponte Vedra Beach, Florida. Sprague is a 1988 graduate of James Madison University, and the first PGA President from the Northeastern New York PGA Section.</div>
</div>

<div class="collapse-row" id="year-2013-2014">
    <div class="collapse-header">
        <div class="head">2013-2014</div>
        <div class="sub-head">Ted Bishop - Indiana PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2013-2014" aria-expanded="false" aria-controls="collapseCopy2013-2014" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2013-2014">Born in Logansport, Indiana, Ted Bishop was a 1976 graduate of Purdue University with a Bachelor’s Degree in Agronomy. He began his career as golf professional and superintendent at the Phil Harris Golf Course in Linton, Indiana. He served at either the Section or National levels from 1989 to 2014.</div>
</div>

<div class="collapse-row" id="year-2011-2012">
    <div class="collapse-header">
        <div class="head">2011-2012</div>
        <div class="sub-head">Allen Wronowski - Middle Atlantic Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2011-2012" aria-expanded="false" aria-controls="collapseCopy2011-2012" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2011-2012">A native of Baltimore, Allen Wronowski was elected to PGA membership in 1981 and became active in Section governance, holding all offices and representing the Section at the national level. As a national officer, Wronowski demonstrated his passion for player development by supporting the core values of PGA Professionals and their impact upon junior golf, minorities and diversity.</div>
</div>

<div class="collapse-row" id="year-2009-2010">
    <div class="collapse-header">
        <div class="head">2009-2010</div>
        <div class="sub-head">Jim Remy - New England PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2009-2010" aria-expanded="false" aria-controls="collapseCopy2009-2010" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2009-2010">Born in Leominster, Massachusetts, Jim Remy was the first member of the New England PGA Section to be elected president of The PGA of America. He navigated rugged economic terrain over his two years as PGA President and oversaw prudent fiscal management of the PGA's budget to help the Association continue to deliver valuable benefits to PGA members. He was inducted into the New England PGA Hall of Fame in 2008.</div>
</div>

<div class="collapse-row" id="year-2007-2008">
    <div class="collapse-header">
        <div class="head">2007-2008</div>
        <div class="sub-head">Brian Whitcomb - Southwest PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2007-2008" aria-expanded="false" aria-controls="collapseCopy2007-2008" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2007-2008">Brian Whitcomb of Bend, Oregon, helped strengthen the bonds among PGA members while overseeing the launch of the new PGA branding position and logo. As President, Whitcomb also provided The PGA with the business knowledge and skills of a successful golf course owner. He led the extensive renovations to the 54 holes of championship golf at PGA Golf Club in Port St. Lucie, Fla., and heralded the launch of Patriot Golf Day.</div>
</div>

<div class="collapse-row" id="year-2005-2006">
    <div class="collapse-header">
        <div class="head">2005-2006</div>
        <div class="sub-head">Roger Warren - Carolinas PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2005-2006" aria-expanded="false" aria-controls="collapseCopy2005-2006" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2005-2006">A native of Galesburg, Ill., Roger Warren was a high school teacher for the Dundee Illinois School System from 1973-1986 and coached basketball and golf for 18 years. His background served as a cornerstone for his commitment to lifelong learning for PGA Professionals through both the Professional Golf Management and Certified Professional programs. Warren was a driving force behind the launch of PGA PerformanceTrak, which provides PGA Professionals with real-time industry data and information to further enhance their business acumen.</div>
</div>

<div class="collapse-row" id="year-2003-2004">
    <div class="collapse-header">
        <div class="head">2003-2004</div>
        <div class="sub-head">M.G. Orender - North Florida PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2003-2004" aria-expanded="false" aria-controls="collapseCopy2003-2004" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2003-2004">M.G. Orender's passion for promoting golf to tomorrow's players has been reflected through his years of service to The PGA of America and in taking a leadership role in launching Play Golf America, the showcase player development program of the Association. Orender's vision has resulted in helping many people to take up the game and others to return to it. Orender also brought his perspective as a multi-course operator to his years as a PGA Officer.</div>
</div>

<div class="collapse-row" id="year-2001-2002">
    <div class="collapse-header">
        <div class="head">2001-2002</div>
        <div class="sub-head">Jack Connelly - Philadelphia PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy2001-2002" aria-expanded="false" aria-controls="collapseCopy2001-2002" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy2001-2002">Jack Connelly was PGA president when the terrorist attacks of Sept. 21, 2001, took place, and he worked with the PGA of America's counterparts in Europe to move the 2001 Ryder Cup to the 2002 calendar year. He also oversaw the decision made by the PGA of America to make a considerable donation to relief efforts for those affected by the attacks. Connelly played on the PGA Tour in 1972, competed in the 1975 U.S. Open, qualified for the PGA Professional Championship nine times and was named Philadelphia PGA Section Player of the Year on four occasions.</div>
</div>

<div class="collapse-row" id="year-1999-2000">
    <div class="collapse-header">
        <div class="head">1999-2000</div>
        <div class="sub-head">Will Mann - Carolinas PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1999-2000" aria-expanded="false" aria-controls="collapseCopy1999-2000" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1999-2000">Graduating from North Carolina State University with a bachelor's degree in forestry, Will Mann began his career in golf as a course superintendent. As PGA president, he provided the Association with the leadership and business perspective of an experienced and successful director of golf, golf course superintendent and golf course owner. Highly active throughout his career, Mann was a member of the PGA's Policies and Procedures Committee, Membership Committee and Membership Steering Committee.</div>
</div>

<div class="collapse-row" id="year-1997-1998">
    <div class="collapse-header">
        <div class="head">1997-1998</div>
        <div class="sub-head">Ken Lindsay - Gulf States PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1997-1998" aria-expanded="false" aria-controls="collapseCopy1997-1998" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1997-1998">After his service in the U.S. Air Force, where he achieved the rank of Captain, Ken Lindsay served for more than 30 years at Colonial Country Club in Jackson, Mississippi, with roles that ranged from assistant professional to head professional to general manager to director of golf. The 1983 PGA Golf Professional of the Year and the 1987 Horton Smith Award recipient, Lindsay is acclaimed Rules of Golf official and a PGA Master Professional. He is a member of the University of Memphis, the Gulf States PGA Section and the Mississippi Sports Hall of Fame.</div>
</div>

<div class="collapse-row" id="year-1995-1996">
    <div class="collapse-header">
        <div class="head">1995-1996</div>
        <div class="sub-head">Tom Addis III - Southern California PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1995-1996" aria-expanded="false" aria-controls="collapseCopy1995-1996" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1995-1996">A PGA member since 1992, Tom Addis earned the 1989 PGA Golf Professional of the Year Award and the 1981 Horton Smith Award. The third member of the Southern California PGA Section to be elected PGA president, Addis was named his Section's Golf Professional of the Year twice, in 1979 and 1989. Addis' commitment to the improvement of PGA Professionals continued to serve as Executive Director/CEO of the Southern California PGA Section.</div>
</div>

<div class="collapse-row" id="year-1993-1994">
    <div class="collapse-header">
        <div class="head">1993-1994</div>
        <div class="sub-head">Gary Schaal - Carolinas PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1993-1994" aria-expanded="false" aria-controls="collapseCopy1993-1994" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1993-1994">The first PGA President from the Carolinas PGA Section, Gary Schaal has owned and operated golf facilities throughout South Carolina. He served as president of the Carolinas PGA Section from 1985-1986, was the Section's Golf Professional of the Year in 1985 and twice earned the Section's Horton Smith Award for outstanding contributions to PGA member education. Schaal chaired numerous national committees, playing an active role in the growth and expansion of the Association.</div>
</div>

<div class="collapse-row" id="year-1991-1992">
    <div class="collapse-header">
        <div class="head">1991-1992</div>
        <div class="sub-head">Dick Smith - Philadelphia PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1991-1992" aria-expanded="false" aria-controls="collapseCopy1991-1992" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1991-1992">A five-time Philadelphia PGA Section Champion, Dick Smith won more than 25 championships during his career. He captured the Section Player of the Year Award six times and served as Section president from 1978-1980. As a member of the PGA Board of Directors and a PGA Officer, he chaired numerous committees.</div>
</div>

<div class="collapse-row" id="year-1989-1990">
    <div class="collapse-header">
        <div class="head">1989-1990</div>
        <div class="sub-head">Patrick J. Rielly - Southern California PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1989-1990" aria-expanded="false" aria-controls="collapseCopy1989-1990" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1989-1990">A former lieutenant in the U.S. Marine Corps, Pat Rielly went on to captain the Penn State University golf team and receive his business degree. After moving to California, he served on the Southern California PGA Section Board of Directors for 11 consecutive years, and also became the Section's president. The Section honored Rielly three times as its Professional of the Year.</div>
</div>

<div class="collapse-row" id="year-1987-1988">
    <div class="collapse-header">
        <div class="head">1987-1988</div>
        <div class="sub-head">James Ray Carpenter - Gulf States PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1987-1988" aria-expanded="false" aria-controls="collapseCopy1987-1988" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1987-1988">Introduced to golf at age 33 by a business associate, J.R. Carpenter became hooked on the game. Eight years later, he was the golf coach, professional and superintendent at the University of Southern Mississippi Golf Club. Carpenter served as president of the Gulf States PGA Section in 1975 and chaired six national committees while on the PGA Board of Directors. As PGA President, he devoted himself to improving communications and club relations with PGA Professionals. He is a member of the Southern Mississippi Sports Hall of Fame.</div>
</div>

<div class="collapse-row" id="year-1985-1986">
    <div class="collapse-header">
        <div class="head">1985-1986</div>
        <div class="sub-head">Mickey Powell - Indiana PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1985-1986" aria-expanded="false" aria-controls="collapseCopy1985-1986" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1985-1986">A college golf star, Mickey Powell began his career as an assistant professional at the Country Club of Indianapolis in 1961, and was later the PGA head professional at Otter Creek Golf Course in Columbus, Indiana. Twice the president of the Indiana PGA Section and the 1972 Section Golf Professional of the Year, Powell served on numerous national committees before being elected PGA president.</div>
</div>

<div class="collapse-row" id="year-1983-1984">
    <div class="collapse-header">
        <div class="head">1983-1984</div>
        <div class="sub-head">Mark Kizziar - South Central PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1983-1984" aria-expanded="false" aria-controls="collapseCopy1983-1984" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1983-1984">Receiving a Bachelor of Arts degree from the University of Tulsa in 1961, Mark Kizziar began his golf career as an assistant professional at Rolling Hills Country Club, later becoming its PGA head professional. Kizziar was the head professional at Adams Municipal Golf Course in Bartlesville, Oklahoma, and later became president of the South Central PGA Section.member of the Metropolitan PGA Section, he also served as national PGA vice president from 1972-1974.</div>
</div>

<div class="collapse-row" id="year-1981-1982">
    <div class="collapse-header">
        <div class="head">1981-1982</div>
        <div class="sub-head">Joe Black - Northern Texas PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1981-1982" aria-expanded="false" aria-controls="collapseCopy1981-1982" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1981-1982">A Texas native, Joe Black played golf at Hardin Simmons University and competed on the PGA Tour before being appointed PGA Assistant Tournament Supervisor in 1958. He later became Tournament Supervisor, a post he held until 1964, when he was named PGA head professional at Brookhaven Country Club in Dallas. He served as president of the Northern Texas PGA Section in 1971 and 1972.</div>
</div>

<div class="collapse-row" id="year-1979-1980">
    <div class="collapse-header">
        <div class="head">1979-1980</div>
        <div class="sub-head">Frank Cardi - Metropolitan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1979-1980" aria-expanded="false" aria-controls="collapseCopy1979-1980" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1979-1980">A graduate of Ohio State University, where he won the Big Ten Golf Championship, Frank Cardi was PGA head professional at Rockaway Country Club in Cedarhurst, New York, from 1962-1973. He then became PGA head professional at Apawamis Country Club in Rye, New York. An active member of the Metropolitan PGA Section, he also served as national PGA vice president from 1972-1974.</div>
</div>

<div class="collapse-row" id="year-1977-1978">
    <div class="collapse-header">
        <div class="head">1977-1978</div>
        <div class="sub-head">Don Padgett - Indiana PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1977-1978" aria-expanded="false" aria-controls="collapseCopy1977-1978" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1977-1978">Don Padgett began his career as PGA head professional at the American Legion Golf Course in New Castle, Ind. Three years later, he became PGA head professional at Green Hill Country Club in Selma, Ind., where he served for 23 years. Padgett was a six-time president of the Indiana PGA Section and was the 1961 PGA Golf Professional of the Year. During his six years as a national officer, he served on the tournament policy board.</div>
</div>

<div class="collapse-row" id="year-1975-1976">
    <div class="collapse-header">
        <div class="head">1975-1976</div>
        <div class="sub-head">Henry Poe - Dixie PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1975-1976" aria-expanded="false" aria-controls="collapseCopy1975-1976" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1975-1976">A 1938 graduate of Duke University, Henry Poe was the PGA head professional at Reading (Pennsylvania) Country Club for 26 years and served as President of the Philadelphia PGA Section from 1957-1959. He became affiliated with Vanity Fair Mills in 1966 and was professional for two courses in southwest Alabama. During his PGA Presidency, Poe formed the Club Professional Relations Department, designed to handle employment matters for the Association.</div>
</div>

<div class="collapse-row" id="year-1973-1974">
    <div class="collapse-header">
        <div class="head">1973-1974</div>
        <div class="sub-head">William Clarke - Middle Atlantic PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1973-1974" aria-expanded="false" aria-controls="collapseCopy1973-1974" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1973-1974">William Clarke turned professional in 1946 and became head professional in Hillendale Country Club outside of Baltimore in 1954, where he remained for 35 years. Clarke was President of the Middle Atlantic PGA Section from 1959-1962, the Section's Golf Professional of the Year in 1960 and the 1968 national Horton Smith Award recipient. Clarke served on the PGA Championship Rules Committee for 30 years.</div>
</div>

<div class="collapse-row" id="year-1971-1972">
    <div class="collapse-header">
        <div class="head">1971-1972</div>
        <div class="sub-head">Warren Orlick - Michigan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1971-1972" aria-expanded="false" aria-controls="collapseCopy1971-1972" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1971-1972">Warren Orlick began caddying at age nine at Grosse Isle Golf and Country Club outside of Detroit and would eventually serve for many years as PGA head professional at Tam O’Shanter Country Club in Orchard Lake, Michigan. The 1960 PGA Golf Professional of the Year, Orlick was the Michigan PGA Section president and a respected authority on the Rules of Golf with long tenures on both the PGA of America and Masters Rules committees, and was Rules Chairman for the Ryder Cup.</div>
</div>

<div class="collapse-row" id="year-1969-1970">
    <div class="collapse-header">
        <div class="head">1969-1970</div>
        <div class="sub-head">Leo Fraser - Philadelphia PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1969-1970" aria-expanded="false" aria-controls="collapseCopy1969-1970" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1969-1970">Leo Fraser joined the PGA of America in 1927 at age 17, and in 1935 he succeeded his father as PGA head professional at Seaview Country Club in Atlantic City, New Jersey. Following World War II, Fraser went to Atlantic City Country Club and became the professional-owner. Fraser served as president of the Philadelphia PGA Section for seven years. He is considered the "father" of the PGA Professional Championship, and the trophy awarded annually to the Senior PGA Professional Championship bears his name.</div>
</div>

<div class="collapse-row" id="year-1966-1968">
    <div class="collapse-header">
        <div class="head">1966-1968</div>
        <div class="sub-head">Max Elbin - Middle Atlantic PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1966-1968" aria-expanded="false" aria-controls="collapseCopy1966-1968" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1966-1968">Introduced to the game as a caddie during the Great Depression, Max Elbin became PGA head professional at Burning Tree Club outside of Washington, D.C., in 1946 and remained there until 1995. He taught golf to six U.S. Presidents. The first PGA President from the Middle Atlantic PGA Section, Elbin substantially expanded the business and education programs of the Association. He maintained key Association events and funds while working through the numerous challenges presented by the PGA's touring players, who broke away during his term and eventually formed the PGA Tour.</div>
</div>

<div class="collapse-row" id="year-1964-1965">
    <div class="collapse-header">
        <div class="head">1964-1965</div>
        <div class="sub-head">Warren Cantrell - Texas PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1964-1965" aria-expanded="false" aria-controls="collapseCopy1964-1965" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1964-1965">Warren Cantrell was perhaps the most experienced businessman to lead the PGA of America. An engineer and contractor, he owned a successful construction business with his brother and was involved in the construction of the Houston Astrodome. However, Cantrell preferred golf to business and served the Texas PGA Section in many capacities before being elected Treasurer of the PGA of America in 1958.</div>
</div>

<div class="collapse-row" id="year-1961-1963">
    <div class="collapse-header">
        <div class="head">1961-1963</div>
        <div class="sub-head">Lou Strong - Illinois PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1961-1963" aria-expanded="false" aria-controls="collapseCopy1961-1963" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1961-1963">Starting in golf as a caddie at age 9 at Urbana (Ill.) Country Club, Lou Strong became the assistant professional at the club in 1930 and head professional in 1935. He later served as head professional at Park Ridge (Ill.) Country Club and Oak Hill Country Club in Rochester, New York. Instrumental in moving PGA Headquarters to Palm Beach Gardens, Florida, following his PGA Presidency, Strong later became director of golf at PGA National Golf Club.</div>
</div>

<div class="collapse-row" id="year-1958-1960">
    <div class="collapse-header">
        <div class="head">1958-1960</div>
        <div class="sub-head">Harold Sargent - Southeastern PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1958-1960" aria-expanded="false" aria-controls="collapseCopy1958-1960" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1958-1960">Harold Sargent started in golf as a caddie at Scioto Country Club in Columbus, Ohio. He served as assistant professional at East Lake Country Club in Atlanta from 1932-1947, and then went on to serve as PGA head professional at East Lake and The Atlanta Athletic Club. During Sargent's time as PGA President, the PGA Championship changed from a match-play format to a stroke-play format and he started a pension program for employees.</div>
</div>

<div class="collapse-row" id="year-1955-1957">
    <div class="collapse-header">
        <div class="head">1955-1957</div>
        <div class="sub-head">Harry Moffitt - Northern Ohio PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1955-1957" aria-expanded="false" aria-controls="collapseCopy1955-1957" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1955-1957">A caddie at age 10, Harry Moffitt's first professional job was at Bay View Park in his native Toledo. Moffitt helped develop many fine players and was known for running an excellent golf shop. During Moffitt's tenure as PGA President, both merchandising and education programs were begun.</div>
</div>

<div class="collapse-row" id="year-1952-1954">
    <div class="collapse-header">
        <div class="head">1952-1954</div>
        <div class="sub-head">Horton Smith - Michigan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1952-1954" aria-expanded="false" aria-controls="collapseCopy1952-1954" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1952-1954">Horton Smith was the first tournament star of the modern era to become a successful club professional. In 1929, at age 21, he won seven tournaments and would go on to win 30 titles during his career, including the inaugural Masters in 1934, and capturing the 1936 title. While PGA President, Smith devoted much of his efforts to improving educational programs for PGA members. Since 1965, the PGA of America has annually presented the Horton Smith Award, honoring a PGA Professional for outstanding and continuing contributions to professional education.</div>
</div>

<div class="collapse-row" id="year-1949-1951">
    <div class="collapse-header">
        <div class="head">1949-1951</div>
        <div class="sub-head">Joe Novak - Southern California PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1949-1951" aria-expanded="false" aria-controls="collapseCopy1949-1951" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1949-1951">Joe Novak began caddying at age 7 and took his first job as a professional at age 16 at Butte (Montana) Country Club. In 1918, he headed west, ending up in Spokane, Washington.  Novak competed in the 1922, 1924 and 1925 U.S. Opens, and then moved to Bel Air Country Club in Los Angeles in 1927. Novak was the first to teach golf over the radio, in 1924. During the Great Depression he attended law school and passed the bar exam.</div>
</div>

<div class="collapse-row" id="year-1942-1948">
    <div class="collapse-header">
        <div class="head">1942-1948</div>
        <div class="sub-head">Ed Dudley - Colorado PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1942-1948" aria-expanded="false" aria-controls="collapseCopy1942-1948" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1942-1948">The professional at The Broadmoor in Colorado in the summer and Augusta (Georgia) National Golf Club in the spring and fall, Ed Dudley was a popular instructor, counting among his students U.S. President  Dwight D. Eisenhower.  Dudley competed on the 1928, 1933 and 1937 United States Ryder Cup Teams and finished in the top 10 a total of seven times over the first eight Masters Tournaments.</div>
</div>

<div class="collapse-row" id="year-1940-1941">
    <div class="collapse-header">
        <div class="head">1940-1941</div>
        <div class="sub-head">Tom Walsh - Illinois PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1940-1941" aria-expanded="false" aria-controls="collapseCopy1940-1941" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1940-1941">Tom Walsh started in golf as a caddie at Beverly Country Club in Chicago. After serving in the United States Navy, he became the director of golf at Olympia Fields (Ill.) Country Club in 1919. He opened the Walsh Brothers Golf School in 1926. One year later, he purchased land to construct Westgate Valley Country Club. He served as golf director of the Chicago Park District and helped organize the Chicago Tribune "Free Golf School."</div>
</div>

<div class="collapse-row" id="year-1933-1939">
    <div class="collapse-header">
        <div class="head">1933-1939</div>
        <div class="sub-head">George Jacobus - New Jersey PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1933-1939" aria-expanded="false" aria-controls="collapseCopy1933-1939" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1933-1939">A professional at age 16, George Jacobus was head of the New Jersey PGA Section for many years before becoming the first American-born president of the PGA. The head professional at Ridgewood Country Club in Paramus, New Jersey, Jacobus had an excellent reputation as a teacher. He was instrumental in bringing the 1935 Ryder Cup to Ridgewood.</div>
</div>

<div class="collapse-row" id="year-1931-1932">
    <div class="collapse-header">
        <div class="head">1931-1932</div>
        <div class="sub-head">Charles Hall - Southeastern PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1931-1932" aria-expanded="false" aria-controls="collapseCopy1931-1932" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1931-1932">The last foreign-born President of The PGA of America,  Hall came to the United States from England at age 3. He served as professional at courses in Nashville, Tenn., and Birmingham, Ala. A good competitor, he played in the PGA Championship and U.S. Open, and won the Southeastern PGA Section Championship twice. Hall is credited with keeping The PGA of America afloat during the Great Depression.</div>
</div>

<div class="collapse-row" id="year-1927-1930">
    <div class="collapse-header">
        <div class="head">1927-1930</div>
        <div class="sub-head">Alex Pirie - Metropolitan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1927-1930" aria-expanded="false" aria-controls="collapseCopy1927-1930" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1927-1930">A native of Scotland, Pirie was the professional at the Old Elm Club in north suburban Chicago. During his tenure as PGA President, member dues were raised from $10 to $50 and Albert Gates was hired as the first business administrator and legal advisor to the Association. Pirie's cousin, Jack, served as PGA Secretary.</div>
</div>

<div class="collapse-row" id="year-1921-1926">
    <div class="collapse-header">
        <div class="head">1921-1926</div>
        <div class="sub-head">George Sargent - Southeastern PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1921-1926" aria-expanded="false" aria-controls="collapseCopy1921-1926" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1921-1926">An assistant to legendary golfer Harry Vardon in England before emigrating to the United States, George Sargent served as a professional at notable American courses such as Interlachen (Minneapolis area), Scioto (Columbus, Ohio) and Chevy Chase (Washington D.C. area). An excellent player, Sargent won the 1909 U.S. Open and the 1912 Canadian Open. He was president of the former Northwest PGA Section, before elected president of The PGA of America.</div>
</div>

<div class="collapse-row" id="year-1919-1920">
    <div class="collapse-header">
        <div class="head">1919-1920</div>
        <div class="sub-head">Jack Mackie - Metropolitan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1919-1920" aria-expanded="false" aria-controls="collapseCopy1919-1920" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1919-1920">Jack Mackie is the longest-serving Officer in the history of the Association. He also served as PGA Vice President in 1918 and 1919, then again from 1922-1925. From 1927-1939, he was the PGA's Treasurer. Mackie was appointed to serve as a "consultant" on the Rules of Golf to the United States Golf Association and was a member of the PGA's influential Manufacturers' Relations Committee.</div>
</div>

<div class="collapse-row" id="year-1916-1919">
    <div class="collapse-header">
        <div class="head">1916-1919</div>
        <div class="sub-head">Robert White - Metropolitan PGA Section</div>
        <div class="toggle"><a data-toggle="collapse" href="#collapseCopy1916-1919" aria-expanded="false" aria-controls="collapseCopy1916-1919" class="toggle-expand-president"> <i class="fa fa-plus"></i><i class="fa fa-minus"></i></a></div> 
    </div>
    <div class="collapse " id="collapseCopy1916-1919">A native of Scotland, Robert White immigrated to the United States in 1894 to study agronomy. He served as a professional/greenkeeper at Myopia Hunt Club in suburban Boston and later at Cincinnati Golf Club and Louisville Golf Club. He would then serve as professional/greenkeeper at Ravisloe Country Club in suburban Chicago from 1902-1914. White became the professional/greenkeeper at Wykagyl Country Club in New Rochelle, New York, in 1916. He was elected the first President of the PGA of America on June 26, 1916. A charter member of the American Society of Golf Course Architects, White designed and built more than 100 courses and was the first to apply scientific principles to course maintenance in the United States.</div>
</div>