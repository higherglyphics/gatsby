---
title: History of the PGA
layout: history-timeline
order_number: 1
sidebar_label: History of the PGA
exclude_from_sidebar: true
permalink: /history/
---
<div id="trigger"></div>
  <div class="timeline-container" id="history-timeline">
    <div class="editable">
      <div class="text">
        <div class="row">
          <div class="col-xs-10 col-sm-10 col-md-8 col-lg-7 mx-auto text-center">
            <h1>History of the PGA</h1>
            <div class="header-nav">
              <span class="active">PGA HISTORY</span><a href="/history/hall-of-fame/">HALL OF FAME</a><a href="/history/past-presidents/">PAST PRESIDENTS</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="editable">
      <div class="timeline-main-container">
        <div class="timeline-period">
          <div class="timeline-period__title">
            1910
          </div>
        </div>
        <div class="timeline-item active" data-text="1916" id="item1916">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1910s.jpg">
            <p class="timeline__content-desc">On Jan. 17, Rodman Wanamaker hosts a luncheon at the invitation of his business group, the Taplow Club, in Wanamaker’s Store in New York City. The agenda: to discuss forming a national association of golfers similar to the British PGA. Less than three months later, on April 10, the PGA of America is founded with 35 charter members. The inaugural PGA Championship is conducted Oct. 10-14, at Siwanoy Country Club in Bronxville, New York. From a field of 32 golfers, Jim Barnes, a native of Cornwall, England, defeats Jock Hutchison of Scotland, 1 up, in the match-play final. Wanamaker donates the trophy, a purse of $2,580 and pays the expenses of the competitors.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1917" id="item1917">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1910s.jpg">
            <p class="timeline__content-desc">The PGA Championship is cancelled due to World War I. The PGA of America presents the American Red Cross with an ambulance, plus $1,000 for maintenance.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1919" id="item1919">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1910s.jpg">
            <p class="timeline__content-desc">The PGA Championship resumes and Jim Barnes successfully defends his title at Engineers Country Club on Long Island, New York.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1920
          </div>
        </div>
        <div class="timeline-item-even" data-text="1920" id="item1920">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1920s.jpg">
            <p class="timeline__content-desc">In May, the PGA of America publishes the first issue of The Professional Golfer of America, which would later be renamed PGA Magazine.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1921" id="item1921">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1920s.jpg">
            <p class="timeline__content-desc">Walter Hagen becomes the first American-born PGA Champion, defeating Jim Barnes at Inwood Country Club in Far Rockaway, New York.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1927" id="item1927">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1920s.jpg">
            <p class="timeline__content-desc">English Seed merchant Samuel Ryder presents the Ryder Cup as a prize for the inaugural international competition between American and British professional golfers. The United States defeats Great Britain, 9 ½ to 2 ½, at Worcester (Massachusetts) Country Club.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1930
          </div>
        </div>
        <div class="timeline-item" data-text="1933" id="item1933">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1930s.jpg">
            <p class="timeline__content-desc">George Jacobus elected the first American-born president of the PGA of America.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1935" id="item1935">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1930s.jpg">
            <p class="timeline__content-desc">Famed golf architect A.W. Tillinghast is hired by the PGA of America as a consultant to improve playability of the nation’s courses.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1937" id="item1937">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1930s.jpg">
            <p class="timeline__content-desc">The first PGA Seniors’ Championship (since renamed the KitchenAid Senior PGA Championship) is held at Augusta (Georgia) National Golf Club. Jock Hutchison wins the title among a field of 31 players.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1939" id="item1939">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1930s.jpg">
            <p class="timeline__content-desc">The Ryder Cup is cancelled due to World War II. At the urging of sportswriter Grantland Rice, the PGA Hall of Fame is established, with inductees including Tommy Armour, Walter Hagen, Bob Jones, Francis Ouimet and Gene Sarazen.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1940
          </div>
        </div>
        <div class="timeline-item" data-text="1942" id="item1942">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1940s.jpg">
            <p class="timeline__content-desc">In wartime, the PGA of America purchases two ambulances for the Red Cross and distributes clubs and balls at military bases. The Association also raises more than $25,000 for USO, Red Cross, Navy and Army Relief.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1943" id="item1943">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1940s.jpg">
            <p class="timeline__content-desc">The PGA Championship is cancelled due to World War II.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1947" id="item1947">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1940s.jpg">
            <p class="timeline__content-desc">The Ryder Cup resumes at Portland (Ore.) Golf Club when Robert Hudson, an Oregon fruit grower and canner, finances the British Team’s expenses. The U.S. wins, 11-1.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1948" id="item1948">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1940s.jpg">
            <p class="timeline__content-desc">The PGA Player of the Year Award is established, with Ben Hogan earning the first award.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1950
          </div>
        </div>
        <div class="timeline-item" data-text="1951" id="item1951">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1950s.jpg">
            <p class="timeline__content-desc">Horton Smith, winner of the first and third Masters Tournaments, is elected to his first term as PGA president. He serves through 1954, and would be recognized for his contributions to PGA education.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1952" id="item1952">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1950s.jpg">
            <p class="timeline__content-desc">The PGA of America, in conjunction with LIFE Magazine, sponsors the first National Golf Day and raises $80,000 for charity.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1954" id="item1954">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1950s.jpg">
            <p class="timeline__content-desc">The inaugural PGA Merchandise Show takes place in the parking lot of PGA National Golf Club in Dunedin, Florida, where salesmen exhibit their product lines during the Senior PGA Championship.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1955" id="item1955">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1950s.jpg">
            <p class="timeline__content-desc">The PGA Golf Professional of the Year Award is established to honor PGA members for total contributions to the game. Bill Gordon of Tam O’Shanter Country Club in Chicago is the first recipient.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1958" id="item1958">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1950s.jpg">
            <p class="timeline__content-desc">The PGA Championship changes from a match-play format to a stroke-play format. Dow Finsterwald, runner-up in 1957, wins by two strokes over Billy Casper at Llanerch Country Club in Havertown, Pennsylvania. CBS television purchases the broadcasting rights for the PGA Championship, making the 1958 PGA Championship the first to be broadcast on live television and radio.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1960
          </div>
        </div>
        <div class="timeline-item-even" data-text="1961" id="item1961">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">The “Caucasian-only” membership clause, introduced in 1934 into the PGA bylaws, is eradicated from the PGA Constitution.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1963" id="item1963">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">Jack Nicklaus wins his first PGA Championship.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1964" id="item1964">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">The PGA of America assists the United Golf Association (a minority golf association) with establishment of their own constitution and bylaws.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1966" id="item1966">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">The PGA of America celebrates its 50th Anniversary with 5,837 members.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1967" id="item1967">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">The tournament players within the PGA of America form their own organization, subsequently named the PGA TOUR.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1968" id="item1968">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1960s.jpg">
            <p class="timeline__content-desc">The first PGA Club Professional Championship (since renamed the PGA Professional Championship) is conducted in Scottsdale, Arizona. Howell Fraser of West Caldwell, New Jersey, captures the Walter Hagen Cup.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1970
          </div>
        </div>
        <div class="timeline-item-even" data-text="1975" id="item1975">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1970s.jpg">
            <p class="timeline__content-desc">The first PGA Professional Golf Management program is established at Ferris State University in Big Rapids, Michigan.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1976" id="item1976">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1970s.jpg">
            <p class="timeline__content-desc">The first Junior PGA Championship is held at Walt Disney World in Florida.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1979" id="item1979">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1970s.jpg">
            <p class="timeline__content-desc">The Ryder Cup expands to include representatives from continental Europe.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1980
          </div>
        </div>
        <div class="timeline-item" data-text="1980" id="item1980">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1980s.jpg">
            <p class="timeline__content-desc">Jack Nicklaus captures his fifth and final PGA Championship, tying Walter Hagen for the all-time mark.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1987" id="item1987">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1980s.jpg">
            <p class="timeline__content-desc">The United States loses the Ryder Cup for the first time on American soil, at Muirfield Village in Dublin, Ohio.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1988" id="item1988">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1980s.jpg">
            <p class="timeline__content-desc">The inaugural PGA Teaching &amp; Coaching Summit is conducted, in Dallas, Texas.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            1990
          </div>
        </div>
        <div class="timeline-item-even" data-text="1990" id="item1990">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1990s.jpg">
            <p class="timeline__content-desc">The PGA of America celebrates its 75th Anniversary with 12,044 members.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1994" id="item1994">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1990s.jpg">
            <p class="timeline__content-desc">The PGA of America creates the Ernie Sabayrac Award for lifetime contributions to the golf industry. Sabayrac is the inaugural recipient.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="1996" id="item1996">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1990s.jpg">
            <p class="timeline__content-desc">The PGA of America fulfills its dream of owning and operating its own courses with the opening of PGA Golf Club in Port St. Lucie, Florida.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="1999" id="item1999">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-1990s.jpg">
            <p class="timeline__content-desc">The PGA Learning Center (since renamed the PGA Center for Golf Learning and Performance) opens at PGA Village in Port St. Lucie, Florida. Tiger Woods wins his first of four PGA Championships.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            2000
          </div>
        </div>
        <div class="timeline-item-even" data-text="2001" id="item2001">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The Ryder Cup, postponed following terrorist attacks upon America on September 11, 2011, announces plans to resume on even-numbered years. The PGA of America donates $500,000 for Sept. 11 Relief Fund, matching U.S. Ryder Cup Team donations.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="2003" id="item2003">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The 50th PGA Merchandise Show is celebrated at the Orange County Convention Center in Orlando, Florida.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2004" id="item2004">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The PGA of America launches nationwide Play Golf America initiative to support the growth of the game.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="2005" id="item2005">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The PGA Golf Professional Hall of Fame (renamed the PGA of America Hall of Fame) inducts an unprecedented 122 members featuring past PGA Presidents and PGA Golf Professional of the Year recipients.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2006" id="item2006">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The PGA of America celebrates its 90th anniversary at the Hotel Martinique (now Radisson Martinique) in New York City, where the PGA was founded.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="2007" id="item2007">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">Patriot Golf Day, inspired by PGA Professional Maj. Dan Rooney, is launched to raise money for educational scholarships for children of military personnel who have either perished or were severely wounded in the line of duty in Iraq and Afghanistan.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2009" id="item2009">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2000s.jpg">
            <p class="timeline__content-desc">The PGA of America bestows posthumous membership upon African-American golf pioneers Ted Rhodes, John Shippen and Bill Spiller, who were denied the opportunity to become PGA Members during their professional careers. Posthumous honorary membership is granted to Joe Louis, the legendary world heavyweight boxing champion, who became an advocate for diversity in golf.</p>
          </div>
        </div>
        <div class="timeline-period">
          <div class="timeline-period__title">
            2010
          </div>
        </div>
        <div class="timeline-item" data-text="2011" id="item2011">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">PGA Jr. League, employing the team concept in youth recreational golf, opens with four teams (Atlanta, Tampa, Dallas and San Diego), capped by a “World Series” in September in Atlanta. By 2017, PGA Jr. League grew to more than 3,400 teams reaching nearly 45,000 youth participants.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2012" id="item2012">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">Rory McIlroy breaks Jack Nicklaus’ all-time victory margin record when he wins the 94th PGA Championship by eight strokes at Kiawah Island (S.C.) Golf resort’s Ocean Course.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="2013" id="item2013">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">The Masters Tournament, United States Golf Association and the PGA of America collaborate to launch the Drive, Chip and Putt Championship. The free nationwide junior golf development competition begins at Augusta National on the Sunday prior to the Masters Tournament.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2014" id="item2014">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">KPMG, the PGA of America and the LPGA announce the KPMG Women’s PGA Championship, and the creation of a multi-faceted program focused on the advancement and empowerment of women on and off the golf course. Suzy Whaley of Farmington, Connecticut, is elected PGA Secretary, becoming the first woman to attain PGA national office.</p>
          </div>
        </div>
        <div class="timeline-item" data-text="2016" id="item2016">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">The PGA of America Centennial culminates with a celebration in New York City, birthplace of the Association, with delegates gathering at the 100th PGA Annual Meeting.</p>
          </div>
        </div>
        <div class="timeline-item-even" data-text="2018" id="item2018">
          <div class="timeline__content">
            <img class="timeline__img" src="/assets/images/history-2010s.jpg">
            <p class="timeline__content-desc">Bellerive Country Club near St. Louis hosts the 100th PGA Championship.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="highlighter-rouge">
    <div class="highlight">
      <pre class="highlight"><code>  &lt;script&gt;sceneMaker('item1916')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1917')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1919')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1920')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1921')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1927')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1933')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1935')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1937')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1939')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1942')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1943')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1947')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1948')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1951')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1952')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1954')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1955')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1958')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1961')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1963')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1964')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1966')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1967')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1968')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1975')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1976')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1979')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1980')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1987')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1988')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item1990')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1994')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1996')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item1999')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item2001')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2003')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2004')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2005')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2006')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2007')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2009')&lt;/script&gt;
  
  

  &lt;script&gt;sceneMaker('item2011')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2012')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2013')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2014')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2016')&lt;/script&gt;

  &lt;script&gt;sceneMaker('item2018')&lt;/script&gt;
</code></pre>
    </div>
  </div>