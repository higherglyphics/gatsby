---
title: History of the PGA
layout: history-timeline
order_number: 1
sidebar_label: History of the PGA
exclude_from_sidebar: true
permalink: /history-old/
timeline:
  - period: 1910
    content:
      - title: 1916
        text: "On Jan. 17, Rodman Wanamaker hosts a luncheon at the invitation of his business group, the Taplow Club, in Wanamaker’s Store in New York City. The agenda: to discuss forming a national association of golfers similar to the British PGA. Less than three months later, on April 10, the PGA of America is founded with 35 charter members. The inaugural PGA Championship is conducted Oct. 10-14, at Siwanoy Country Club in Bronxville, New York. From a field of 32 golfers, Jim Barnes, a native of Cornwall, England, defeats Jock Hutchison of Scotland, 1 up, in the match-play final. Wanamaker donates the trophy, a purse of $2,580 and pays the expenses of the competitors."
        image: "/assets/images/history-1910s.jpg"
      - title: 1917
        text: The PGA Championship is cancelled due to World War I.  The PGA of America presents the American Red Cross with an ambulance, plus $1,000 for maintenance.
        image: "/assets/images/history-1910s.jpg"
      - title: 1919
        text:  The PGA Championship resumes and Jim Barnes successfully defends his title at Engineers Country Club on Long Island, New York.
        image: "/assets/images/history-1910s.jpg"
  - period: 1920
    content:
      - title: 1920
        text: In May, the PGA of America publishes the first issue of The Professional Golfer of America, which would later be renamed PGA Magazine.
        image: "/assets/images/history-1920s.jpg"
      - title: 1921
        text:  Walter Hagen becomes the first American-born PGA Champion, defeating Jim Barnes at Inwood Country Club in Far Rockaway, New York.
        image: "/assets/images/history-1920s.jpg"
      - title: 1927
        text:  English Seed merchant Samuel Ryder presents the Ryder Cup as a prize for the inaugural international competition between American and British professional golfers.  The United States defeats Great Britain, 9 ½ to 2 ½, at Worcester (Massachusetts) Country Club.
        image: "/assets/images/history-1920s.jpg"
  - period: 1930
    content:
    - title: 1933
      text: George Jacobus elected the first American-born president of the PGA of America.
      image: "/assets/images/history-1930s.jpg"
    - title: 1935
      text: Famed golf architect A.W. Tillinghast is hired by the PGA of America as a consultant to improve playability of the nation’s courses.
      image: "/assets/images/history-1930s.jpg"
    - title: 1937
      text: The first PGA Seniors’ Championship (since renamed the KitchenAid Senior PGA Championship) is held at Augusta (Georgia) National Golf Club.  Jock Hutchison wins the title among a field of 31 players.
      image: "/assets/images/history-1930s.jpg"
    - title: 1939
      text: The Ryder Cup is cancelled due to World War II. At the urging of sportswriter Grantland Rice, the PGA Hall of Fame is established, with inductees including Tommy Armour, Walter Hagen, Bob Jones, Francis Ouimet and Gene Sarazen.
      image: "/assets/images/history-1930s.jpg"
  - period: 1940
    content:
    - title: 1942
      text: In wartime, the PGA of America purchases two ambulances for the Red Cross and distributes clubs and balls at military bases.  The Association also raises more than $25,000 for USO, Red Cross, Navy and Army Relief.
      image: "/assets/images/history-1940s.jpg"
    - title: 1943
      text: The PGA Championship is cancelled due to World War II.
      image: "/assets/images/history-1940s.jpg"
    - title: 1947
      text: The Ryder Cup resumes at Portland (Ore.) Golf Club when Robert Hudson, an Oregon fruit grower and canner, finances the British Team’s expenses.  The U.S. wins, 11-1.
      image: "/assets/images/history-1940s.jpg"
    - title: 1948
      text: The PGA Player of the Year Award is established, with Ben Hogan earning the first award.
      image: "/assets/images/history-1940s.jpg"
  - period: 1950
    content:
    - title: 1951
      text: Horton Smith, winner of the first and third Masters Tournaments, is elected to his first term as PGA president.  He serves through 1954, and would be recognized for his contributions to PGA education.
      image: "/assets/images/history-1950s.jpg"
    - title: 1952
      text: The PGA of America, in conjunction with LIFE Magazine, sponsors the first National Golf Day and raises $80,000 for charity.
      image: "/assets/images/history-1950s.jpg"
    - title: 1954
      text: The inaugural PGA Merchandise Show takes place in the parking lot of PGA National Golf Club in Dunedin, Florida, where salesmen exhibit their product lines during the Senior PGA Championship.
      image: "/assets/images/history-1950s.jpg"
    - title: 1955
      text: The PGA Golf Professional of the Year Award is established to honor PGA members for total contributions to the game.  Bill Gordon of Tam O’Shanter Country Club in Chicago is the first recipient.
      image: "/assets/images/history-1950s.jpg"
    - title: 1958
      text: The PGA Championship changes from a match-play format to a stroke-play format.  Dow Finsterwald, runner-up in 1957, wins by two strokes over Billy Casper at Llanerch Country Club in Havertown, Pennsylvania. CBS television purchases the broadcasting rights for the PGA Championship, making the 1958 PGA Championship the first to be broadcast on live television and radio.
      image: "/assets/images/history-1950s.jpg"
  - period: 1960
    content:
    - title: 1961
      text: The “Caucasian-only” membership clause, introduced in 1934 into the PGA bylaws, is eradicated from the PGA Constitution.
      image: "/assets/images/history-1960s.jpg"
    - title: 1963
      text: Jack Nicklaus wins his first PGA Championship.
      image: "/assets/images/history-1960s.jpg"
    - title: 1964
      text: The PGA of America assists the United Golf Association (a minority golf association) with establishment of their own constitution and bylaws.
      image: "/assets/images/history-1960s.jpg"
    - title: 1966
      text: The PGA of America celebrates its 50th Anniversary with 5,837 members.
      image: "/assets/images/history-1960s.jpg"
    - title: 1967
      text: The tournament players within the PGA of America form their own organization, subsequently named the PGA TOUR.
      image: "/assets/images/history-1960s.jpg"
    - title: 1968
      text: The first PGA Club Professional Championship (since renamed the PGA Professional Championship) is conducted in Scottsdale, Arizona. Howell Fraser of West Caldwell, New Jersey, captures the Walter Hagen Cup.
      image: "/assets/images/history-1960s.jpg"
  - period: 1970
    content:
    - title: 1975
      text: The first PGA Professional Golf Management program is established at Ferris State University in Big Rapids, Michigan.
      image: "/assets/images/history-1970s.jpg"
    - title: 1976
      text: The first Junior PGA Championship is held at Walt Disney World in Florida.
      image: "/assets/images/history-1970s.jpg"
    - title: 1979
      text: The Ryder Cup expands to include representatives from continental Europe.
      image: "/assets/images/history-1970s.jpg"
  - period: 1980
    content:
    - title: 1980
      text: Jack Nicklaus captures his fifth and final PGA Championship, tying Walter Hagen for the all-time mark.
      image: "/assets/images/history-1980s.jpg"
    - title: 1987
      text: The United States loses the Ryder Cup for the first time on American soil, at Muirfield Village in Dublin, Ohio.
      image: "/assets/images/history-1980s.jpg"
    - title: 1988
      text: The inaugural PGA Teaching & Coaching Summit is conducted, in Dallas, Texas.
      image: "/assets/images/history-1980s.jpg"
  - period: 1990
    content:
    - title: 1990
      text:  The PGA of America celebrates its 75th Anniversary with 12,044 members.
      image: "/assets/images/history-1990s.jpg"
    - title: 1994
      text: The PGA of America creates the Ernie Sabayrac Award for lifetime contributions to the golf industry. Sabayrac is the inaugural recipient.
      image: "/assets/images/history-1990s.jpg"
    - title: 1996
      text: The PGA of America fulfills its dream of owning and operating its own courses with the opening of PGA Golf Club in Port St. Lucie, Florida.
      image: "/assets/images/history-1990s.jpg"
    - title: 1999
      text: The PGA Learning Center (since renamed the PGA Center for Golf Learning and Performance) opens at PGA Village in Port St. Lucie, Florida. Tiger Woods wins his first of four PGA Championships.
      image: "/assets/images/history-1990s.jpg"
  - period: 2000
    content:
    - title: 2001
      text: The Ryder Cup, postponed following terrorist attacks upon America on September 11, 2011, announces plans to resume on even-numbered years.  The PGA of America donates $500,000 for Sept. 11 Relief Fund, matching U.S. Ryder Cup Team donations.
      image: "/assets/images/history-2000s.jpg"
    - title: 2003
      text: The 50th PGA Merchandise Show is celebrated at the Orange County Convention Center in Orlando, Florida.
      image: "/assets/images/history-2000s.jpg"
    - title: 2004
      text: The PGA of America launches nationwide Play Golf America initiative to support the growth of the game.
      image: "/assets/images/history-2000s.jpg"
    - title: 2005
      text: The PGA Golf Professional Hall of Fame (renamed the PGA of America Hall of Fame) inducts an unprecedented 122 members featuring past PGA Presidents and PGA Golf Professional of the Year recipients.
      image: "/assets/images/history-2000s.jpg"
    - title: 2006
      text: The PGA of America celebrates its 90th anniversary at the Hotel Martinique (now Radisson Martinique) in New York City, where the PGA was founded.
      image: "/assets/images/history-2000s.jpg"
    - title: 2007
      text: Patriot Golf Day, inspired by PGA Professional Maj. Dan Rooney, is launched to raise money for educational scholarships for children of military personnel who have either perished or were severely wounded in the line of duty in Iraq and Afghanistan.
      image: "/assets/images/history-2000s.jpg"
    - title: 2009
      text: The PGA of America bestows posthumous membership upon African-American golf pioneers Ted Rhodes, John Shippen and Bill Spiller, who were denied the opportunity to become PGA Members during their professional careers.  Posthumous honorary membership is granted to Joe Louis, the legendary world heavyweight boxing champion, who became an advocate for diversity in golf.
      image: "/assets/images/history-2000s.jpg"
  - period: 2010
    content:
    - title: 2011
      text: PGA Jr. League, employing the team concept in youth recreational golf, opens with four teams (Atlanta, Tampa, Dallas and San Diego), capped by a “World Series” in September in Atlanta. By 2017, PGA Jr. League grew to more than 3,400 teams reaching nearly 45,000 youth participants.
      image: "/assets/images/history-2010s.jpg"
    - title: 2012
      text: Rory McIlroy breaks Jack Nicklaus’ all-time victory margin record when he wins the 94th PGA Championship by eight strokes at Kiawah Island (S.C.) Golf resort’s Ocean Course.
      image: "/assets/images/history-2010s.jpg"
    - title: 2013
      text: The Masters Tournament, United States Golf Association and the PGA of America collaborate to launch the Drive, Chip and Putt Championship. The free nationwide junior golf development competition begins at Augusta National on the Sunday prior to the Masters Tournament.
      image: "/assets/images/history-2010s.jpg"
    - title: 2014
      text: KPMG, the PGA of America and the LPGA announce the KPMG Women’s PGA Championship, and the creation of a multi-faceted program focused on the advancement and empowerment of women on and off the golf course. Suzy Whaley of Farmington, Connecticut, is elected PGA Secretary, becoming the first woman to attain PGA national office.
      image: "/assets/images/history-2010s.jpg"
    - title: 2016
      text: The PGA of America Centennial culminates with a celebration in New York City, birthplace of the Association, with delegates gathering at the 100th PGA Annual Meeting.
      image: "/assets/images/history-2010s.jpg"
    - title: 2018
      text:  Bellerive Country Club near St. Louis hosts the 100th PGA Championship.
      image: "/assets/images/history-2010s.jpg"
---
<div id="trigger"></div>
<div class="timeline-container" id="history-timeline">
<div class="editable">
  <div class="text">
    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-8 col-lg-7 mx-auto text-center">
        <h1>History of the PGA</h1>
        <div class="header-nav"><span class="active">PGA HISTORY</span><a href="/history/hall-of-fame/">HALL OF FAME</a><a href="/history/past-presidents/">PAST PRESIDENTS</a></div>
      </div>
    </div>
  </div>
</div>
<div class="editable">
    <div class="timeline-main-container">
      {% for period in page.timeline  %}
        <div class="timeline-period">
          <div class="timeline-period__title">{{period.period}}</div>
        </div>      
        {% for item in period.content %}
          <div class="timeline-item{% cycle '', '-even' %}{% if item.title == 1916 %} active{% endif %}" data-text="{{item.title}}" id="item{{item.title}}">
            <div class="timeline__content">
              {% if item.image %}
                <img class="timeline__img" src="{{ item.image }}" />
              {% endif %}
              <p class="timeline__content-desc">
                {{ item.text }}
              </p>
            </div>
          </div>
        {% endfor %}  
      {% endfor %}
    </div>
  </div>
</div>

  {% for period in page.timeline  %}
    {% for item in period.content %}
      <script>sceneMaker('item{{item.title}}')</script>
    {% endfor %}  
  {% endfor %}
