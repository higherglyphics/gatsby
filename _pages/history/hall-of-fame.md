---
title: PGA of America Hall of Fame
layout: default
order_number: 2
sidebar_label: Hall of Fame
---
  <p>The PGA of America Hall of Fame originated in 1940 at the suggestion of famed sportswriter Grantland Rice. It is the highest honor that the PGA of America can bestow upon its membership or ambassadors of golf.</p>
  <p>Most of the original inductees were later enshrined at the PGA World Golf Hall of Fame in Pinehurst, North Carolina.</p>
  <p>In 1993, the PGA of America ceased PGA World Golf Hall of Fame operations in Pinehurst and subsequently relocated to the World Golf Hall of Fame in St. Augustine, Florida.</p>
  <div class="collapse" id="collapseCopy">
    <p>While all of the Pinehurst-enshrined members were transferred to the new World Golf Hall of Fame, there were some members of the PGA Hall of Fame who were not recognized at the facility in World Golf Village.</p>
    <p>In 2002, the PGA opened the PGA Historical Center [later the PGA Museum of Golf] at PGA Village in Port St. Lucie, Florida. This paved the way for the first home for the PGA Hall of Fame. The inaugural ceremony was conducted Sept. 8, 2005, as the Association recognized all PGA Members who have made significant and lasting contributions to building the PGA of America and the game of golf.</p>
    <p>In 2015, the PGA of America reinstituted its original Hall of Fame requirements to include non-PGA Members who have served as ambassadors of golf.</p>
    <p>In December 2015, the PGA Museum of Golf ceased operations, with the PGA Hall of Fame’s new location in transition.</p>
  </div>
  <p><a aria-controls="collapseCopy" aria-expanded="false" data-toggle="collapse" href="#collapseCopy" id="toggle-expand"><span class="readMore">Read More <i class="fa fa-angle-down"></i></span><span class="readLess">Read Less <i class="fa fa-angle-up"></i></span></a></p>
  <h3>PGA of America Hall of Fame Members (including year of induction)</h3><select class="form-control select-narrow" onchange="location=this.options[this.selectedIndex].value">
    <option>
      Jump to a Year
    </option>
    <option value="#year-1940">
      1940
    </option>
    <option value="#year-1953">
      1953
    </option>
    <option value="#year-1954">
      1954
    </option>
    <option value="#year-1955">
      1955
    </option>
    <option value="#year-1956">
      1956
    </option>
    <option value="#year-1957">
      1957
    </option>
    <option value="#year-1958">
      1958
    </option>
    <option value="#year-1960">
      1960
    </option>
    <option value="#year-1961">
      1961
    </option>
    <option value="#year-1962">
      1962
    </option>
    <option value="#year-1963">
      1963
    </option>
    <option value="#year-1964">
      1964
    </option>
    <option value="#year-1965">
      1965
    </option>
    <option value="#year-1966">
      1966
    </option>
    <option value="#year-1967">
      1967
    </option>
    <option value="#year-1968">
      1968
    </option>
    <option value="#year-1969">
      1969
    </option>
    <option value="#year-1974">
      1974
    </option>
    <option value="#year-1975">
      1975
    </option>
    <option value="#year-1977">
      1977
    </option>
    <option value="#year-1978">
      1978
    </option>
    <option value="#year-1979">
      1979
    </option>
    <option value="#year-1980">
      1982
    </option>
    <option value="#year-2005">
      2005
    </option>
    <option value="#year-2006">
      2006
    </option>
    <option value="#year-2009">
      2009
    </option>
    <option value="#year-2011">
      2011
    </option>
    <option value="#year-2013">
      2013
    </option>
    <option value="#year-2015">
      2015
    </option>
    <option value="#year-2017">
      2017
    </option>
  </select>
  <div class="history-hall">
    <hr class="history">
    <h2 class="table-heading" id="year-1940">1940</h2>
    <div class="row">
      <div class="col-md-4">
        Willie Anderson
      </div>
      <div class="col-md-4">
        Tommy Armour
      </div>
      <div class="col-md-4">
        Jim Barnes
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Charles “Chick” Evans
      </div>
      <div class="col-md-4">
        Walter Hagen
      </div>
      <div class="col-md-4">
        Robert Tyre “Bob” Jones
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Francis Ouimet
      </div>
      <div class="col-md-4">
        Alex Smith
      </div>
      <div class="col-md-4">
        Jerry Travers
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Walter Travis
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1953">1953</h2>
    <div class="row">
      <div class="col-md-4">
        Ben Hogan
      </div>
      <div class="col-md-4">
        Byron Nelson
      </div>
      <div class="col-md-4">
        Sam Snead
      </div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1954">1954</h2>
    <div class="row">
      <div class="col-md-4">
        Macdonald Smith
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1955">1955</h2>
    <div class="row">
      <div class="col-md-4">
        Leo Diegel
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1956">1956</h2>
    <div class="row">
      <div class="col-md-4">
        Craig Wood
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1957">1957</h2>
    <div class="row">
      <div class="col-md-4">
        Craig Wood
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1958">1958</h2>
    <div class="row">
      <div class="col-md-4">
        Harry Cooper
      </div>
      <div class="col-md-4">
        Jock Hutchison
      </div>
      <div class="col-md-4">
        Paul Runyan
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Horton Smith
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1960">1960</h2>
    <div class="row">
      <div class="col-md-4">
        Mike Brady
      </div>
      <div class="col-md-4">
        Jimmy Demaret
      </div>
      <div class="col-md-4">
        Fred McLeod
      </div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1961">1961</h2>
    <div class="row">
      <div class="col-md-4">
        Johnny Farrell
      </div>
      <div class="col-md-4">
        Lawson Little
      </div>
      <div class="col-md-4">
        Henry Picard
      </div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1962">1962</h2>
    <div class="row">
      <div class="col-md-4">
        Ernest Joseph “Dutch” Harrison
      </div>
      <div class="col-md-4">
        Olin Dutra
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1963">1963</h2>
    <div class="row">
      <div class="col-md-4">
        Ralph Guldahl
      </div>
      <div class="col-md-4">
        Johnny Revolta
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1964">1964</h2>
    <div class="row">
      <div class="col-md-4">
        Ed Dudley
      </div>
      <div class="col-md-4">
        Lloyd Mangram
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1965">1965</h2>
    <div class="row">
      <div class="col-md-4">
        Vic Ghezzi
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1966">1966</h2>
    <div class="row">
      <div class="col-md-4">
        Billy Burke
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1967">1967</h2>
    <div class="row">
      <div class="col-md-4">
        Bobby Cruickshank
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1968">1968</h2>
    <div class="row">
      <div class="col-md-4">
        Melvin R. “Chick” Harbert
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1969">1969</h2>
    <div class="row">
      <div class="col-md-4">
        Chandler Harper
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1974">1974</h2>
    <div class="row">
      <div class="col-md-4">
        Julius Boros
      </div>
      <div class="col-md-4">
        Cary Middlecoff
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1975">1975</h2>
    <div class="row">
      <div class="col-md-4">
        Jack Burke Jr.
      </div>
      <div class="col-md-4">
        Doug Ford
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1977">1977</h2>
    <div class="row">
      <div class="col-md-4">
        Babe Didrikson Zaharias
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1978">1978</h2>
    <div class="row">
      <div class="col-md-4">
        Patty Berg
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1979">1979</h2>
    <div class="row">
      <div class="col-md-4">
        Roberto DeVicenzo
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1980">1980</h2>
    <div class="row">
      <div class="col-md-4">
        Arnold Palmer
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-1982">1982</h2>
    <div class="row">
      <div class="col-md-4">
        Billy Casper
      </div>
      <div class="col-md-4">
        Gene Littler
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2005">2005</h2>
    <h3 class="first">Past Presidents</h3>
    <div class="row">
      <div class="col-md-4">
        Robert White (1916-1919)
      </div>
      <div class="col-md-4">
        Warren Cantrell (1964-1965)
      </div>
      <div class="col-md-4">
        Mickey Powell (1985-1986)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Jack Mackie (1919-1920)
      </div>
      <div class="col-md-4">
        Max Elbin (1966-1968)
      </div>
      <div class="col-md-4">
        James Ray “J.R.” Carpenter (1987-1988)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        George Sargent (1921-1926)
      </div>
      <div class="col-md-4">
        Leo Fraser (1969-1970)
      </div>
      <div class="col-md-4">
        Patrick Rielly (1989-1990)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Alex Pirie (1927-1930)
      </div>
      <div class="col-md-4">
        Warren Orlick (1971-1972)
      </div>
      <div class="col-md-4">
        Dick Smith (1992-1992)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Charles Hall (1931-1932)
      </div>
      <div class="col-md-4">
        William Clarke (1973-1974)
      </div>
      <div class="col-md-4">
        Gary Schaal (1993-1994)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        George Jacobus (1933-1939)
      </div>
      <div class="col-md-4">
        Henry Poe (1975-1976)
      </div>
      <div class="col-md-4">
        Tom Addis III (1995-1996)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Tom Walsh (1940-1941)
      </div>
      <div class="col-md-4">
        Don Padgett II (1977-1978)
      </div>
      <div class="col-md-4">
        Ken Lindsay (1997-1998)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Joe Novak (1949-1951)
      </div>
      <div class="col-md-4">
        Frank Cardi (1979-1980)
      </div>
      <div class="col-md-4">
        Will Mann (1999-2000)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Harry Moffitt (1952-1954)
      </div>
      <div class="col-md-4">
        Joe Black (1981-1982)
      </div>
      <div class="col-md-4">
        Jack Connelly (2001-2002)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Harold Sargent (1958-1960)
      </div>
      <div class="col-md-4">
        Mark Kizziar (1983-1984)
      </div>
      <div class="col-md-4">
        M.G. Orender (2003-2004)
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Lou Strong (1961-1963)
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <h3>PGA Golf Professionals of the Year</h3>
    <div class="row">
      <div class="col-md-4">
        1955 - Bill Gordon
      </div>
      <div class="col-md-4">
        1956 - Harry Shepard
      </div>
      <div class="col-md-4">
        1957 - Dugan Aycock
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1958 - Harry Pezzullo
      </div>
      <div class="col-md-4">
        1959 - Eddie Duino Sr.
      </div>
      <div class="col-md-4">
        1960 - Warren Orlick
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1961 - Don Padgett II
      </div>
      <div class="col-md-4">
        1962 - Tom LoPresti
      </div>
      <div class="col-md-4">
        1963 - Bruce Herd
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1964 - Lyle Wehrman
      </div>
      <div class="col-md-4">
        1965 - Hubby Habjan
      </div>
      <div class="col-md-4">
        1966 - Bill Strausbaugh Jr.
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1967 - Ernie Vossler
      </div>
      <div class="col-md-4">
        1968 - Hardy Loudermilk
      </div>
      <div class="col-md-4">
        1969 - Wally Mund
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1969 - A. Hubert Smith
      </div>
      <div class="col-md-4">
        1970 - Grady Shumate
      </div>
      <div class="col-md-4">
        1971 - Ross Collins
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1972 - Howard Morrette
      </div>
      <div class="col-md-4">
        1973 - Warren Smith
      </div>
      <div class="col-md-4">
        1974 - Paul Harney
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1975 - Walker Inman Jr.
      </div>
      <div class="col-md-4">
        1976 - Ron Letellier
      </div>
      <div class="col-md-4">
        1977 - Don Soper
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1978 - Walter Lowell
      </div>
      <div class="col-md-4">
        1979 - Gary Ellis
      </div>
      <div class="col-md-4">
        1980 - Stan Thirsk
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1981 - John Gerring
      </div>
      <div class="col-md-4">
        1982 - Bob Popp
      </div>
      <div class="col-md-4">
        1983 - Ken Lindsay
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1984 - Jerry Mowlds
      </div>
      <div class="col-md-4">
        1985 - Jerry Cozby
      </div>
      <div class="col-md-4">
        1986 - David Ogilvie
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1987 - Bob Ford
      </div>
      <div class="col-md-4">
        1988 - Hank Majewski
      </div>
      <div class="col-md-4">
        1989 - Tom Addis III
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1990 - Jim Albus
      </div>
      <div class="col-md-4">
        1991 - Joe Jemsek
      </div>
      <div class="col-md-4"></div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1992 - Martin T. Kavanaugh II
      </div>
      <div class="col-md-4">
        1993 - Don Kotnik
      </div>
      <div class="col-md-4">
        1994 - Dick Murphy
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1995 - David C. Price
      </div>
      <div class="col-md-4">
        1996 - Randall Smith
      </div>
      <div class="col-md-4">
        1997 - Tom Sargent
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        1998 - Ken Morton Sr.
      </div>
      <div class="col-md-4">
        1999 - Ed Hoard
      </div>
      <div class="col-md-4">
        2000 - Charles “Vic” Kline
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        2001 - Tony Morosco
      </div>
      <div class="col-md-4">
        2002 - Jock Olson
      </div>
      <div class="col-md-4">
        2003 - Jim Brotherton Jr.
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        2004 - Craig Harmon
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2006">2006</h2>
    <div class="row">
      <div class="col-md-4">
        Manuel de la Torre
      </div>
      <div class="col-md-4">
        Bill Eschenbrenner
      </div>
      <div class="col-md-4">
        Dow Finsterwald
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        William Heald
      </div>
      <div class="col-md-4">
        Jack Nicklaus
      </div>
      <div class="col-md-4">
        Roger Warren
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Dr. Gary Wiren
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2009">2009</h2>
    <div class="row">
      <div class="col-md-4">
        Harry “Cotton” Berrier
      </div>
      <div class="col-md-4">
        Don Essig III
      </div>
      <div class="col-md-4">
        Claude Harmon Sr.
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Brent Krause
      </div>
      <div class="col-md-4">
        Jim Manthis
      </div>
      <div class="col-md-4">
        Eddie Merrins
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Harvey Penick
      </div>
      <div class="col-md-4">
        Brian Whitcomb
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2011">2011</h2>
    <div class="row">
      <div class="col-md-4">
        Jim Antkiewicz
      </div>
      <div class="col-md-4">
        Jim Awtrey
      </div>
      <div class="col-md-4">
        Samuel Henry "Errie" Ball
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Jack Barber
      </div>
      <div class="col-md-4">
        Jim Flick
      </div>
      <div class="col-md-4">
        Jim Remy
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Guy Wimberly
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2013">2013</h2>
    <div class="row">
      <div class="col-md-4">
        Jimmie DeVoe
      </div>
      <div class="col-md-4">
        Don "Chip" Essig IV
      </div>
      <div class="col-md-4">
        Michael Hebron
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Jim Mrva
      </div>
      <div class="col-md-4">
        Bill Ogden
      </div>
      <div class="col-md-4">
        William "Bill" Powell
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Bob Toski
      </div>
      <div class="col-md-4">
        Allen Wronowski
      </div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2015">2015</h2>
    <div class="row">
      <div class="col-md-4">
        Tommy Bolt
      </div>
      <div class="col-md-4">
        Ray Cutright
      </div>
      <div class="col-md-4">
        Michael Doctor
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        George Hannon
      </div>
      <div class="col-md-4">
        Charles L. "Charlie" Sifford
      </div>
      <div class="col-md-4">
        Payne Stewart
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Lee Trevino
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
    <h2 class="table-heading" id="year-2017">2017</h2>
    <div class="row">
      <div class="col-md-4">
        Gary Player
      </div>
      <div class="col-md-4">
        Renee Powell
      </div>
      <div class="col-md-4">
        George Henry Schneiter
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Mike Schultz
      </div>
      <div class="col-md-4">
        Joe Tesori
      </div>
      <div class="col-md-4">
        Lew Worsham Jr.
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        Mickey Wright
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <hr class="history">
  </div>