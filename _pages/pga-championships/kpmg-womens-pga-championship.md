---
title: KPMG Women's PGA Championship
layout: default
order_number: 40
sidebar_label: KPMG Women's Championship
---
<h3 id="supporting-the-development-advancement-and-empowerment-of-women-on-and-off-the-course">Supporting the development, advancement and empowerment of women on and off the course</h3>

<p>The KPMG Women’s PGA Championship continues the rich tradition of the LPGA Championship as the second-longest running tournament in the history of the Ladies Professional Golf Association. The Championship held its inaugural event in 1955 at Orchard Ridge Country Club in Fort Wayne, Indiana, and today it offers a purse among the highest in women’s golf.  Every year it pairs a world-class major golf championship with a women’s leadership summit and an ongoing charitable initiative to inspire and develop new generations of leaders. </p>

<p>This Championship is one of five majors in women’s golf, joining the ANA Inspiration, U.S. Women’s Open, AIG Women’s British Open and The Evian Championship. </p>

<h3 id="the-next-womens-championship">The Next Women’s Championship</h3>

<p>Hazeltine National Golf Club in Chaska, Minnesota, will host the 2019 KPMG Women’s PGA Championship, June 18-23. This will be the first time the KPMG Women’s PGA Championship is at Hazeltine National and only the second time a women’s major championship is played in the state of Minnesota. Recognized throughout the world as a monumental test of golf, Hazeltine is one of only two golf clubs in the country to have hosted every premier championship offered by the USGA and PGA of America and will also be the host site of the 2028 Ryder Cup.</p>

<h3 id="tickets">Tickets</h3>

<p>Tickets are available now for 2019 KPMG Women’s PGA Championship. <a href="https://ems.pgalinks.com/ticketing/index.cfm?sale_id=430" target="_blank">Follow this link for more information and to purchase tickets</a>.</p>