---
title: PGA Professional Championship
layout: default
order_number: 50
sidebar_label: PGA Professional Championship
---
<h3 id="the-pga-professionals-showcase">The PGA Professional’s Showcase</h3>

<p>The PGA Professional Championship began in 1968 to provide additional playing opportunities for PGA Professionals. It has become a showcase event featuring some of the finest players in the Association. Formerly a 360-player field, the format of the PGA Professional Championship was converted from 1997-2005 to a larger nationwide event featuring the finest 156 players at the peak of their games. Today, the field features 312 players.</p>

<p>The list of Champions includes: Sam Snead, Bob Rosburg, Ed Dougherty, Bruce Fleisher, Larry Gilbert, Rex Baxter Jr., Don Massengale, Laurie Hammer, Larry Webb, Bob Boyd, Brett Upper, Bruce Zabriski and Mike Small.</p>

<h3 id="the-pga-professionals-championship">The PGA Professional’s Championship</h3>

<p>The 52nd Annual PGA Professional Championship will be played April 28th – May 1st 2019 at Belfair of Bluffton, South Carolina—one of the Southeast’s acclaimed golf destinations. Belfair, featuring the Tom Fazio-designed East and West Courses, also hosted the 2018 U.S. Open local qualifying tournament, and was the site of the 2013 South Carolina Open and the Players Amateur (2000-11), which featured past Champions Ben Curtis, Rickie Fowler, Bill Haas and Brian Harman. </p>