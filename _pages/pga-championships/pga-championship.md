---
title: PGA Championship
layout: default
order_number: 40
sidebar_label: PGA Championship
---
<h3 id="the-pga-championship">The PGA Championship</h3>

<p>Since its inception in 1916, the PGA Championship has evolved into one of the world’s premier sporting events. Each summer, one of the nation’s leading golf facilities hosts the game’s best professionals as they compete for the Rodman Wanamaker Trophy and a major championship title. The PGA Championship has featured the most players in the Top 100 of the Official World Golf Rankings, and has perennially boasted the strongest field in golf. </p>

<p>This Championship is one of four majors in men’s professional golf joining The Masters, U.S. Open, and The Open. </p>

<h3 id="the-next-pga-championship">The Next PGA Championship</h3>

<p>The Black Course at Bethpage in Farmingdale, New York, will host the 101st PGA Championship May 13-19, 2019. This public course on Long Island, New York, is the most difficult of the five courses at Bethpage State Park, and is consistently ranked as one of the top courses in the United States. Bethpage Black hosted The U.S. Open in 2002 and 2009, The Barclays in 2012 and 2016, and will host the Ryder Cup in 2024.</p>

<h3 id="tickets">Tickets</h3>

<p>Ticket Registration for Bethpage Black’s first ever PGA Championship has concluded. All fans interested in purchasing tickets can now do so at <a href="http://www.pga.com/tickets" target="_blank">PGA.com/tickets</a>. </p>

<p>A variety of ticket options are available, including a limited number of “Ultimate Wanamaker” packages providing guaranteed access to Ryder Cup tickets when the most exciting event in golf comes to the Black Course at Bethpage in 2024. </p>