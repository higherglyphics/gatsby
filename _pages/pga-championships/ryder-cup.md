---
title: Ryder Cup
layout: default
order_number: 10
sidebar_label: Ryder Cup
---
<h3 id="team-golf-at-its-finest">Team Golf at its Finest</h3>

<p>The Ryder Cup is golf’s pre-eminent event, biennially bringing together 12-member teams from the U.S. and Europe, and where patriotism and national pride take center stage for a worldwide audience. This match-play event is played in five sessions over three days, and consists of two days of four-ball and foursome matches, and one day of singles matches.</p>

<p>Spanning 91 years and now 42 competitions, the Ryder Cup is among the last great professional sporting events where winning, and not prize money, is the reward.</p>

<h3 id="the-next-championship">The Next Championship</h3>

<p>The 2020 Ryder Cup will be held September 22-27 at Whistling Straits in Kohler, Wisconsin. This Pete Dye designed course is ranked among the best in the United States, and has hosted the 2004, 2010 and 2015 PGA Championships and the 2007 U.S. Senior Open. Chiseled out of the rugged Lake Michigan shoreline, this links-style course was will be the first public course to host the Ryder Cup in 29 years.</p>

<h3 id="tickets">Tickets</h3>

<p>Tickets for the 2020 Ryder Cup are not yet available for purchase, but if you would like to be among the first to know when tickets do become available, you can <a href="https://ems.pgalinks.com/2020RyderCupInterest.cfm" target="_blank">follow this link to sign up for updates</a>.</p>