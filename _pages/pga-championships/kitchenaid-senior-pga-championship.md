---
title: KitchenAid Senior PGA Championship
layout: default
order_number: 30
sidebar_label: KitchenAid Senior PGA Championship
---
<h3 id="senior-golfs-storied-major">Senior Golf’s Storied Major</h3>

<p>The KitchenAid Senior PGA Championship, the most historic and prestigious major championship in senior golf, will celebrate its 80th competition in 2019. The Championship was born in 1937 on the grounds of another of golf’s majors, at the invitation of one of the game’s greatest players. At the suggestion of renowned amateur Bobby Jones, the inaugural Senior PGA Championship was played at Augusta National Golf Club three years after the first Masters Tournament was held there.</p>

<p>This Championship was established to provide the opportunity for PGA Members ages 50 and over to compete with their peers, and brings together the legends of the game and the newest members of senior professional golf for seventy-two holes of stroke play.</p>

<h3 id="the-next-senior-championship">The Next Senior Championship</h3>

<p>Oak Hill Country Club in Pittsford, New York, will host the 80th KitchenAid Senior PGA Championship in 2019. The event will be played for a second time on Oak Hill’s historic East Course, which has staged three previous PGA Championships (1980, 2003, 2013), as well as other major and premier championships including the 1949 and 1998 U.S. Amateur, three U.S. Opens (1956, 1968, 1989), and the 1984 U.S. Senior Open. Jack Nicklaus, Lee Trevino, Cary Middlecoff, Curtis Strange and Jason Dufner headline Oak Hill’s roster of champions.</p>

<h3 id="tickets">Tickets</h3>

<p>The advance ticket window is closed for the 80th KitchenAid Senior PGA Championship, but if you would like to be among the first to know when tickets do become available, you can <a href="http://info.thepgaofamerica.com/acton/media/25751/2019-kitchenaid-senior-pga-championship" target="_blank">follow this link to sign up for updates</a>.</p>