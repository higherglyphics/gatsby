---
title: 'PGA of America Championships'
# layout: default-no-container
layout: default-no-title
permalink: /pga-championships/
order_number: 1
sidebar_label: 'PGA Championships'
exclude_from_sidebar: true
intro_image_path: /uploads/BDC.05.30.14.201_web.jpg
championships:
  - logo_path: /assets/images/logo-rc-2020.png
    logo_link: /pga-championships/ryder-cup
    event_name: 43rd Ryder Cup
    location: 'Whistling Straits in Kohler, Wisconsin'
  - logo_path: /assets/images/logo-champ-2019.png
    logo_link: /pga-championships/pga-championship
    event_name: 2019 PGA Championship
    location: 'Bethpage Black Course, Farmingdale, NY'
  - logo_path: /assets/images/logo-senior-2019.png
    logo_link: /pga-championships/kitchenaid-senior-pga-championship
    event_name: KitchenAid PGA Championship
    location: 'Oak Hill Country Club in Pittsford, NY'
  - logo_path: /assets/images/logo-kpmg-2019.png
    logo_link: /pga-championships/kpmg-womens-pga-championship
    event_name: KPMG PGA Championship
    location: 'Hazeltine National Golf Club in Chaska, Minnesota'
  - logo_path: /assets/images/logo-pro-champ.png
    logo_link: /pga-championships/pga-professional-championship
    event_name: 101st PGA Championship
    location: 'Belfair of Bluffton, South Carolina'
---
<main class="pt-2 pb-lg-5 mb-3 container">
	<h2 class="text-navy py-3">PGA of America Championships</h2>
	<div class="stack-on-mobile">
		<img src="/uploads/BDC.05.30.14.201_web.jpg" style="height: 251px;background-color: #EDF0F4;" class="img-fluid">
		<p>In addition to our spectator championships the PGA of America hosts numerous events for our members including the
			PGA Professional Championship, the Senior PGA Professional Championship, the PGA Jones Cup, the National Car Rental
			Assistant PGA Professional Championship, the PGA Tournament Series presented by Golf Advisor and the PGA Winter
			Championships presented by Golf Advisor and PrimeSport.</p>
		<p>The PGA of America also puts on a number of championships to highlight the future of golf including the PGA Jr.
			League Championship presented by National Car Rental, the PGA Collegiate Championship, the Junior Ryder Cup, and
			the Boy’s and Girl’s Junior PGA Championships.</p>
	</div>
</main>

<section class="bg-gray-light py-3">
	<div class="container">
		<h5 class="text-lg-center py-4 text-navy">Our Championship season promises to be the best in golf featuring:</h5>
			<div class="row text-lg-center">
<div class="col-lg">
					<div class="card">
						<div class="card-body">
							<div class="d-flex justify-content-center align-items-center">
								<img src="/assets/images/logo-rc-2020.png" width="141" height="141" class="img-lg-fluid">
							</div>
							<div class="d-flex justify-content-center">
								<a href="/pga-championships/ryder-cup">Learn More</a>
							</div>
						</div>
						<a class="tiles-link" href="/pga-championships/ryder-cup"></a>
					</div>
				</div>
				<div class="col-lg">
					<div class="card">
						<div class="card-body">
							<div class="d-flex justify-content-center align-items-center">
								<img src="/assets/images/logo-champ-2019.png" width="141" height="141" class="img-lg-fluid">
							</div>
							<div class="d-flex justify-content-center">
								<a href="/pga-championships/pga-championship">Learn More</a>
							</div>
						</div>
						<a class="tiles-link" href="/pga-championships/pga-championship"></a>
					</div>
				</div>
				<div class="col-lg">
					<div class="card">
						<div class="card-body">
							<div class="d-flex justify-content-center align-items-center">
								<img src="/assets/images/logo-senior-2019.png" width="141" height="141" class="img-lg-fluid">
							</div>
							<div class="d-flex justify-content-center">
								<a href="/pga-championships/kitchenaid-senior-pga-championship">Learn More</a>
							</div>
						</div>
						<a class="tiles-link" href="/pga-championships/kitchenaid-senior-pga-championship"></a>
					</div>
				</div>
				<div class="col-lg">
					<div class="card">
						<div class="card-body">
							<div class="d-flex justify-content-center align-items-center">
								<img src="/assets/images/logo-kpmg-2019.png" width="141" height="141" class="img-lg-fluid">
							</div>
							<div class="d-flex justify-content-center">
								<a href="/pga-championships/kpmg-womens-pga-championship">Learn More</a>
							</div>
						</div>
						<a class="tiles-link" href="/pga-championships/kpmg-womens-pga-championship"></a>
					</div>
				</div>
				<div class="col-lg">
					<div class="card">
						<div class="card-body">
							<div class="d-flex justify-content-center align-items-center">
								<img src="/assets/images/logo-pro-champ.png" width="141" height="141" class="img-lg-fluid">
							</div>
							<div class="d-flex justify-content-center">
								<a href="/pga-championships/pga-professional-championship">Learn More</a>
							</div>
						</div>
						<a class="tiles-link" href="/pga-championships/pga-professional-championship"></a>
					</div>
				</div>
				
			</div>
	</div>
</section>