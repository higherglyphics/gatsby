---
title: Leadership
jumbotron: true
layout: default-jumbo
navbar-secondary: leadership
permalink: /leadership/
image: /uploads/leadership/hero-leadership.jpg
---
<div class="leadership-landing-wpr">
    <div class="jumbotron jumbotron-fluid jumbotron-custom-md editable">
      <img class="jumbotron-image" src="/uploads/leadership/hero-leadership.jpg" data-src="/uploads/leadership/hero-leadership.jpg" alt="">
      <div class="jumbotron-body">
        <div class="container">
          <div class="row">
            <div class="col-8 col-md-7">
              <h1>PGA of America Executive Leadership and Board of Directors</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="section-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading mt-4 editable">
              <h3>Officers and Executives</h3>
            </div>
            <div class="row">
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Suzy_Whaley_2018_web.jpg" data-src="/uploads/executives/Suzy_Whaley_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> President</span>
                    <a href=""><h3>Suzy Whaley,  PGA/LPGA</h3></a>
                  </div>
                  <a href="/suzy-whaley" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Jim_Richerson_2018_web.jpg" data-src="/uploads/executives/Jim_Richerson_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Vice President</span>
                    <a href=""><h3>Jim Richerson,  PGA</h3></a>
                  </div>
                  <a href="/jim-richerson" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/John_Lindert_2018_web.jpg" data-src="/uploads/executives/John_Lindert_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Secretary</span>
                    <a href=""><h3>John Lindert,  PGA</h3></a>
                  </div>
                  <a href="/john-lindert" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Paul%20Levy_2018_web.jpg" data-src="/uploads/executives/Paul Levy_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Honorary President</span>
                    <a href=""><h3>Paul K. Levy,  PGA</h3></a>
                  </div>
                  <a href="/paul-levy" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Seth_Waugh_2018_web.jpg" data-src="/uploads/executives/Seth_Waugh_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Chief Executive Officer</span>
                    <a href=""><h3>Seth Waugh</h3></a>
                  </div>
                  <a href="/seth-waugh" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Darrell%20Crall_2018_web.jpg" data-src="/uploads/executives/Darrell Crall_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Chief Operating Officer</span>
                    <a href=""><h3>Darrell Crall</h3></a>
                  </div>
                  <a href="/darrell-crall" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/Kerry%20Haigh_2018_web.jpg" data-src="/uploads/executives/Kerry Haigh_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Chief Championships Officer</span>
                    <a href=""><h3>Kerry Haigh</h3></a>
                  </div>
                  <a href="/kerry-haigh" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles tiles-square">
                  <div class="tiles-image">
                    <img src="/uploads/executives/John_Easterbrook_2018_web.jpg" data-src="/uploads/executives/John_Easterbrook_2018_web.jpg" height="233" alt="">
                  </div>
                  <div class="tiles-body">
                    <span class="tiles-label"> Chief Membership Officer</span>
                    <a href=""><h3>John Easterbrook, PGA</h3></a>
                  </div>
                  <a href="/john-easterbrook" class="tiles-link"></a>
                </div>
              </div>
              
              <div class="col-lg-12">
                <a class="editor-link" href="cloudcannon:collections/_data/officers.yml">Edit officers</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading mt-4">
              <div class="editable">
                <h3>Board of Directors</h3>
                <p>The PGA Board of Directors includes representatives from each of the PGA's 14 districts, two independent directors and a member of the PGA Tour. Each District is comprised of the PGA Sections listed under the District Director.</p>
              </div>
            </div>
            <div class="row">
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Noel%20Gebauer_cropped.jpg" data-src="/uploads/directors/Noel Gebauer_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Noel Gebauer, PGA</h3>
                      <span class="tiles-label"> District 1</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:ngebauer@pga.com">ngebauer@pga.com</a>
                      <br> Connecticut <br> New England <br> Northeastern New York
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Tom%20Henderson_cropped.jpg" data-src="/uploads/directors/Tom Henderson_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Tom Henderson, PGA</h3>
                      <span class="tiles-label"> District 2</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:quandegot@gmail.com">quandegot@gmail.com</a>
                      <br> Metropolitan <br> New Jersey <br> Philadelphia
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/John_Bridgeman_2018_web.jpg" data-src="/uploads/directors/John_Bridgeman_2018_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>John Bridgeman, PGA</h3>
                      <span class="tiles-label"> District 3</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:johnny@bccgolf.org">johnny@bccgolf.org</a>
                      <br> Alabama-NW Florida <br> Gulf States <br> Tennessee
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Steven%20Aloi_cropped.jpg" data-src="/uploads/directors/Steven Aloi_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Steven Aloi, PGA</h3>
                      <span class="tiles-label"> District 4</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:stevepro63@aol.com">stevepro63@aol.com</a>
                      <br> Central New York <br> Tri-State <br> Western New York
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Ronald_Osborne_web.jpg" data-src="/uploads/directors/Ronald_Osborne_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Ronald Osborne, PGA</h3>
                      <span class="tiles-label"> District 5</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:g2spga@hotmail.com">g2spga@hotmail.com</a>
                      <br> Michigan <br> Northern Ohio <br> Southern Ohio
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Terry_Russell_2018_web.jpg" data-src="/uploads/directors/Terry_Russell_2018_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Terry Russell, PGA</h3>
                      <span class="tiles-label"> District 6</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:trusspga2@aol.com">trusspga2@aol.com</a>
                      <br> Illinois <br> Indiana <br> Wisconsin
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Nathan_Charnes_2018_web.jpg" data-src="/uploads/directors/Nathan_Charnes_2018_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Nathan Charnes, PGA</h3>
                      <span class="tiles-label"> District 7</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:ncharnes@winghavencc.com">ncharnes@winghavencc.com</a>
                      <br> Gateway <br> Midwest <br> South Central
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/David%20Schneider_cropped.jpg" data-src="/uploads/directors/David Schneider_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>David Schneider, PGA</h3>
                      <span class="tiles-label"> District 8</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:daves@happyhollowclub.com">daves@happyhollowclub.com</a>
                      <br> Iowa <br> Minnesota <br> Nebraska
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Ron%20Rawls_cropped.jpg" data-src="/uploads/directors/Ron Rawls_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Ron Rawls, PGA</h3>
                      <span class="tiles-label"> District 9</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:ron@cranecreekcc.com">ron@cranecreekcc.com</a>
                      <br> Colorado <br> Rocky Mountain <br> Utah
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Kelly%20Williams_cropped.jpg" data-src="/uploads/directors/Kelly Williams_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Kelly Williams, PGA</h3>
                      <span class="tiles-label"> District 10</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:kwilliams@greenbriergcc.com">kwilliams@greenbriergcc.com</a>
                      <br> Carolinas <br> Kentucky <br> Middle Atlantic
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Bill_Troyanoski_2018_web.jpg" data-src="/uploads/directors/Bill_Troyanoski_2018_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Bill Troyanoski, PGA</h3>
                      <span class="tiles-label"> District 11</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:btroyanoski@hmbgolflinks.com">btroyanoski@hmbgolflinks.com</a>
                      <br> Aloha <br> Northern California <br> Southern California
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Tony_Martinez_web.jpg" data-src="/uploads/directors/Tony_Martinez_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Tony Martinez, PGA</h3>
                      <span class="tiles-label"> District 12</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:tony@tmgolfmanagement.com">tony@tmgolfmanagement.com</a>
                      <br> Northern Texas <br> Southern Texas <br> Sun Country
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Patrick%20Richardson_cropped.jpg" data-src="/uploads/directors/Patrick Richardson_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Patrick Richardson, PGA</h3>
                      <span class="tiles-label"> District 13</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:patrickrichardson@pga.com">patrickrichardson@pga.com</a>
                      <br> Georgia <br> North Florida <br> South Florida
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Don%20Rea_cropped.jpg" data-src="/uploads/directors/Don Rea_cropped.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Don Rea Jr., PGA</h3>
                      <span class="tiles-label"> District 14</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:drea@augustaranchgolf.com">drea@augustaranchgolf.com</a>
                      <br> Pacific Northwest <br> Southwest <br> 
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/renee-powell-web.jpg" data-src="/uploads/renee-powell-web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Renee Powell, PGA/LPGA</h3>
                      <span class="tiles-label"> At-Large Director</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:cv46@aol.com">cv46@aol.com</a>
                      <br>  <br>  <br> 
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Christopher_Liedel-magic.jpg" data-src="/uploads/directors/Christopher_Liedel-magic.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Christopher Liedel</h3>
                      <span class="tiles-label"> Independent Director</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:kflores@pgahq.com">kflores@pgahq.com</a>
                      <br> Chief Executive Officer <br> U.S. Olympic Museum and Hall of Fame <br> 
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/Andrea_Smith_web.jpg" data-src="/uploads/directors/Andrea_Smith_web.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Andrea B. Smith</h3>
                      <span class="tiles-label"> Independent Director</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:kflores@pgahq.com">kflores@pgahq.com</a>
                      <br> Chief Administrative Officer <br> Bank of America <br> 
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-4 col-md-6">
                <div class="tiles-wrapper mb-4">
                  <div class="tiles tiles-square mb-0">
                    <div class="tiles-image mb-0">
                      <img src="/uploads/directors/board-player-johnson-wagner.jpg" data-src="/uploads/directors/board-player-johnson-wagner.jpg" height="233" alt="">
                    </div>
                    <div class="tiles-body">
                      <h3>Johnson Wagner, PGA</h3>
                      <span class="tiles-label"> Player Director</span>
                    </div>
                  </div>
                  <div class="tiles-footer">
                    <p class="mb-0">
                      <a href="mailto:kflores@pgahq.com">kflores@pgahq.com</a>
                      <br> PGA Tour <br>  <br> 
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-12">
                <a class="editor-link" href="cloudcannon:collections/_data/directors.yml">Edit directors</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>