---
title: Case Study - How PGA Career Services Helped One Club Find a New GM
layout: default
order_number: 40
sidebar_label: 'Career Services Case Study #2'
---
<p>Oakmont Country Club in Glendale, California, is a storied facility, having been established in 1922.
The private equity club recently took a very modern approach to hiring a new general manager by turning to PGA Career Services for help with the search.</p>

<p>Over the course of three months, PGA Career Consultant Ken Farrell worked closely with Oakmont President Christopher Westhoff to find the right person to fill the vacant General Manager/COO position at the club, which has 450 golf members and has hosted two LPGA Tour events. The search included nine candidates, and ultimately ended with the hiring of PGA Professional Craig Cliver, who started at Oakmont last month.</p>

<p>Oakmont had never had a PGA Professional as its general manager, but Westhoff sees a definite benefit to hiring a PGA Member to oversee the club.</p>

<p>“We’re not running a dinner club or a social club – this is a golf club. If you don’t have someone in that GM/COO position that doesn’t understand how the golf course and the golf shop, and knows how to work with a superintendent and the assets of the golf operation, then you’re really missing a piece of the puzzle,” Westhoff says. “Having a PGA Professional who knows how to handle food &amp; beverage, a swimming pool, gym and activities really augments everything the members want from the club.”</p>

<p>Westhoff and his fellow board members had not undertaken a job search for a general manager in their time leading Oakmont. They were glad to be referred to Farrell, the PGA Career Consultant for Southern California, at the outset of the search process. Farrell worked with Westhoff and the Oakmont board through a thorough process that started with identifying qualified candidates, helping set up and conduct interviews, and performing background checks on the final four potential hires.</p>

<p>In the end, Oakmont hired Cliver after the board unanimously approved the move following multiple interviews and meetings with club employees and members.</p>

<p>“We had no real blueprint on how to hire such an important position, and it’s not an easy scenario to navigate if you don’t have HR experience,” Westhoff says. “Thanks to Ken and PGA Career Services, the process could not have worked out any better than it did. Ken understands the market, knows about Oakmont and understood what we were looking for. Ken provided a lot of direction, and PGA Career Services made our jobs as a board much easier. I would recommend this approach to any club looking to fill a golf management position.”</p>

<p>For additional information on how Career Consultants can help your company please reach out to  <a href="/career-services/">your Career Consultant</a></p>