---
title: Case Study - Career Services Leads to Successful Searches
layout: default
order_number: 30
sidebar_label: 'Career Services Case Study #1'
---
<p>When PGA Head Professional Tim O’Neill retired from North Shore Country Club in Glenview, Illinois, the club retained PGA Career Services due to the PGA’s vast database of industry professionals and its reputation for hands-on service. Together, they defined parameters, as each employer can pinpoint the size and scope of their search.</p>

<p>A pool of potential candidates was identified and an announcement was initially sent to 5,000? golf industry professionals. The Committee then granted 14 interviews (all via Skype) from 50 applicants. From there, three finalists were named, who were interviewed at the club.</p>

<p>A PGA Career Consultant worked with the club to ensure that each applicant was given equal consideration. Eventually, North Shore selected PGA Professional Shaun McElroy as its new Head Professional.</p>

<p>“They exceeded our expectations of how they were going to take us throughout the whole process,” says Patrick Shea, Search Committee Chair and Golf Chairman for North Shore. “It was fantastic – it was the most thorough and diligent process laid out. I’d recommend it to any club.”</p>

<p>For additional information on how Career Consultants can help your company please reach out to <a href="/career-services/">your Career Consultant</a></p>