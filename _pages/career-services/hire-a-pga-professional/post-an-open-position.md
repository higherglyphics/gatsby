---
title: How to Post an Open Position
layout: default
order_number: 20
sidebar_label: Posting Open Positions
---
<p>Employers of PGA Professionals reap the rewards of unrivaled marketing and operational benefits.  Selecting the best PGA Professional for your facility is one of the most important decisions you will make. </p>

<h3 id="job-postings">Job Postings</h3>

<p>The PGA of America offers complimentary hiring and job posting assistance for employers looking to hire PGA Professionals, Associates or Students.</p>

<p>To post a non-management level position, visit our PGA Job Board link to <a href="https://jobs.pga.org/post">Post a Job</a>.  If you do not have an account to post jobs, please complete the <a href="https://apps.pga.org/career/employer/index.cfm" target="_blank">Employer Registration Form</a>.</p>

<p>To post a management level position, please <a href="/career-services/">contact the Career Consultant</a> in your section for further guidance and assistance.</p>

<p>For additional assistance, contact PGA Career Services at (800) 314-2713 or <a href="mailto:careerservices@pgahq.com?subject=Career%20Services%20Inquiry">careerservices@pgahq.com</a>.</p>

<h3 id="internships">Internships</h3>

<p>As a graduation requirement, The PGA of America requires all students enrolled in an accredited PGA Golf Management University Program to complete 16 months of internship at facilities approved as eligible employment. If you would like to learn more about hosting an intern at your facility or to post a position, please contact PGA Golf Management University Program Staff directly.</p>

<p>Click here for <a href="/Document-Library/PGA%20PGM%20University%20Program%20Internship%20Contacts.docx" target="_blank">a full contact roster of accredited PGA Golf Management University Programs</a>.</p>