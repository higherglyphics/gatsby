---
title: PGA Career Consultants
layout: default
sidebar_label: Career Consultant Directory
permalink: /career-services/
header_html: >-
  <p>PGA Career Services helps golf industry and PGA Professionals advance into
  higher level employment opportunities including general management roles and
  non-traditional career paths; assists employers throughout the hiring process;
  promotes the immeasurable benefits of employing a qualified Professional and
  provides resources for individuals seeking to gain employment in the golf
  industry.</p><p>With an expanded team of 19 PGA Career Consultants, Career
  Services offers a toolbox of invaluable resources to meet the hiring
  requirements demanded by employers today.&nbsp; Core services, which span your
  entire career, include career planning, coaching, negotiating, resume writing,
  interviewing, networking, compensation, education, certification and
  reporting.</p>
consultants:
  - name: 'Jason Boaz, PGA'
    image: /uploads/career-consultants/Jason Boaz.jpg
    sections:
      - Illinois
      - Wisconsin
    email: jboaz@pgahq.com
    city: Sun Prairie
    state: WI
    zip: '53590'
    phone: (608) 318-5355
  - name: 'Jeff Drimel, PGA'
    image: /uploads/career-consultants/Jeff Drimel.jpg
    sections:
      - Iowa
      - Minnesota
      - Nebraska
    email: jdrimel@pgahq.com
    city: Saint Michael
    state: MN
    zip: '55376'
    phone: (561) 386-7715
  - name: 'Ken Ferrell, PGA'
    image: /uploads/career-consultants/Ken Ferrell.jpg
    sections:
      - Aloha
      - Southern California
    email: kferrell@pgahq.com
    city: Wildomar
    state: CA
    zip: '92595'
    phone: (951) 324-3665
  - name: 'Keith Fisher, PGA'
    image: /uploads/career-consultants/Keith Fisher.jpg
    sections:
      - Kentucky
      - Northern Ohio
      - Southern Ohio
    email: kfisher@pgahq.com
    city: Dayton
    state: OH
    zip: '45459'
    phone: (937) 475-4509
  - name: 'Kelly Gilley, PGA'
    image: /uploads/career-consultants/Kelly Gilley.jpg
    sections:
      - Northern Texas
      - Southern Texas
    email: kgilley@pgahq.com
    city: Spring
    state: TX
    zip: '77382'
    phone: (832) 458-8112
  - name: 'Jonathan Gold, PGA'
    image: /uploads/career-consultants/Jonathan Gold.jpg
    sections:
      - Metropolitan
      - New Jersey
      - Philadelphia
    email: jgold@pgahq.com
    city: Westbury
    state: NY
    zip: '11590'
    phone: (516) 567-6918
  - name: 'Kathy Grayson, PGA'
    image: /uploads/career-consultants/Kathy Grayson.jpg
    sections:
      - South Florida
    email: kgrayson@pgahq.com
    city: Naples
    state: FL
    zip: '34119'
    phone: (239) 207-9181
  - name: 'Monte Koch, PGA'
    image: /uploads/career-consultants/Monte Koch.jpg
    sections:
      - Pacific Northwest
      - Rocky Mountain
    email: mkoch@pgahq.com
    city: Auburn
    state: WA
    zip: '98092'
    phone: (206) 335-5260
  - name: 'Chris Kulinski, PGA'
    image: /uploads/career-consultants/Chris_Kulinski.jpg
    sections:
      - Central New York
      - Western New York
      - NE New York
    email: CKulinski@pgahq.com
    city: 'West Seneca,'
    state: NY
    zip: '14206'
    phone: (716) 316-4160
  - name: 'Bruce Lubach, PGA'
    image: /uploads/career-consultants/Bruce Lubach.jpg
    sections:
      - Southwest
      - Sun Country
    email: blubach@pgahq.com
    city: Mesa
    state: AZ
    zip: '85212'
    phone: (402) 802-8241
  - name: 'Mike McCollum, PGA'
    image: /uploads/career-consultants/Mike McCollum.jpg
    sections:
      - Georgia
      - North Florida
    email: mmccollum@pgahq.com
    city: Columbus
    state: GA
    zip: '31909'
    phone: (706) 563-9773
  - name: 'Michael Mueller, PGA'
    image: /uploads/career-consultants/Michael Mueller.jpg
    sections:
      - Carolinas
    email: mmueller@pgahq.com
    city: Fuquay Varina
    state: NC
    zip: '27526'
    phone: (919) 552-3781
  - name: 'Carol Pence, PGA'
    image: /uploads/career-consultants/Carol Pence.jpg
    sections:
      - Northern California
    email: cpence@pgahq.com
    city: Santa Rosa
    state: CA
    zip: '95409'
    phone: (707) 843-5033
  - name: 'Jim Remy, PGA'
    image: /uploads/career-consultants/Jim Remy.jpg
    sections:
      - Connecticut
      - New England
    email: jremy@pgahq.com
    city: Ludlow
    state: VT
    zip: 05149
    phone: (561) 268-3946
  - name: 'Todd Smith, PGA'
    image: /uploads/career-consultants/Todd Smith.jpg
    sections:
      - Michigan
      - Indiana
    email: tsmith@pgahq.com
    city: Peru
    state:
    zip: '46970'
    phone: (561) 371-2778
  - name: 'Keith Soriano, PGA'
    image: /uploads/career-consultants/Keith Soriano.jpg
    sections:
      - Colorado
      - Utah
    email: ksoriano@pgahq.com
    city: Thornton
    state: CO
    zip: '80602'
    phone: (720) 841-1006
  - name: 'Greg Stenzel, PGA'
    image: /uploads/career-consultants/Greg Stenzel.jpg
    sections:
      - Middle Atlantic
      - Tri-State
    email: gstenzel@pgahq.com
    city: Raleigh
    state: NC
    zip: '27614'
    phone: (561) 379-7724
  - name: Sean Thornberry
    image: /uploads/career-consultants/Sean_Thornberry.jpg
    sections:
      - Global
    email: sthornberry@pgahq.com
    city: Palm Beach Gardens
    state: FL
    zip: '33418'
    phone: (561) 624-7630
  - name: 'Doug Turner, PGA'
    image: /uploads/career-consultants/Doug Turner.jpg
    sections:
      - Gateway
      - Midwest
      - South Central
    email: dturner@pgahq.com
    city: Little Rock
    state: AR
    zip: '72223'
    phone: (972) 977-2746
  - name: 'Kevin Walls, PGA'
    image: /uploads/career-consultants/Kevin Walls.jpg
    sections:
      - Alabama-NW Florida
      - Gulf States
      - Tennessee
    email: kwalls@pgahq.com
    city: Pontotoc
    state: MS
    zip: '38863'
    phone: (662) 489-1528
---
<div class="container career-consultants">
  <div class="row">
    <div class="col-md-12">
      <div class="career-consultants-header-text">
<p>PGA Career Services helps golf industry and PGA Professionals advance into higher level employment opportunities including general management roles and non-traditional career paths; assists employers throughout the hiring process; promotes the immeasurable benefits of employing a qualified Professional and provides resources for individuals seeking to gain employment in the golf industry.</p>
<p>With an expanded team of 19 PGA Career Consultants, Career Services offers a toolbox of invaluable resources to meet the hiring requirements demanded by employers today.  Core services, which span your entire career, include career planning, coaching, negotiating, resume writing, interviewing, networking, compensation, education, certification and reporting.</p> </div>
    </div>
  </div>
  <div class="row">
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Jason%20Boaz.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Jason Boaz, PGA</div>
      <p class="text-muted lead mb-0">Sun Prairie, WI 53590</p>
      <p class="text-muted lead mb-0">(608) 318-5355</p>
      <a class="email" href="mailto:jboaz@pgahq.com">jboaz@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Illinois;
          
            Wisconsin
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Jeff%20Drimel.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Jeff Drimel, PGA</div>
      <p class="text-muted lead mb-0">Saint Michael, MN 55376</p>
      <p class="text-muted lead mb-0">(561) 386-7715</p>
      <a class="email" href="mailto:jdrimel@pgahq.com">jdrimel@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Iowa;
          
            Minnesota;
          
            Nebraska
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Ken%20Ferrell.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Ken Ferrell, PGA</div>
      <p class="text-muted lead mb-0">Wildomar, CA 92595</p>
      <p class="text-muted lead mb-0">(951) 324-3665</p>
      <a class="email" href="mailto:kferrell@pgahq.com">kferrell@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Aloha;
          
            Southern California
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Keith%20Fisher.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Keith Fisher, PGA</div>
      <p class="text-muted lead mb-0">Dayton, OH 45459</p>
      <p class="text-muted lead mb-0">(937) 475-4509</p>
      <a class="email" href="mailto:kfisher@pgahq.com">kfisher@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Kentucky;
          
            Northern Ohio;
          
            Southern Ohio
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Kelly%20Gilley.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Kelly Gilley, PGA</div>
      <p class="text-muted lead mb-0">Spring, TX 77382</p>
      <p class="text-muted lead mb-0">(832) 458-8112</p>
      <a class="email" href="mailto:kgilley@pgahq.com">kgilley@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Northern Texas;
          
            Southern Texas
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Jonathan%20Gold.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Jonathan Gold, PGA</div>
      <p class="text-muted lead mb-0">Westbury, NY 11590</p>
      <p class="text-muted lead mb-0">(516) 567-6918</p>
      <a class="email" href="mailto:jgold@pgahq.com">jgold@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Metropolitan;
          
            New Jersey;
          
            Philadelphia
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Kathy%20Grayson.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Kathy Grayson, PGA</div>
      <p class="text-muted lead mb-0">Naples, FL 34119</p>
      <p class="text-muted lead mb-0">(239) 207-9181</p>
      <a class="email" href="mailto:kgrayson@pgahq.com">kgrayson@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            South Florida
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Monte%20Koch.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Monte Koch, PGA</div>
      <p class="text-muted lead mb-0">Auburn, WA 98092</p>
      <p class="text-muted lead mb-0">(206) 335-5260</p>
      <a class="email" href="mailto:mkoch@pgahq.com">mkoch@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Pacific Northwest;
          
            Rocky Mountain
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Chris_Kulinski.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Chris Kulinski, PGA</div>
      <p class="text-muted lead mb-0">West Seneca,, NY 14206</p>
      <p class="text-muted lead mb-0">(716) 316-4160</p>
      <a class="email" href="mailto:CKulinski@pgahq.com">CKulinski@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Central New York;
          
            Western New York;
          
            NE New York
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Bruce%20Lubach.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Bruce Lubach, PGA</div>
      <p class="text-muted lead mb-0">Mesa, AZ 85212</p>
      <p class="text-muted lead mb-0">(402) 802-8241</p>
      <a class="email" href="mailto:blubach@pgahq.com">blubach@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Southwest;
          
            Sun Country
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Mike%20McCollum.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Mike McCollum, PGA</div>
      <p class="text-muted lead mb-0">Columbus, GA 31909</p>
      <p class="text-muted lead mb-0">(706) 563-9773</p>
      <a class="email" href="mailto:mmccollum@pgahq.com">mmccollum@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Georgia;
          
            North Florida
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Michael%20Mueller.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Michael Mueller, PGA</div>
      <p class="text-muted lead mb-0">Fuquay Varina, NC 27526</p>
      <p class="text-muted lead mb-0">(919) 552-3781</p>
      <a class="email" href="mailto:mmueller@pgahq.com">mmueller@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Carolinas
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Carol%20Pence.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Carol Pence, PGA</div>
      <p class="text-muted lead mb-0">Santa Rosa, CA 95409</p>
      <p class="text-muted lead mb-0">(707) 843-5033</p>
      <a class="email" href="mailto:cpence@pgahq.com">cpence@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Northern California
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Jim%20Remy.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Jim Remy, PGA</div>
      <p class="text-muted lead mb-0">Ludlow, VT 05149</p>
      <p class="text-muted lead mb-0">(561) 268-3946</p>
      <a class="email" href="mailto:jremy@pgahq.com">jremy@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Connecticut;
          
            New England
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Todd%20Smith.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Todd Smith, PGA</div>
      <p class="text-muted lead mb-0">Peru,  46970</p>
      <p class="text-muted lead mb-0">(561) 371-2778</p>
      <a class="email" href="mailto:tsmith@pgahq.com">tsmith@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Michigan;
          
            Indiana
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Keith%20Soriano.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Keith Soriano, PGA</div>
      <p class="text-muted lead mb-0">Thornton, CO 80602</p>
      <p class="text-muted lead mb-0">(720) 841-1006</p>
      <a class="email" href="mailto:ksoriano@pgahq.com">ksoriano@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Colorado;
          
            Utah
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Greg%20Stenzel.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Greg Stenzel, PGA</div>
      <p class="text-muted lead mb-0">Raleigh, NC 27614</p>
      <p class="text-muted lead mb-0">(561) 379-7724</p>
      <a class="email" href="mailto:gstenzel@pgahq.com">gstenzel@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Middle Atlantic;
          
            Tri-State
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Sean_Thornberry.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Sean Thornberry</div>
      <p class="text-muted lead mb-0">Palm Beach Gardens, FL 33418</p>
      <p class="text-muted lead mb-0">(561) 624-7630</p>
      <a class="email" href="mailto:sthornberry@pgahq.com">sthornberry@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Global
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Doug%20Turner.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Doug Turner, PGA</div>
      <p class="text-muted lead mb-0">Little Rock, AR 72223</p>
      <p class="text-muted lead mb-0">(972) 977-2746</p>
      <a class="email" href="mailto:dturner@pgahq.com">dturner@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Gateway;
          
            Midwest;
          
            South Central
          
        </strong></p>
    </div>
  </div>
</div>
    
       <div class="col-lg-4 col-md-6">
  <div class="career-consultant-tiles career-consultant-tiles-square">
    <div class="career-consultant-tiles-image">
      <img src="/uploads/career-consultants/Kevin%20Walls.jpg">
    </div>
    <div class="career-consultant-tiles-body">
      <div class="name">Kevin Walls, PGA</div>
      <p class="text-muted lead mb-0">Pontotoc, MS 38863</p>
      <p class="text-muted lead mb-0">(662) 489-1528</p>
      <a class="email" href="mailto:kwalls@pgahq.com">kwalls@pgahq.com <i class="icon icon-arrow-right"></i></a>
        <p class="lead mb-0"><strong>
          
            Alabama-NW Florida;
          
            Gulf States;
          
            Tennessee
          
        </strong></p>
    </div>
  </div>
</div>
    
  </div>
</div>