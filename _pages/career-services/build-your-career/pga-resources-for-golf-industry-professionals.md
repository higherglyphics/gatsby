---
title: PGA Resources for Golf Industry Professionals
layout: default
order_number: 20
sidebar_label: Resources for Professionals
---
<p>The PGA of America offers a variety of resources to help all golf industry professionals build their careers. Here are some of the resources available to help reach short- and long-term career goals:</p>

<p><strong><a href="/career-services/">PGA Career Consultants</a>:</strong> There are 19 PGA Career Consultants serving geographic regions around the country. Making an appointment to talk with your PGA Career Consultant is a strong first step in career planning.</p>

<p><strong>Career Planning Handbook:</strong> This new resource, created by PGA Career Consultant Michael Mueller, is a workbook that helps golf industry professionals navigate a straightforward eight-step process to setting goals and enhancing their careers. This handbook is only available from PGA Career Consultants.</p>

<p><strong><a href="https://jobs.pga.org">PGA Job Board</a>:</strong> Along with PGA CareerLinks, the new PGA Job Board is available at PGA.org for golf industry professionals to find a wide variety of different job openings within the sport. Thousands of open positions are posted each year.</p>

<p><strong>PGA Education:</strong> From education seminars at the Section and Chapter level to the Education Conferences at the PGA Merchandise Show – as well as a wealth of information available on PGA.org – PGA Education is a valuable resource in the career paths of Golf Operations, Executive Management and Teaching &amp; Coaching.</p>

<p>For additional information and more career advice please reach out to <a href="/career-services/">your Career Consultant</a></p>