---
title: Working with a Career Consultant
layout: default
order_number: 40
sidebar_label: Working with a Career Consultant
---
<p>Use a Career Consultant early and often. A Career Consultants can absolutely help you when you are looking to make a move, but the best use of a Career Consultants is to start looking 3, 5 and 10 years down the road and using their expertise to help identify where you want to go in your career and how you are going to get there.</p>

<p>If you look at the team of Career Consultants, the diversity of career backgrounds is significant. There isn’t a career track in the industry that has not been covered by one of the Career Consultants. Career Consultants are uniquely positioned to identify the skills and experiences that need to be developed between your current job and where you want to be in the future. Then when you are ready for it, and the job opportunity presents itself, Career Consultants can talk about getting ready for the job search and pursuing that opportunity. Career Consultants can then help with the basics of resumes, cover letters and interview prep.</p>

<p>The first building block needed for career path planning is creating the relationship that allows your Career Consultant to understand what your needs are and what your goals and passions are. Once the relationship has been establish, here are some of the things that Career Consultants can help you with:</p>

<ol>
  <li>
    <p>Identifying guiding principles and values</p>
  </li>
  <li>
    <p>Self-assessment and goal setting</p>
  </li>
  <li>
    <p>Skill gap analysis</p>
  </li>
  <li>
    <p>Creating a development plan</p>
  </li>
  <li>
    <p>Job search readiness</p>
  </li>
  <li>
    <p>Pursuing those opportunities</p>
  </li>
</ol>

<p>For additional information and more career advice please reach out to <a href="/career-services/">your Career Consultant</a></p>