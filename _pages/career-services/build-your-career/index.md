---
title: Building Your Career with Career Services
layout: default
order_number: 10
sidebar_label: Build Your Career
permalink: /career-services/build-your-career/
---
<p>Whether you are preparing for a new job, looking to excel in your current position or planning for life after your career in the golf industry, building and maintaining a relationship with your PGA Career Consultant is an invaluable asset in achieving your goals.</p>

<p>Our team of seasoned Career Consultants bring a wealth of diverse industry knowledge and experience to PGA Career Services. With an expanded team of 19 field consultants, our territories are more focused, which affords us the opportunity to spend more time with you – allowing us to better understand your career goals and provide you with a strategic roadmap to meet those goals.</p>

<h3 id="what-to-expect">What to Expect?</h3>

<p>Your Career Consultant will guide you every step of the way through the PGA Career Planning process, an individualized long, term, relationship-based program designed to allow us to best serve you.</p>

<ul>
  <li>Self-Assessment and Goal Setting</li>
  <li>Skills Gap Analysis</li>
  <li>Career Plan Development</li>
  <li>Communicating Your Value</li>
  <li>Pursuing Opportunities</li>
  <li>Achieving and Leading</li>
</ul>

<p>Position yourself for growth and success throughout your career, contact your <a href="/career-services/">Career Consultant today</a>.</p>