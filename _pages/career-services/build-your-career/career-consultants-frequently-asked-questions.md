---
title: Career Consultant FAQs
layout: faq
order_number: 10
sidebar_label: Career Consultant FAQs
questions:
  - question: What is PGA Career Services?
    answer_markdown: >-
      PGA Career Services, a component of The PGA of America’s long-term
      strategic plan formerly known as the PGA Employment department, is an
      expanded team of field consultants committed to serving the PGA Member and
      executing daily Career Services operations.


      [Learn More
      &gt;&nbsp;](/career-services/)
  - question: When should I contact my Career Consultant?
    answer_markdown: |-
      At any stage of your golf career:

      1. Preparing for a new job
      2. Looking to excel in your current position
      3. Planning for life after your golf career

      [Learn More &gt;&nbsp;](/career-services/)
  - question: What can my Career Consultant do for me?
    answer_markdown: |-
      PREPARE FOR A JOB

      * Resume review and prep
      * Interview tips
      * Stand out in the crowd – differentiate yourself in the market place
      * Build a career plan
      * Exciting new opportunities in the game, - TopGolf, GOLFTEC, etc..

      EXCEL AT YOUR JOB

      * Lifelong learning
      * Negotiating your next agreement, compensation details
      * Know your employer needs
      * Create your goals
      * Take the next step

      RETIRE FROM YOUR CAREER

      * Exit strategy, Retirement planning

      [Find my Career Consultant &gt;](/career-services/)
  - question: Who is my career consultant?
    answer_markdown: >-
      Your Career Consultant is determined based on your section. [Click
      here](/career-services/) to view the list of career consultants, their
      sections covered and their contact information.
---
<div class="mb-4" id="accordion">
    <div class="card mb-0">
      <div aria-controls="collapse1" aria-expanded="true" class="card-header pointer d-flex justify-content-between align-items-center collapsed" data-target="#collapse1" data-toggle="collapse" id="heading1">
        <p class="my-3 fw-600 d-inline-block">What is PGA Career Services?</p><i class="icon icon-arrow text-navy collapsed" data-target="#collapse1" data-toggle="collapse"></i>
      </div>
      <div aria-labelledby="heading1" class="collapse" data-parent="#accordion" id="collapse1">
        <div class="card-body">
          <p class="mb-0"></p>
          <p>PGA Career Services, a component of The PGA of America’s long-term strategic plan formerly known as the PGA Employment department, is an expanded team of field consultants committed to serving the PGA Member and executing daily Career Services operations.</p>
          <p><a href="/career-services/">Learn More &gt;&nbsp;</a></p>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <div aria-controls="collapse2" aria-expanded="true" class="card-header pointer d-flex justify-content-between align-items-center collapsed" data-target="#collapse2" data-toggle="collapse" id="heading2">
        <p class="my-3 fw-600 d-inline-block">When should I contact my Career Consultant?</p><i class="icon icon-arrow text-navy collapsed" data-target="#collapse2" data-toggle="collapse"></i>
      </div>
      <div aria-labelledby="heading2" class="collapse" data-parent="#accordion" id="collapse2">
        <div class="card-body">
          <p class="mb-0"></p>
          <p>At any stage of your golf career:</p>
          <ol>
            <li>Preparing for a new job</li>
            <li>Looking to excel in your current position</li>
            <li>Planning for life after your golf career</li>
          </ol>
          <p><a href="/career-services/">Learn More &gt;&nbsp;</a></p>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <div aria-controls="collapse3" aria-expanded="true" class="card-header pointer d-flex justify-content-between align-items-center collapsed" data-target="#collapse3" data-toggle="collapse" id="heading3">
        <p class="my-3 fw-600 d-inline-block">What can my Career Consultant do for me?</p><i class="icon icon-arrow text-navy collapsed" data-target="#collapse3" data-toggle="collapse"></i>
      </div>
      <div aria-labelledby="heading3" class="collapse" data-parent="#accordion" id="collapse3">
        <div class="card-body">
          <p class="mb-0"></p>
          <p>PREPARE FOR A JOB</p>
          <ul>
            <li>Resume review and prep</li>
            <li>Interview tips</li>
            <li>Stand out in the crowd – differentiate yourself in the market place</li>
            <li>Build a career plan</li>
            <li>Exciting new opportunities in the game, - TopGolf, GOLFTEC, etc..</li>
          </ul>
          <p>EXCEL AT YOUR JOB</p>
          <ul>
            <li>Lifelong learning</li>
            <li>Negotiating your next agreement, compensation details</li>
            <li>Know your employer needs</li>
            <li>Create your goals</li>
            <li>Take the next step</li>
          </ul>
          <p>RETIRE FROM YOUR CAREER</p>
          <ul>
            <li>Exit strategy, Retirement planning</li>
          </ul>
          <p><a href="/career-services/">Find my Career Consultant &gt;</a></p>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <div aria-controls="collapse4" aria-expanded="true" class="card-header pointer d-flex justify-content-between align-items-center collapsed" data-target="#collapse4" data-toggle="collapse" id="heading4">
        <p class="my-3 fw-600 d-inline-block">Who is my career consultant?</p><i class="icon icon-arrow text-navy collapsed" data-target="#collapse4" data-toggle="collapse"></i>
      </div>
      <div aria-labelledby="heading4" class="collapse" data-parent="#accordion" id="collapse4">
        <div class="card-body">
          <p class="mb-0"></p>
          <p>Your Career Consultant is determined based on your section. <a href="/career-services/">Click here</a> to view the list of career consultants, their sections covered and their contact information.</p>
        </div>
      </div>
    </div>
  </div>