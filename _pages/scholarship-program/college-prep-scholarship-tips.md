---
title: College Preparation Scholarship Tips
layout: default
order_number: 20
sidebar_label: College Prep Tips
---
<h3 id="college-prep-checklists">College Prep Checklists</h3>

<p>Whether you’re a student or parent, years away from college or just about to start, the following checklists will help you get ready.</p>

<p><strong><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/elementary-school" target="_blank">Elementary School Checklist</a></strong><strong>:</strong> Student and parent checklists that start the student on the road to enjoying learning and point the parent to resources for college savings accounts.</p>

<p><strong><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/middle-school" target="_blank">Middle School Checklist</a></strong><strong>:</strong> Student and parent checklists that get the student thinking about high school and possible careers and encourage the parent to keep an eye on the student’s progress.</p>

<p><strong>High School Checklist:</strong> Student and parent checklists that help the student focus on succeeding academically and learning about financial aid and provide the parent with tips for supporting the student and participating in the financial aid application process.</p>

<ul>
  <li><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/9th-grade" target="_blank">9th Grade Checklist</a></li>
  <li><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/10th-grade" target="_blank">10th Grade Checklist</a></li>
  <li><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/11th-grade" target="_blank">11th Grade Checklist</a></li>
  <li><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/12th-grade" target="_blank">12th Grade Checklist</a></li>
</ul>

<p><strong><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/adult-student" target="_blank">Adult Student Checklist</a>:</strong> Checklist for adults applying to college, including those who left high school before graduating, graduated high school, completed some college courses, or may be in the workforce.</p>

<p><strong><a href="https://studentaid.ed.gov/sa/prepare-for-college/checklists/late-start" target="_blank">Getting a Late Start?—Last-minute Checklist</a>:</strong> Checklist for anyone who has been accepted at a college and is starting classes soon but hasn’t applied for financial aid yet.</p>

<h3 id="tips-for-your-scholarship-search">Tips for Your Scholarship Search</h3>

<p>You can learn about scholarships in several ways, including contacting the financial aid office at the school you plan to attend and checking information in a public library or online. But be careful. Make sure scholarship information and offers you receive are legitimate; and remember that you don’t have to pay to find scholarships or other financial aid.</p>

<p>Try these free sources of information about scholarships:</p>

<ul>
  <li>the financial aid office at a college or career school</li>
  <li>a high school or TRIO counselor</li>
  <li> the U.S. Department of Labor’s <a href="https://www.careeronestop.org/toolkit/training/find-scholarships.aspx" target="_blank">FREE scholarship search tool</a>
</li>
  <li><a href="https://studentaid.ed.gov/sa/types#aid-from-the-federal-government" target="_blank">federal agencies</a></li>
  <li>your <a href="https://www2.ed.gov/about/contacts/state/index.html" target="_blank">state grant agency</a>
</li>
  <li>your library’s reference section</li>
  <li>foundations, religious or community organizations, local businesses, or civic groups</li>
  <li>organizations (including professional associations) related to your field of interest</li>
  <li>ethnicity-based organizations</li>
  <li>your employer or your parents’ employers</li>
</ul>

<p><br>All information on this page comes from: <a href="https://studentaid.ed.gov/sa/" target="_blank">https://studentaid.ed.gov/sa/</a></p>