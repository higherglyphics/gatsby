---
title: Scholarship FAQs
layout: faq
order_number: 10
sidebar_label: Scholarship FAQs
questions:
  - question: What is the PGA Financial Assistance Fund?
    answer_markdown: >-
      It provides college scholarships for children and grandchildren of PGA
      Members.
  - question: How do I apply?
    answer_markdown: >-
      The PGA Financial Assistance Fund Scholarship application is online at
      PGA.org. You will need the PGA Member’s ID number to access the
      application.
  - question: Who can apply?
    answer_markdown: >-
      High school seniors and college students with a cumulative GPA of 3.4
      (unweighted) and who are the children or grandchildren of a PGA Class “A”
      Member in good standing.
  - question: Do I have to play golf to be eligible for a scholarship?
    answer_markdown: No. The PGA Financial Assistance Fund Scholarship is based on academics, activities and merit.
  - question: What is the scholarship amount(s)?
    answer_markdown: >-
      There are two scholarships, the PGA Financial Assistance Fund Scholarship
      and the John Cox Scholarship. The PGA Financial Assistance Fund
      Scholarship is in the amount of $2000. The John Cox Scholarship is in the
      amount of $5000 and is for college students only. The online scholarship
      application is used for both scholarships.
  - question: Can I use the scholarship money for a trade school?
    answer_markdown: >-
      The scholarship money is for attending a traditional college to earn an
      associates or a baccalaureate degree.
  - question: How are the scholarship dollars disbursed?
    answer_markdown: >-
      The PGA of America will issue a check in the school’s name for the full
      amount. The scholarship dollars are for the fall and spring semester,
      tuition, books and lab fees only.
  - question: Do I have to submit an essay or personal references?
    answer_markdown: No, an essay or personal references are not needed.
  - question: What are “supporting documents”?
    answer_markdown: >-
      Supporting documents are the official school transcript, and the ACT or
      SAT scores (if a high school student). The first page of the Student Aid
      Report is required if applying for financial need.
  - question: What does it mean “official” school transcript?
    answer_markdown: >-
      An official transcript is one that the school has mailed to the PGA of
      America in a sealed envelope.
  - question: >-
      I have an ACT account online with my test information. Can I print my
      scores from the web and send it in?
    answer_markdown: No. The scores need to be submitted by ACT to the PGA of America.
  - question: >-
      I have an SAT account online with my test information. Can I print my
      scores from the web and send it in?
    answer_markdown: >-
      No. The scores need to be submitted by the College Board to the PGA of America.
  - question: How many scholarships are awarded?
    answer_markdown: >-
      The number varies. It is dependent on the availability of funds and IRS
      guidelines concerning private scholarship foundations.
  - question: How is it decided which student gets a scholarship?
    answer_markdown: >-
      Scholarships are awarded at the discretion of the PGA Scholarship
      Committee.
  - question: When and how will I know if I am selected to receive a scholarship?
    answer_markdown: >-
      The scholarship recipients are announced in May. All the applicants will receive a letter and an email with the results.
  - question: >-
      What happens to the scholarship money if I leave school after the fall
      semester?
    answer_markdown: >-
      The school has to send the remaining scholarship dollars back to the PGA
      of America.
  - question: >-
      I am planning to attend graduate school. Can I apply for the PGA Financial Assistance Fund Scholarship?
    answer_markdown: >-
      The scholarship is for first four years only. No graduate studies or 5th year under-graduate.
  - question: Do I have to apply every year?
    answer_markdown: Yes. All students need to reapply every year.
  - question: Can I receive a scholarship every year while I am in college?
    answer_markdown: No. The maximum number of scholarships a student can receive is two.
  - question: Why do I have to submit a photo?
    answer_markdown: >-
      Your photo is needed because it will be used in several of the PGA's
      communication vehicles. The John Cox Scholarship recipients' photos and
      all the names of the scholarship recipients will be listed in a fall issue
      of the PGA Magazine and online at
      [pgamagazinedigital.com](http://www.pgamagazinedigital.com/){: target="_blank"},
      the digital edition of PGA Magazine. Photographs of all scholarship
      recipients will appear on PGA.org and
      [PGAmagazine.com](https://pgamagazine.com/){: target="_blank"}.
  - question: >-
      I have applied for a scholarship before and didn’t receive one. Should I
      still apply?
    answer_markdown: >-
      Yes. Every year presents an entirely new opportunity, as the applicant
      pool will change and potentially be less qualified. The required criteria
      of this application will automatically rank the applicants in terms of who
      is best qualified to receive a scholarship. The rankings are automated by
      the criteria (test scores, GPA, activities).
  - question: When should I have my school mail in my transcript?
    answer_markdown: >-
      You should have your school submit your school transcript after the fall
      semester and before the deadline. The official transcript should have your
      first semester grades. Generally students will request their transcript be
      sent to the PGA after the New Year. That way the transcript has the first
      semester grades and the student’s current class schedule.
  - question: >-
      I’m a college student. Do I have to get my SAT and/or ACT scores sent to
      the PGA?
    answer_markdown: >-
      No. Only high school students have to have the College Board or ACT submit
      their test scores.
  - question: >-
      How will I know if PGA has received my supporting documents?
    answer_markdown: >-
      You will be contacted if there are any missing documents. You may call anytime during normal business hours to check on the status.
---
<h3 id="questions-about-the-scholarship-application-process">Questions About the Scholarship Application Process?</h3>

<p>We’re here to help!  Here’s a list of the most frequently asked questions regarding the PGA Financial Assistance Fund.  If you have any additional questions, please <a href="mailto:membership@pgahq.com?subject=Scholarship%20Question">contact us</a> directly.</p>
<div id="accordion" class="mb-4">
<div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading1" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                          <p class="my-3 fw-600 d-inline-block">What is the PGA Financial Assistance Fund?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse1"></i>
                      </div>
                      <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>It provides college scholarships for children and grandchildren of PGA Members.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading2" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                          <p class="my-3 fw-600 d-inline-block">How do I apply?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse2"></i>
                      </div>
                      <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The PGA Financial Assistance Fund Scholarship application is online at PGA.org. You will need the PGA Member’s ID number to access the application.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading3" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                          <p class="my-3 fw-600 d-inline-block">Who can apply?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse3"></i>
                      </div>
                      <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>High school seniors and college students with a cumulative GPA of 3.4 (unweighted) and who are the children or grandchildren of a PGA Class “A” Member in good standing.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading4" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                          <p class="my-3 fw-600 d-inline-block">Do I have to play golf to be eligible for a scholarship?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse4"></i>
                      </div>
                      <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No. The PGA Financial Assistance Fund Scholarship is based on academics, activities and merit.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading5" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                          <p class="my-3 fw-600 d-inline-block">What is the scholarship amount(s)?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse5"></i>
                      </div>
                      <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>There are two scholarships, the PGA Financial Assistance Fund Scholarship and the John Cox Scholarship. The PGA Financial Assistance Fund Scholarship is in the amount of $2000. The John Cox Scholarship is in the amount of $5000 and is for college students only. The online scholarship application is used for both scholarships.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading6" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                          <p class="my-3 fw-600 d-inline-block">Can I use the scholarship money for a trade school?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse6"></i>
                      </div>
                      <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The scholarship money is for attending a traditional college to earn an associates or a baccalaureate degree.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading7" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                          <p class="my-3 fw-600 d-inline-block">How are the scholarship dollars disbursed?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse7"></i>
                      </div>
                      <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The PGA of America will issue a check in the school’s name for the full amount. The scholarship dollars are for the fall and spring semester, tuition, books and lab fees only.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading8" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                          <p class="my-3 fw-600 d-inline-block">Do I have to submit an essay or personal references?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse8"></i>
                      </div>
                      <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No, an essay or personal references are not needed.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading9" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                          <p class="my-3 fw-600 d-inline-block">What are “supporting documents”?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse9"></i>
                      </div>
                      <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>Supporting documents are the official school transcript, and the ACT or SAT scores (if a high school student). The first page of the Student Aid Report is required if applying for financial need.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading10" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                          <p class="my-3 fw-600 d-inline-block">What does it mean “official” school transcript?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse10"></i>
                      </div>
                      <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>An official transcript is one that the school has mailed to the PGA of America in a sealed envelope.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading11" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                          <p class="my-3 fw-600 d-inline-block">I have an ACT account online with my test information. Can I print my scores from the web and send it in?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse11"></i>
                      </div>
                      <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No. The scores need to be submitted by ACT to the PGA of America.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading12" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                          <p class="my-3 fw-600 d-inline-block">I have an SAT account online with my test information. Can I print my scores from the web and send it in?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse12"></i>
                      </div>
                      <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No. The scores need to be submitted by the College Board to the PGA of America.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading13" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                          <p class="my-3 fw-600 d-inline-block">How many scholarships are awarded?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse13"></i>
                      </div>
                      <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The number varies. It is dependent on the availability of funds and IRS guidelines concerning private scholarship foundations.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading14" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                          <p class="my-3 fw-600 d-inline-block">How is it decided which student gets a scholarship?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse14"></i>
                      </div>
                      <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>Scholarships are awarded at the discretion of the PGA Scholarship Committee.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading15" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                          <p class="my-3 fw-600 d-inline-block">When and how will I know if I am selected to receive a scholarship?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse15"></i>
                      </div>
                      <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The scholarship recipients are announced in May. All the applicants will receive a letter and an email with the results.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading16" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
                          <p class="my-3 fw-600 d-inline-block">What happens to the scholarship money if I leave school after the fall semester?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse16"></i>
                      </div>
                      <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The school has to send the remaining scholarship dollars back to the PGA of America.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading17" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse17">
                          <p class="my-3 fw-600 d-inline-block">I am planning to attend graduate school. Can I apply for the PGA Financial Assistance Fund Scholarship?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse17"></i>
                      </div>
                      <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>The scholarship is for first four years only. No graduate studies or 5th year under-graduate.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading18" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse18">
                          <p class="my-3 fw-600 d-inline-block">Do I have to apply every year?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse18"></i>
                      </div>
                      <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>Yes. All students need to reapply every year.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading19" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">
                          <p class="my-3 fw-600 d-inline-block">Can I receive a scholarship every year while I am in college?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse19"></i>
                      </div>
                      <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No. The maximum number of scholarships a student can receive is two.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading20" data-toggle="collapse" data-target="#collapse20" aria-expanded="true" aria-controls="collapse20">
                          <p class="my-3 fw-600 d-inline-block">Why do I have to submit a photo?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse20"></i>
                      </div>
                      <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>Your photo is needed because it will be used in several of the PGA’s communication vehicles. The John Cox Scholarship recipients’ photos and all the names of the scholarship recipients will be listed in a fall issue of the PGA Magazine and online at <a href="http://www.pgamagazinedigital.com/" target="_blank">pgamagazinedigital.com</a>, the digital edition of PGA Magazine. Photographs of all scholarship recipients will appear on PGA.org and <a href="https://pgamagazine.com/" target="_blank">PGAmagazine.com</a>.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading21" data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">
                          <p class="my-3 fw-600 d-inline-block">I have applied for a scholarship before and didn’t receive one. Should I still apply?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse21"></i>
                      </div>
                      <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>Yes. Every year presents an entirely new opportunity, as the applicant pool will change and potentially be less qualified. The required criteria of this application will automatically rank the applicants in terms of who is best qualified to receive a scholarship. The rankings are automated by the criteria (test scores, GPA, activities).</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading22" data-toggle="collapse" data-target="#collapse22" aria-expanded="true" aria-controls="collapse22">
                          <p class="my-3 fw-600 d-inline-block">When should I have my school mail in my transcript?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse22"></i>
                      </div>
                      <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>You should have your school submit your school transcript after the fall semester and before the deadline. The official transcript should have your first semester grades. Generally students will request their transcript be sent to the PGA after the New Year. That way the transcript has the first semester grades and the student’s current class schedule.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading23" data-toggle="collapse" data-target="#collapse23" aria-expanded="true" aria-controls="collapse23">
                          <p class="my-3 fw-600 d-inline-block">I’m a college student. Do I have to get my SAT and/or ACT scores sent to the PGA?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse23"></i>
                      </div>
                      <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>No. Only high school students have to have the College Board or ACT submit their test scores.</p>

                        </div>
                      </div>
                    </div>
                    <div class="card mb-0">
                      <div class="card-header pointer d-flex justify-content-between align-items-center collapsed" id="heading24" data-toggle="collapse" data-target="#collapse24" aria-expanded="true" aria-controls="collapse24">
                          <p class="my-3 fw-600 d-inline-block">How will I know if PGA has received my supporting documents?</p>
                          <i class="icon icon-arrow text-navy collapsed" data-toggle="collapse" data-target="#collapse24"></i>
                      </div>
                      <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#accordion">
                        <div class="card-body">
                          <p class="mb-0"></p>
<p>You will be contacted if there are any missing documents. You may call anytime during normal business hours to check on the status.</p>

                        </div>
                      </div>
                    </div>
                    </div>