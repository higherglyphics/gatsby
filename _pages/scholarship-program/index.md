---
title: PGA Financial Assistance Fund Scholarship Program
layout: default
order_number: 1
sidebar_label: Scholarship Program
permalink: /scholarship-program/
---
<h2 id="academic-scholarships-for-college">Academic Scholarships for College</h2>

<p>Are your kids academic achievers?  The PGA Financial Assistance Fund was established in 1986, and provides college scholarships to children and grandchildren of PGA Members based on evidence of academic achievement during high school or college. The PGA of America awarded 183 academic scholarships totaling $411,000 to students for the 2017-18 school year.</p>

<p>Help put your kids on the fast-track to success with scholarship money from the PGA of America.  Here’s how you can <strong><a href="https://apps.pga.org/scholarship" target="_blank">apply</a></strong>.</p>

<h3 id="important-dates">Important Dates</h3>

<ul>
  <li>Nov. 16, 2018 – Access the <strong><a href="https://apps.pga.org/scholarship" target="_blank">PGA Financial Assistance Fund Scholarship</a></strong> application here.</li>
  <li>March 4, 2019 – Deadline date for submitting scholarship application.</li>
  <li>March 18, 2019 – Deadline date for receiving the supporting documents.</li>
  <li>May 2019 – The PGA Financial Assistance Fund Scholarship recipients will be announced and notified.</li>
  <li>July 2019 – The scholarship checks will be mailed out.</li>
  <li>Aug. 2019 – Deadline date for photo submission.</li>
  <li>Oct. 2019 – The PGA Financial Assistance Fund Scholarship recipients will be on PGA.org and in PGA Magazine.</li>
  <li>Nov. 2019 - PGA Financial Assistance Fund Scholarship application will be available.</li>
</ul>

<h3 id="faq">FAQ</h3>

<p>Have some questions about the PGA scholarship or the application process? Check out our <strong><a href="/scholarship-program/scholarship-faqs/">list of FAQs</a></strong>.</p>

<h3 id="search-tips">SEARCH Tips</h3>

<p><a href="/scholarship-program/college-prep-scholarship-tips/"><strong>Here are a number of suggestions</strong></a> your child or grandchild can follow in their search for scholarships.</p>