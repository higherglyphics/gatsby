---
title: Apprentice Program Details
layout: default-jumbo
permalink: /how-to-become-a-pga-member/associate-program-details
---
<div class="jumbotron jumbotron-fluid jumbotron-thin editable">
  <img class="jumbotron-image" src="/uploads/become-a-member-header.jpg" data-src="/uploads/become-a-member-header.jpg" alt="">
  <div class="jumbotron-body text-center text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-lg-7 mx-auto">
          <h1>The PGM Associate Program</h1>
          <p class="mx-auto">Take the steps to become a PGA Member and build your career in golf.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="section-indent">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="tiles-wrapper tiles-stretch">
          <div class="benefits-items text-center editable">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <p class="lead mb-0 text-secondary">To apply for membership with the PGA, you must first be accepted into the PGM Associate Program by meeting the milestones listed below.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="playerAbilityTest">
  <div class="container editable">
    <div class="row">
      <div class="col-md-12 mb-4">
        <h2 class="text-secondary">Player Ability Test (PAT)</h2>
        <p>One of the following must be completed within two years prior to registering into the PGA Professional Golf Management Program:</p>
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="/uploads/table-image.jpg" data-src="/uploads/table-image.jpg" alt="Table image">
      </div>
      <div class="col-md-6">
        <ul class="checked-list">
          <li>Pass the 36-hole Playing Ability Test </li>
          <li>Attempt the PAT at least once within the two years prior to registering into the PGA PGM Program. </li>
        </ul>
        <p>Within that time frame, shoot one 18-hole score in a PAT that is equal to or less than the PAT target score for 18- holes, plus 5 strokes. Note: Each PAT score has a validity date of two years.</p>
        <p>The 36-hole PAT remains valid during the Acceptaple Progress period. In order to pass the 36-hole PAT, you must achieve a 36-hole score within 15 shots ot the course raiting. For example, if the course rating is 72, the target score for the 36
          holes would be 159 (72 x 2 - 144 +15 - 159).</p>
        <p>This competition is normally conducted in one day. Fewer than 20% of those taking the test achieve a passing score, therefore, it is highly recommended that you work diligently on your game prior to registereting for the PAT. To register by phone,
          please contact PGA Membership Services at 1-800-474-2776.</p>
        <a class="btn btn-info" target="_blank" href="http://apps.pga.org/patinfo/patsearch.cfm">Schedule Your PAT</a>
      </div>
    </div>
  </div>
</section>
<section class="section-dark" id="qualifyingTest">
  <div class="container editable">
    <div class="row">
      <div class="col-md-8 mx-auto">
        <div class="text-center mb-4">
          <h2 class="text-secondary">Qualifying Test</h2>
          <p class="my-4">Applicants need to purchase and review the three qualifying level courses: Introduction to the PGM and the Golf Profession, PGA History and Constitution, and Rules of Golf to be eligible to register for the qualifying test at the PGA Partnered
            test company with local test centers throughout the country.</p>
        </div>
        <h3 class="text-secondary">Associated Costs</h3>
        <div class="tiles-wrapper p-5 mb-4">
          <div class="row">
            <div class="col-md-12">
              <h5> Portal Access to Online Course | $200</h5>
              <p>Valid for 6 months – If the Qualifying Test is not passed within 6 months of the purchase date a renewal fee of $200 will be required.</p>
            </div>
            <div class="col-md-12">
              <h5> Qualifying Level Test Fee at Test Centers | $40</h5>
              <p>A passing score is valid for 12 months – Failure to register as an associate within that time period will invalidate the test and require re-purchase of the online access and re-test.</p>
            </div>
          </div>
        </div>
        <p class="text-right">
          <a class="text-info" target="_blank" href="/Document-Library/pgm-associated-costs-form-252.pdf">See complete list of fees</a>
        </p>
        <div class="text-center">
          <a class="btn btn-info" target="_blank" href="https://apps.pga.org/pgapgm/index.cfm">Register</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bg-secondary" id="eligibleEmployment">
  <div class="container editable">
    <div class="row">
      <div class="col-md-8 mx-auto text-center">
          <h2 class="text-white">Eligible Employment</h2>
          <p class="my-4">To be eligible to register as an associate, individuals must be employed in an eligible position, within one of the approved classifications. While many traditional golf positions may be top of mind, there are many non-traditional jobs that are
            also eligible positions including things like tournament director, sales &amp; marketing, hospitality and even real estate.</p>
          <a class="btn btn-primary" target="_blank" href="https://jobs.pga.org">Find a Job</a> <br>
          <p class="mt-3 small">NOTE: Participation in an amateur event will forfeit all work <br> experience credits earned prior to the event.</p>
        </div>
    </div>
  </div>
</section>
<section class="section-dark" id="backgroundCheck">
  <div class="container editable">
    <div class="row align-items-center">
      <div class="col-md-6">
        <h2 class="text-secondary">Background Check</h2>
        <p class="my-4">All individuals registering or re-registering as an apprentice must complete a Background Check. Employment Screening Associates (ESA) performs this service at a reasonable cost, paid by the prospective member during an online screening session.</p>
        <a class="btn btn-info" target="_blank" href="https://www.emplscreen.com/pga.asp">Get Started</a>
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="/uploads/complete-bg-check.png" data-src="/uploads/complete-bg-check.png" alt="Complete background check">
      </div>
    </div>
  </div>
</section>