---
title: PGA Career Consultants as an Extension of your Support System
layout: default
order_number: 50
sidebar_label: Support System
---
<p>Another way for anyone to make the most of their PGA Career Consultants is to see them as an extension of the support system that helped them get into the golf industry in the first place. As PGA Career Consultant Kathy Grayson explains, golf professionals of all ages can benefit from expert advice.</p>

<p>“Think about this: From the time you were born, your parents were your advisors. Then in high school, you had teachers and coaches. After that, you had college advisors or PGM faculty helping you as you got into the business,” says Grayson, the PGA Career Consultant for the South Florida PGA Section. “Now, the PGA Career Consultants are your professional advisors – we know how to help you make goals, create a plan and navigate the system.”</p>

<p>Grayson emphasizes the advice PGA Career Consultants can provide in the basics of career planning and preparedness: Creating and updating strong resumes, writing cover letters and being ready for the interview process.</p>

<p>In terms of resumes, Grayson advises PGA Professionals to have multiple versions ready for different types of positions. That includes one version that could highlight general golf operations experience for an open head professional position, and another that emphasizes teaching and playing accolades for a director of instruction opening.</p>

<p>“We can help you with the nuances of resume writing so you can emphasize different skills an employer is looking for,” Grayson says. “You might be a great player, which is great for a teaching and coaching job, but might not be nearly as important if you’re up for a head professional job with budgeting and food &amp; beverage responsibilities. We can show you how to research and figure out what employers are looking for, then tailor your marketing materials to address their concerns.”</p>

<p>Grayson and her fellow PGA Career Consultants can also prepare golf industry professionals for the interview process. She suggests putting together a sample business plan for the facility, showing the value the interviewee can bring in either driving revenue or reducing costs.</p>

<p>Grayson’s advice is echoed by PGA Professional Monte Koch, the PGA Career Consultant for the Pacific Northwest and Rocky Mountain PGA Sections. He says employers are always on the lookout for proactive people instead of those who prefer to wait and react to situations – and being able to effectively tell your story as a PGA Professional is an ideal way to portray your proactive nature.</p>

<p>“If I track my performance and I can report it to my current employer or a potential new employer, that shows me being proactive in driving value and being able to explain it,” Koch says. “You’re selling yourself, and the way to raise the price an employer is willing to pay is by showing that you bring the value and the skills that’s going to improve the bottom line at that facility.”</p>

<p>Koch says he’s seeing a trend in his territory toward PGA Professionals becoming more comfortable – and successful – with flexible compensation packages.</p>

<p>While everyone would prefer to have a higher salary, Koch sees more facilities offering ways for creative and innovative PGA Professionals to augment their compensation with “pay for performance” opportunities.</p>

<p>“I think, in a way, this is a bit of a return to what we used to have in the golf industry, where the head professional might have had a relatively low salary – but could dramatically increase income by owning the golf shop or the golf cars,” Koch says. “I’m seeing facilities offer packages where PGA Professionals can earn additional money based on what the facility produces in food &amp; beverage, shop sales, rounds played and other seasonal targets. Or there might be other perks, like housing on property.</p>

<p>“We can help professionals see the big picture in these situations – seeing the opportunities beyond the salary, and how you can advance your career by changing your mindset.”</p>

<p>Changing your mindset – and upgrading your skill set – has helped several PGA Professionals build their careers.</p>

<p>Here’s a look at how six of them determined their career paths, then took the steps needed to secure the jobs they wanted.</p>

<p>For additional information and more career advice please reach out to <a href="/career-consultants/">your Career Consultant</a></p>