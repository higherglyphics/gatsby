---
layout: default
permalink: /categories/
title: Categories
---
<div id="archives">

  <div class="archive-group">
    
    <div id="#reciprocity"></div>
    <p></p>
    <h3 class="category-head">reciprocity</h3>
    <a name="reciprocity"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/lpga-test-out-options/">LPGA Test-Out Options</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#become a member"></div>
    <p></p>
    <h3 class="category-head">become a member</h3>
    <a name="become%20a%20member"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/lpga-test-out-options/">LPGA Test-Out Options</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#STUDENT"></div>
    <p></p>
    <h3 class="category-head">STUDENT</h3>
    <a name="STUDENT"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/how-become-pga-member/">How to Become a PGA Member</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#BECOME A MEMBER"></div>
    <p></p>
    <h3 class="category-head">BECOME A MEMBER</h3>
    <a name="BECOME%20A%20MEMBER"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/how-become-pga-member/">How to Become a PGA Member</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#APPRENTICE"></div>
    <p></p>
    <h3 class="category-head">APPRENTICE</h3>
    <a name="APPRENTICE"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/how-become-pga-member/">How to Become a PGA Member</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#RECIPROCITY"></div>
    <p></p>
    <h3 class="category-head">RECIPROCITY</h3>
    <a name="RECIPROCITY"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/how-become-pga-member/">How to Become a PGA Member</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#TEACHING BEST PRACTICES"></div>
    <p></p>
    <h3 class="category-head">TEACHING BEST PRACTICES</h3>
    <a name="TEACHING%20BEST%20PRACTICES"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/player-development/">Player Development</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/how-become-pga-member/">How to Become a PGA Member</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#CONTINUING EDUCATION"></div>
    <p></p>
    <h3 class="category-head">CONTINUING EDUCATION</h3>
    <a name="CONTINUING%20EDUCATION"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/player-development/">Player Development</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#GOLFER RETENTION"></div>
    <p></p>
    <h3 class="category-head">GOLFER RETENTION</h3>
    <a name="GOLFER%20RETENTION"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/player-development/">Player Development</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#IDEAS &amp; BEST PRACTICES"></div>
    <p></p>
    <h3 class="category-head">IDEAS &amp; BEST PRACTICES</h3>
    <a name="IDEAS%20&amp;%20BEST%20PRACTICES"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/player-development/">Player Development</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#BUILDING YOUR BRAND"></div>
    <p></p>
    <h3 class="category-head">BUILDING YOUR BRAND</h3>
    <a name="BUILDING%20YOUR%20BRAND"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
    <article class="archive-item">
      <h4><a href="/articles/player-development/">Player Development</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#SECTION EDUCATION"></div>
    <p></p>
    <h3 class="category-head">SECTION EDUCATION</h3>
    <a name="SECTION%20EDUCATION"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#EXCELLING IN YOUR JOB"></div>
    <p></p>
    <h3 class="category-head">EXCELLING IN YOUR JOB</h3>
    <a name="EXCELLING%20IN%20YOUR%20JOB"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
  </div>

  <div class="archive-group">
    
    <div id="#JOB PERFORMANCE"></div>
    <p></p>
    <h3 class="category-head">JOB PERFORMANCE</h3>
    <a name="JOB%20PERFORMANCE"></a>
    
    <article class="archive-item">
      <h4><a href="/articles/enhance-income-and-increase-job-security-attend-player-development-section-workshop/">Enhance Income and Increase Job Security: Attend a Player Development Section Workshop</a></h4>
    </article>
    
  </div>

</div>