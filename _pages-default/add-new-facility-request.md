---
title: Add New Facility Request
layout: default-no-sidebar
permalink: /add-new-facility-request/
---
<p>Please complete and return the request form to your Section for approval via email or fax.</p>

<p>If you do not know your Section, or if you are not part of a Section, please complete and return the request form via email at <a href="mailto:membership@pgahq.com">membership@pgahq.com</a>. </p>

<ul>
  <li>
<a href="/Document-Library/recognized-golf-course-form-400.pdf" target="_blank">PGA-Recognized Golf Course</a> (400)</li>
  <li>
<a href="/Document-Library/recognized-golf-range-form-401.pdf" target="_blank">PGA-Recognized Golf Range</a> (401)</li>
  <li>
<a href="/Document-Library/recognized-golf-school-form-402.pdf" target="_blank">PGA-Recognized Golf School</a> (402)</li>
  <li>
<a href="/Document-Library/recognized-indoor-facility-form-403.pdf" target="_blank">PGA-Recognized Indoor Facility</a> (403)</li>
  <li>
<a href="/Document-Library/recognized-association-form-404.pdf" target="_blank">PGA-Recognized Golf Association</a> (404)</li>
</ul>

<p>For questions, please contact us at 1-800-474-2776.</p>