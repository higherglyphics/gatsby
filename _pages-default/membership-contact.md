---
title: Contact Us
layout: default-jumbo
---
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="content-default">
        <form class="contact-form px-4" id="contactForm" action="https://formspree.io/akw9jgf3@pga-hq.intercom-mail.com" method="POST">
          <input type="hidden" name="notification" value="The following individual was unable to login to PGA.org because we are missing the Entity ID in OneLogin. This means that either the record exists in Oracle, but was never added to OneLogin OR we are missing the Oracle record.  The updated information is below. Please verify the status as either Member, Associate or PGM Univ ST and contact Carrie McDonald with the IT Group PGA.org team for assistance.">
          <div class="heading">
            <h1 class="text-secondary mb-4">Contact the PGA</h1>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="firstName">First Name*</label>
                <input type="text" class="form-control" id="firstName" name="firstName" size="60" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="lastName">Last Name*</label>
                <input type="text" class="form-control" id="lastName" name="lastName" size="60" required="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="edit-submitted-email">Email*</label>
                <input type="email" class="form-control" id="edit-submitted-email" name="email" size="60" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="edit-submitted-email">Gender*</label> <br>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="maleRadio" name="gender" value="male" class="custom-control-input" required="">
                  <label class="custom-control-label" for="maleRadio">Male</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="femaleRadio" name="gender" value="female" class="custom-control-input">
                  <label class="custom-control-label" for="femaleRadio">Female</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 d-flex flex-column justify-content-between">
              <div class="form-group">
                <label for="">Member Number</label>
                <input type="text" class="form-control" name="memberNumber" size="60">
              </div>
              <button type="submit" class="btn btn-lg btn-info d-none d-md-block">Submit</button>
            </div>
            <div class="col-md-6">
              <div class="form-group mb-md-0">
                <label for="lastName">Message</label>
                <textarea class="form-control" id="edit-submitted-question" name="question" cols="60" rows="5"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info d-md-none d-block">Submit</button>
              </div>
            </div>
            <input type="text" name="_gotcha" style="display:none">
            <input type="hidden" name="_next" value="/contact-pga/thank-you/">
            <input type="hidden" name="_subject" value="Contact">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="map-wrapper">
  <div class="tiles-wrapper p-5">
    <div class="tiles-icon-header">
      <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewbox="0 0 99 99" version="1.1">
  <title>5C3B5768-79BE-4B49-A916-6137F5C72DFB</title>
  <desc>Created with sketchtool.</desc>
  <defs>
    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
    <filter x="-22.2%" y="-15.8%" width="144.3%" height="144.3%" filterunits="objectBoundingBox" id="filter-2">
      <feoffset dx="0" dy="5" in="SourceAlpha" result="shadowOffsetOuter1"></feoffset>
      <fegaussianblur stddeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></fegaussianblur>
      <fecolormatrix values="0 0 0 0 0 0 0 0 0 0.137254902 0 0 0 0 0.294117647 0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></fecolormatrix>
    </filter>
  </defs>
  <g id="UI" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="PGA_UI_Membership" transform="translate(-367.000000, -534.000000)">
      <g id="Group-4" transform="translate(145.000000, 539.000000)">
        <g id="Card">
          <g id="Group" transform="translate(40.000000, 0.000000)">
            <g id="Group-2">
              <g id="Group-3">
                <g id="White-icon" transform="translate(192.000000, 0.000000)">
                  <g id="icon-education">
                    <use fill="#FFFFFF" fill-rule="evenodd"></use>
                  </g>
                  <g id="path-1-link" fill="#FFFFFF">
                    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
                  </g>
                  <g id="Associate-icon-gold" transform="translate(4.000000, 4.000000)" stroke="#B4975A">
                    <path d="M10.3747173,10.3747173 C24.1925324,-3.44309787 46.5782949,-3.46044015 60.3747173,10.3359822 C74.1711396,24.1324046 74.1537973,46.5181671 60.3359822,60.3359822 C46.5181671,74.1537973 24.1324046,74.1711396 10.3359822,60.3747173 C-3.46044015,46.5782949 -3.44309787,24.1925324 10.3747173,10.3747173 Z" id="icon-membership" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="80,4,4,4"></path>
                    <g id="golf-(1)" transform="translate(23.000000, 18.000000)" stroke-linecap="square" stroke-width="1.75">
                      <path d="M22.5,9.5 L13.5,9.5 L13.5,5.7878265 C13.5,5.0035395 13.9584064,4.291634 14.6723938,3.9670934 L21.6723938,0.7852754 C22.9965935,0.183367 24.5,1.151431 24.5,2.6060083 L24.5,7.5 C24.5,8.6045694 23.6045704,9.5 22.5,9.5 Z" id="Shape"></path>
                      <path d="M13.5,9.5 L13.5,19.5" id="Shape"></path>
                      <path d="M2.5,13.5 L9.5,13.5 L9.5,10.7997465 C9.5,10.0093689 9.0345325,9.2931261 8.3122768,8.9721231 L3.3122768,6.7499008 C1.9897022,6.1620898 0.5,7.1302071 0.5,8.5775242 L0.5,11.5 C0.5,12.6045694 1.3954306,13.5 2.5,13.5 Z" id="Shape"></path>
                      <path d="M9.5,13.5 L9.5,19.5" id="Shape"></path>
                      <rect id="Rectangle-path" x="2.5" y="19.5" width="18" height="4"></rect>
                      <path d="M18,24 L18,34" id="Shape"></path>
                      <path d="M6,34 L6,24.0237228" id="Shape"></path>
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </g>
</svg>
    </div>
    <h3 class="text-secondary">PGA of America Headquarters</h3>
    <p><strong>100 Avenue of the Champions <br>Box 109601 Palm Beach Gardens, FL 33410-9601</strong></p>
    <p><strong>General:</strong> 561-624-8400 or 1-800-477-6465</p>
    <p><strong>Membership Information Services Center (MISC):</strong> 1-800-474-2776</p>
  </div>
  <div id="map"></div>
</div>
<!-- Googple Map Init -->
<script>
  var map;

  function initMap() {
    // Styles a map in night mode.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 26.8367,
        lng: -80.13655
      },
      zoom: 16,
      styles: [{
          elementType: 'geometry',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#746855'
          }]
        },
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{
            color: '#263c3f'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#6b9a76'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{
            color: '#38414e'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#212a37'
          }]
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#9ca5b3'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{
            color: '#746855'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#1f2835'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#f3d19c'
          }]
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{
            color: '#2f3948'
          }]
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{
            color: '#17263c'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#515c6d'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#17263c'
          }]
        }
      ]
    });
    var image = '/assets/images/icons/icon-pin.svg';
    var beachMarker = new google.maps.Marker({
      position: {
        lat: 26.8367,
        lng: -80.13655
      },
      map: map,
      icon: image
    });
  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjGuD4qHG61FEOcAGDxqyt2xIK_hFBI-0&amp;callback=initMap" async="" defer></script>