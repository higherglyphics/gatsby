---
title: How Career Consultants Can Help When You are Happy in Your Career
layout: default
order_number: 60
sidebar_label: Happy in your Career
---
<p>Many golf industry professionals have wonderful jobs and cannot imagine working anywhere else, but consider these three things:</p>

<ol>
  <li>What happens if your “Dream Job” opened up (everyone has one). Are you qualified for it? Do you lack something that would prevent you from being a serious candidate for the position? Even if you are happy with your position, you should continue to position yourself as a viable candidate for jobs that open up, and your Career Consultant can help you do that.</li>
  <li>Career Consultants can assist you in learning how to make a good job even better! They can provide you with the tools, knowledge, and resources to assist you in showing your value to employers. The can also offer advice on improving your negotiating skills to help increase compensation for you and your staff.</li>
  <li>Everyone’s career must end, and Career Consultants can assist in planning for retirement from your current position.</li>
</ol>

<p>For additional information and more career advice please reach out to <a href="/career-services/">your Career Consultant</a></p>