---
title: 'PGA Professional Membership Directory, Golf Tips & More | PGA.org'
secondary_title: home
permalink: /
layout: home
navbar-secondary: discover
header-style-filled: false
# body-color: home-page
body-color: #e2e2e2
hero:
  title: The PGA of America
  image_link: /uploads/hero-homepage5.jpg
  lead: The PGA of America is one of the world's largest sports organizations, composed of PGA Professionals who daily work to grow interest and participation in the game of golf.
benefits_of_hiring:
  header: Benefits of Hiring a PGA Professional
  lead: Selecting the best PGA Professional for your facility is one of the most important decisions you will make. Employers of PGA Professionals reap the rewards of unrivaled marketing and operational benefits thanks to a variety of tools, resources and cost-saving benefits.
  button_link: /career-services/hire-a-pga-professional/
  pathway_to_membership_link: /membership
benefits_of_membership:
  - title: Grow the Game You Love
    lead: As the industry's standard bearer, PGA Professionals serve as the recognized teachers and leaders of the game and promote its growth passionately.
    logo_link: /uploads/homepage/golf-ball.svg
  - title: Expand Your Career
    lead: PGA Professionals have the opportunity to pursue a variety of exciting careers within the golf industry including golf operations, teaching and coaching and executive management.
    logo_link: /uploads/homepage/certificate.svg
  - title: Play Your Best
    lead: PGA Members have the opportunity to compete in numerous tournaments throughout the country on some of the best courses in the world.
    logo_link: /uploads/homepage/trophy.svg
journeys_videos:
  - image_link: /uploads/start-your-journey-in-golf-2.png
    text: Start Your Journey in Golf
    video_link: https://player.vimeo.com/video/332320323
  - image_link: /uploads/pga-journey-bg2.png
    text: Let's Get You There
    video_link: https://player.vimeo.com/video/283104965
  - image_link: /uploads/pga-journey-bg3.png
    text: Become a Leader in Golf
    video_link: https://player.vimeo.com/video/271492220
championships_section:
  image_link: /uploads/championships-header.jpg
  logo_link: /uploads/ryder-cup-logo.png
defined_section:
  - title: Our constant pursuit of excellence and innovation
    lead: We strive to protect and enhance the PGA brand, engage and support our members, develop and retain golfers, promote the game and make it more fun.
    image_link: /uploads/defined-pursuit.jpg
  - title: Our culture of teamwork and collaboration
    lead: Position the PGA of America and our members as leaders in golf coaching, business operations and general management.
    image_link: /uploads/defined-culture.jpg
  - title: Our commitment to diversity and inclusion
    lead: Strengthen the perception of the PGA of America and our members as the tangible connection between the game and all that play.
    image_link: /uploads/defined-commitment.jpg
---
<div class="jumbotron jumbotron-fluid editable">
  <p><img class="jumbotron-image" data-src="/uploads/hero-homepage5.jpg" alt="The PGA of America" src="/uploads/hero-homepage5.jpg" /></p>
  <div class="jumbotron-body">
    <div class="container">
      <div class="row jb-content-wpr">
        <div class="col-9 col-md-8 col-lg-5">
          <h1>The PGA of America</h1>
          <p>The PGA of America is one of the world's largest sports organizations, composed of PGA Professionals who work daily to grow interest and participation in the game of golf.</p>
          <p><a class="btn btn-info" data-cms-editor-link-style="undefined" href="https://auth.pga.org/v1/login?return_to=https://account.pga.org">Login</a></p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="editable py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="card mb-0">
          <img alt="Hire a PGA Professional" class="card-img-top" src="/uploads/hire-a-pro-cta.jpg">
          <div class="card-body p-4">
            <h4 class="text-secondary">Benefits of Hiring a PGA Professional</h4>
            <p class="mt-3" style="font-size: 0.8rem">Selecting the best PGA Professional for your facility is one of the most important decisions you will make. Employers of PGA Professionals reap the rewards of unrivaled marketing and operational benefits thanks to a variety of tools, resource and cost-saving benefits.</p>
<a class="btn btn-outline-info btn-block" href="/career-services/hire-a-pga-professional/">Learn More</a>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="tiles-wrapper tiles-stretch d-flex flex-column justify-content-between">
          <div class="benefits-items benefits-membership">
            <div class="row">
              <div class="col-md-12">
                <h3 class="text-secondary mb-4">Benefits of Membership</h3>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon my-3"><img alt="golf ball icon" height="62" src="/uploads/homepage/golf-ball.svg" width="62"></div>
                <h6 class="fw-600 pb-2">Grow the Game You Love</h6>
                <p class="mb-0">As the industry's standard bearer, PGA Professionals serve as the recognized teachers and leaders of the game and promote its growth passionately.</p>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon my-3"><img alt="golf ball icon" height="62" src="/uploads/homepage/certificate.svg" width="62"></div>
                <h6 class="fw-600 pb-2">Expand Your Career</h6>
                <p class="mb-0">PGA Professionals have the opportunity to pursue a variety of exciting careers within the golf industry including golf operations, teaching and coaching and executive management.</p>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon my-3"><img alt="golf ball icon" height="62" src="/uploads/homepage/trophy.svg" width="62"></div>
                <h6 class="fw-600 pb-2">Play Your Best</h6>
                <p class="mb-0">PGA Members have the opportunity to compete in numerous tournaments throughout the country on some of the best courses in the world.</p>
              </div>
            </div>
          </div>
          <section class="benefits-footer">
            <div class="row">
              <div class="col-items with-border col-md-6">
                <p>Learn about <a class="text-info lead" href="/membership">Pathways to Membership</a></p>
              </div>
              <div class="col-items col-md-6">
                <p>Already a member? <a class="text-info" href="https://auth.pga.org/v1/login?return_to=https://account.pga.org">Login</a></p>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- PGA JOURNEYS SECTION -->
<section class="editable pb-5">
  <div class="row no-gutters">
    <div class="col-lg-6 d-none d-lg-block responsive-img__wrapper"></div>
    <div class="col-lg-6 bg-navy">
      <div class="journeys__wrapper">
        <div class="journeys__header row align-items-center">
          <img alt="Championship Logo" src="/uploads/pga-journey-logo.svg">
          <h3 class="title text-white">Your Journey Starts Here</h3>
        </div>
        <div class="container">
          <p class="journeys__lead text-white">Whether you work in the golf business or are taking your first swings; if you are picking the game back up after some time off or have won at the highest level of competitive golf; every journey is unique and no one can take that journey alone.</p>
        </div>
        <div class="container pt-xl-3">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <button class="journeys__video" data-target="#pga-journey-1" data-toggle="modal"><img class="w-100" src="/uploads/start-your-journey-in-golf-2.png"></button>
              <p class="journeys__video-text">Start Your Journey in Golf</p>
              <div aria-hidden="true" aria-labelledby="pga-journey-1" class="modal fade" id="pga-journey-1" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-1">Start Your Journey in Golf</h3>
<i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/332320323"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <button class="journeys__video" data-target="#pga-journey-2" data-toggle="modal"><img class="w-100" src="/uploads/pga-journey-bg2.png"></button>
              <p class="journeys__video-text">Let's Get You There</p>
              <div aria-hidden="true" aria-labelledby="pga-journey-2" class="modal fade" id="pga-journey-2" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-2">Let's Get You There</h3>
<i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/283104965"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <button class="journeys__video" data-target="#pga-journey-3" data-toggle="modal"><img class="w-100" src="/uploads/pga-journey-bg3.png"></button>
              <p class="journeys__video-text">Become a Leader in Golf</p>
              <div aria-hidden="true" aria-labelledby="pga-journey-3" class="modal fade" id="pga-journey-3" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-3">Become a Leader in Golf</h3>
<i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/271492220"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- PGA REACH SECTION WITH TABS -->
<section class="editable reach-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-5">
        <div class="card card-inline h-100">
          <h3 class="p-3 text-navy">PGA REACH</h3>
<img alt="PGA PGM Apprentice Program" class="img-fluid" src="/uploads/pga-reach-header.jpg">
          <div class="card-body p-3">
            <ul class="nav nav-tabs nav-fill flex-nowrap nav-tabs__reach" id="myTab" role="tablist">
              <li class="nav-item">
                <a aria-controls="reach" aria-selected="false" class="text-info nav-link" data-toggle="tab" href="#reach" id="reach-tab" role="tab">foundation</a>
              </li>
              <li class="nav-item">
                <a aria-controls="juniorleague" aria-selected="false" class="text-info nav-link" data-toggle="tab" href="#juniorleague" id="juniorleague-tab" role="tab">PGA JR. League</a>
              </li>
              <li class="nav-item">
                <a aria-controls="hope" aria-selected="false" class="text-info nav-link" data-toggle="tab" href="#hope" id="hope-tab" role="tab">PGA Hope</a>
              </li>
              <li class="nav-item">
                <a aria-controls="works" aria-selected="true" class="text-info nav-link show" data-toggle="tab" href="#works" id="works-tab" role="tab">PGA Works</a>
              </li>
            </ul>
            <div class="tab-content tab-content__reach">
              <div aria-labelledby="reach-tab" class="tab-pane" id="reach" role="tabpanel">
                <div class="program">
                  <div class="program-image"><img alt="foundation" src="/uploads/reach-logo.svg" width="92"></div>
                  <div class="program-body">
                    <p>PGA REACH is the 501(c)(3) charitable foundation of the PGA of America. The mission of PGA REACH is to positively impact the lives of youth, military, and diverse populations by enabling access to PGA Professionals, PGA Sections and the game of golf.</p>
                    <p class="mb-0"><a class="text-info" href="https://pgareach.org/" target="_blank">Visit PGAREACH.org</a></p>
                  </div>
                </div>
              </div>
              <div aria-labelledby="juniorleague-tab" class="tab-pane" id="juniorleague" role="tabpanel">
                <div class="program">
                  <div class="program-image"><img alt="PGA JR. League" src="/assets/images/JR_League_4c.svg" width="92"></div>
                  <div class="program-body">
                    <p>PGA Jr. League is the flagship youth pillar program of PGA REACH, with the goal of making the program accessible to all interested kids in the United States and around the world.</p>
                    <p class="mb-0"><a class="text-info" href="https://pgareach.org/" target="_blank">Visit PGAREACH.org</a></p>
                  </div>
                </div>
              </div>
              <div aria-labelledby="hope-tab" class="tab-pane" id="hope" role="tabpanel">
                <div class="program">
                  <div class="program-image"><img alt="PGA Hope" src="/uploads/pga-hope.png" width="92"></div>
                  <div class="program-body">
                    <p>PGA HOPE (Helping Our Patriots Everywhere) is the flagship military program of PGA REACH, the charitable foundation of the PGA of America. PGA HOPE introduces golf to Veterans with disabilities to enhance their physical, mental, social and emotional well-being.</p>
                    <p class="mb-0"><a class="text-info" href="https://pgareach.org/" target="_blank">Visit PGAREACH.org</a></p>
                  </div>
                </div>
              </div>
              <div aria-labelledby="works-tab" class="tab-pane active show" id="works" role="tabpanel">
                <div class="program">
                  <div class="program-image"><img alt="PGA Works" src="/uploads/pga-works.png" width="92"></div>
                  <div class="program-body">
                    <p>PGA WORKS Fellowship is one of the flagship programs of PGA REACH. PGA WORKS Fellowship aspires to be the most valuable entry-level employment opportunity for individuals from diverse backgrounds to garner experience in all facets of the golf industry.</p>
                    <p class="mb-0"><a class="text-info" href="https://pgareach.org/" target="_blank">Visit PGAREACH.org</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mb-5">
        <div class="card card-inline h-100">
          <h3 class="p-3 text-navy">Our Championships</h3>
<img alt="Championships" class="img-fluid" src="/uploads/championships-header.jpg">
          <div class="card-body mt-lg-3">
            <div class="row">
              <div class="col-lg-4 text-center mb-3"><img alt="Championship Logo" height="114" src="/uploads/ryder-cup-logo.png" width="114"></div>
              <div class="col-lg-8">
                <p>In addition to the Ryder Cup, PGA Championship, KPMG Women's PGA Championship and KitchenAid Senior PGA Championship, the PGA of America hosts numerous member and junior championships.</p>
<a class="text-info" href="/pga-championships">Learn About Championships</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="editable pb-5">
  <div class="container">
    <div class="row no-gutters box-shadow-1">
      <div class="col-lg-8">
        <div class="tiles-stretch bg-white">
          <div class="benefits-items">
            <div class="row">
              <div class="col-md-12">
                <h3 class="text-secondary">We will be defined by...</h3>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon"><img alt="Our constant pursuit of excellence and innovation" class="w-100" src="/uploads/defined-pursuit.jpg"></div>
                <h6 class="mb-2 fw-600">Our constant pursuit of excellence and innovation</h6>
                <p class="mb-0">We strive to protect and enhance the PGA brand, engage and support our members, develop and retain golfers, promote the game and make it more fun.</p>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon"><img alt="Our culture of teamwork and collaboration" class="w-100" src="/uploads/defined-culture.jpg"></div>
                <h6 class="mb-2 fw-600">Our culture of teamwork and collaboration</h6>
                <p class="mb-0">Position the PGA of America and our members as leaders in golf coaching, business operations and general management.</p>
              </div>
              <div class="col-md-4">
                <div class="benefits-icon"><img alt="Our commitment to diversity and inclusion" class="w-100" src="/uploads/defined-commitment.jpg"></div>
                <h6 class="mb-2 fw-600">Our commitment to diversity and inclusion</h6>
                <p class="mb-0">Strengthen the perception of the PGA of America and our members as the tangible connection between the game and all that play.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="tiles-stretch bg-gray-light p-4">
          <div class="mb-0">
            <img alt="" class="img-fluid w-100" height="528" src="/uploads/map-navy.svg" width="800">
            <div class="tiles-footer flex-column align-items-start">
              <h3 class="text-navy fw-900">A shared commitment to the member</h3>
              <p><a href="/strategic-plan">Learn more about our strategic plan</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>