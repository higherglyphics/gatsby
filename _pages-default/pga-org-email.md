---
title: PGA.com Email
layout: default-no-sidebar
---
<p>Some PGA Members using the pga.com email platform have been seeing a “Server Identity” error that looks something like this:<br><em>The identity of “mbrimap4.pgalinks.com” cannot be verified by Mail</em></p>

<p><strong>If this is you, continue reading for steps on how to correct the issue. </strong></p>

<p>NOTE: these are for individuals who are using an email client (something like “Mail” or “Outlook”) on their computer, tablet, or phone, since those who are seeing this error are more likely to be using one of those tools.  If you go to the website–either by clicking the email link on pga.org or going directly to the email website–then these steps will not work. If you use the website to access your email and you are seeing error, please contact the PGA Headquarters Service Desk (details below). </p>

<h3 id="steps-to-correct">Steps to correct</h3>

<ol>
  <li>Delete the email account from your device (phone, tablet or computer) by going to the app you use to manage your email. You do not need to delete the entire app, just the account.  Don’t worry! Everything will be just as you left it when you add your account back to your device.
    <ul>
      <li>Information on deleting accounts from <a href="/Document-Library/How%20to%20Remove%20an%20Email%20Account%20from%20an%20iPhone.pdf" target="_blank">iPhone</a>
</li>
      <li>Information on deleting accounts from <a href="https://support.apple.com/guide/mail/add-or-remove-email-accounts-mail35803/mac" target="_blank">Mac Mail</a>
</li>
      <li>Information on deleting accounts from <a href="https://support.office.com/en-us/article/remove-or-delete-an-email-account-from-outlook-1fa900ae-6dc8-468c-b754-10292f7ee47a" target="_blank">Outlook</a><br> </li>
    </ul>
  </li>
  <li>Once the email account is deleted, turn your device off completely.  Wait 30 seconds or a minute or so, and turn it back on.<br> </li>
  <li>Go back to your mail app to add your pga.com email account back to your device.
    <ul>
      <li>Information on adding accounts to <a href="/Document-Library/Setup-email-for-iOS-11-or-later-on-iPhone.pdf" target="_blank">iPhone</a>
</li>
      <li>Information on adding accounts to <a href="https://support.apple.com/guide/mail/add-or-remove-email-accounts-mail35803/mac" target="_blank">Mac Mail</a>
</li>
      <li>Information on adding accounts to <a href="https://support.office.com/en-us/article/add-an-email-account-to-outlook-6e27792a-9267-4aa4-8bb6-c84ef146101b" target="_blank">Outlook</a><br> </li>
    </ul>
  </li>
  <li>When you prompted to do so, enter these settings:</li>
</ol>

<h5 id="inbound-servers">Inbound Servers:</h5>

<table>
  <tbody>
    <tr>
      <td><strong>Type</strong></td>
      <td><strong>Server Address</strong></td>
      <td><strong>Port</strong></td>
    </tr>
    <tr>
      <td>POP Server (SSL)</td>
      <td>secure.emailsrvr.com</td>
      <td>995</td>
    </tr>
    <tr>
      <td>IMAP Server (SSL)</td>
      <td>secure.emailsrvr.com</td>
      <td>993</td>
    </tr>
  </tbody>
</table>

<h5 id="outbound-servers">Outbound Servers:</h5>

<table>
  <tbody>
    <tr>
      <td>Type</td>
      <td>Server Address</td>
      <td>Port</td>
    </tr>
    <tr>
      <td>SMTP Server (SSL)</td>
      <td>secure.emailsrvr.com</td>
      <td>587 or 465</td>
    </tr>
  </tbody>
</table>

<p> </p>

<p>If, once you’ve completed the restore, you are still seeing an error message or require further assistance related to the server setting changes please contact the PGA  Headquarters Service Desk from 8:00 am -5:00 pm EST at 561-624-8524 or via email – <a href="mailto:helpdesk@pgahq.com?subject=Email%20Server%20Setting%20Issues">helpdesk@pgahq.com</a></p>