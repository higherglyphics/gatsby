---
title: Privacy Policy
layout: default-no-sidebar
permalink: /privacy-policy/
---
<p><strong><em>Effective September 21, 2018</em></strong></p>

<h3 id="introduction">Introduction</h3>

<p>Your privacy is important to us at the Professional Golfers’ Association of America. We respect your Personal Data and endeavor to treat it in accordance with your expectations. </p>

<p><strong>“Personal Data”</strong> means any information relating to an identified or identifiable natural person, where an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to that natural person.</p>

<p>This Privacy Policy provides notice explaining our online information practices and the choices you can make about the way your information is collected and used at PGA.org.  </p>

<p>This Privacy Policy applies to different categories of users, specifically, PGA Members who sign up for an account to access services on the web site that are supplementary to their PGA membership, and other users who access PGA.org but do not register.</p>

<h3 id="who-we-are">Who we are</h3>

<p>The Professional Golfers’ Association of America, (sometime referred to as “PGA”, “we”, “us” or “our” in this Privacy Policy) is the controller and is responsible for your Personal Data.</p>

<p>We have appointed a data privacy director who is responsible for overseeing questions in relation to this Privacy Policy.  If you have any questions, including requests to exercise your legal rights in regard to this Privacy Policy, please contact the data privacy director using the details set out below:</p>

<p>Full name of legal entity: The Professional Golfers’ Association of America</p>

<p>Name of Data Privacy Director: Laurie French</p>

<p>Email address: <a href="mailto:golf@pgahq.com">golf@pgahq.com</a></p>

<p>Postal address: 100 Ave of the Champions, Palm Beach Gardens, FL 33418</p>

<p>You may have the right to make a complaint to a data protection supervisory authority in the European Union. We would, however, appreciate the chance to address your concerns before you approach a supervisory authority, so please contact us in the first instance.</p>

<h3 id="the-personal-data-we-collect-and-store-and-how-it-is-collected">The Personal Data we collect and store, and how it is collected.</h3>

<p>We often seek to collect from you, various forms of Personal Data. Examples of the types of Personal Data that may be collected on our web site include: name, e-mail address, telephone number, demographic information, social media handles, and information about your interests in and use of various products, programs, and services.</p>

<p>You may also be able to submit Personal Data about other people, for example when you purchase products or tickets to be sent to someone else.  Examples of the types of Personal Data that may be collected about other people on our web site include: recipient’s name, and e-mail address, phone number and/or shipping address.</p>

<p>At certain parts of our web site, only persons who provide us with the appropriate credentials will be able to participate in the site’s services, activities and offerings. For credentialed users, we will collect education history, employment details, e-mail history, and personal interests in the form of profiles, activities, and surveys. </p>

<p>We, our service providers, advertisers and partners may collect various types of information from your computer or device when you visit any of our sites. A representative list of the types of information we may collect include: Internet protocol address and type of browser you are using (e.g., Firefox, Internet Explorer, Chrome), the type of operating system you are using, (e.g., Microsoft Windows or Mac OS), the domain name of your Internet service provider (e.g., Comcast, Verizon or AT&amp;T), the web pages you have visited, sites visited before and after you visit our site, the type of handheld or mobile device used to view the site (e.g., iPhone, Samsung device), location information, the content you have accessed and the advertisements you have been shown and/or clicked on.</p>

<p>You also can engage with our content on or through third-party social networking sites, such as Facebook, Instagram, Turner Network, Twitter, Golf Channel, Post Beyond, Vimeo or third-party social media plug-ins and applications. When you engage with our content on or through third party sites, plug-ins and applications, you may allow us to have access to certain information from your social media profile (e.g., name, e-mail address, photo, gender, birthday, location, your list of friends, people you follow and/or who follow you, the posts or the ‘likes’ you make) to deliver the content or as part of the operation of the application. We may also obtain information (e.g., content viewed, game performance, high scores, and information about advertisements within the content you have been shown or may have clicked on,) from your interaction with our content.</p>

<p>When you provide information from your social media account, it can help enable us to do things like (1) give you exclusive content, (2) personalize your online experience with us within and outside our applications or websites, and (3) contact you through the social networking sites or directly by sending you the latest news, special offerings, and rewards. When you provide Personal Data to us in a public area through an application, on our sites, or on social networking sites, it may be publicly viewed by other members of these sites and we cannot prevent further use of the information by third parties.</p>

<p>Through some social networking sites’ privacy settings, you can control what data you share. For more information about how social networking sites handle your Personal Data, please refer to their privacy policies and terms of use.</p>

<p>We may also receive Personal Data about you from sources other than you, for example, from other golf industry companies.  Some of these sources of data may be publicly available sources of information about you, such as data aggregators.</p>

<p>You are not required by law to provide us with your Personal Data.  However, some Personal Data is necessary for us to provide our services and offerings to you, so if you do not provide it to us you may not be able to partake in those services or offerings.  As an example, PGA Members are required to provide us Personal Data (including for example name, date of birth, phone number, email address and other contact information, and information about their professional golfing history and career) in order to be PGA Members. </p>

<h3 id="how-we-use-the-personal-data-we-collect">How We Use the Personal Data we collect.</h3>

<p>We use your Personal Data as necessary to perform under a contract we have with you, such as a PGA Member agreement, or to take steps at your request prior to entering into a contract with you.  We also use your Personal Data to serve our legitimate interests in communicating with members, prospective members, customers and prospective customers in order to inform them of, and improve, our offerings, but only if our legitimate interests are not outweighed by your own interests, rights or freedoms to control the use of your Personal Data.  Finally, we may also use your Personal Data with your consent, such as when you sign-up to receive optional communications from us.  Examples of these uses of your Personal Data follow:</p>

<p>We may use the Personal Data you provide about yourself to fulfill your requests for our programs and services, to respond to your inquiries about our services, to assist in your education and employment requests, to  offer you other programs or services that we believe may be of interest to you, to enforce the legal terms that govern your use of our sites, and/or for the purposes for which you provided the information.</p>

<p>We sometimes use this information to communicate with you, such as to notify you of PGA related opportunities and services, or when we make changes to our terms of use, to fulfill a request by you for an online newsletter, or to contact you about your membership with us. The information we collect is used to provide a personalized experience.  </p>

<p>We may on occasion combine information we collect through our sites with information that we collect from other sources, such as web sites related to events that we run.</p>

<p>We use the information that we collect to improve the design and content of our sites, to deliver more relevant marketing messages and advertisements and to enable us to personalize your Internet experience. We also may use this information to analyze usage of our sites, as well as to offer you products, programs, or services.</p>

<h3 id="personal-data-sharing-and-disclosure">Personal Data Sharing and Disclosure</h3>

<p>We disclose your Personal Data to the regional section of PGA of America to which you belong, or to which you are eligible to belong based on your postal address.</p>

<p>We may disclose Personal Data in response to legal process, for example, in response to a court order or a subpoena. We also may disclose such information in response to a law enforcement agency’s request, or where we believe it is necessary to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our terms of use, to verify or enforce compliance with the policies governing our sites and applicable laws or as otherwise required or permitted by law or consistent with legal requirements.</p>

<p>We may disclose Personal Data if you indicate that information is for public usage or for other credentialed members within the site. </p>

<p>In addition, we may transfer Personal Data about you if we, or one of our business units, are acquired by, sold to, merged with or otherwise transferred to another entity.</p>

<p>Our agents and contractors who have access to Personal Data, are required to protect this information in a manner that is consistent with this Privacy Policy and are not authorized to use the information for any purpose other than to carry out the services they are performing for us.</p>

<p>Although we take appropriate measures to safeguard against unauthorized disclosures of information, we cannot assure you that Personal Data that we collect will never be disclosed in a manner that is inconsistent with this Privacy Policy.</p>

<p>We may disclose personal identifiable information to third parties whose practices are not covered by this privacy statement (e.g., other marketers, magazine publishers, retailers, participatory databases, and non-profit organizations) that want to market products or services to you.  You may opt-out of this by emailing us at <a href="mailto:golf@pgahq.com">golf@pgahq.com</a>.</p>

<p>If you have signed up to receive our e-mails and prefer not to receive marketing information from us, follow the “unsubscribe” instructions provided on any marketing e-mail you receive from us.</p>

<p>To provide visitors with additional or more relevant product and service opportunities, we may share information with third parties. You may opt-out of this by emailing us at <a href="mailto:golf@pgahq.com">golf@pgahq.com</a>.</p>

<h3 id="cookies--similar-technologies">Cookies &amp; Similar Technologies</h3>

<p>To enhance your online experience, we use “cookies” or similar technologies. Cookies are text files placed in your computer’s browser to store your preferences. Cookies do not contain Personal Data; however, once you choose to furnish a site with Personal Data, this information may be linked to the data stored in the cookie.</p>

<p>We use cookies to understand site and Internet usage and to improve or customize the content, offerings or advertisements on our sites. For example, we may use cookies to personalize your experience at our sites (e.g., to recognize you by name when you return to a site), save your username in password-protected areas, and enable you to use shopping carts on our sites. We also may use cookies to help us offer you products, programs, or services that may be of interest to you and to deliver relevant advertising. </p>

<p>We, our third-party service providers, advertisers or our partners also may use cookies to manage and measure the performance of advertisements displayed on or delivered by or through partners like the Turner Network and/or other networks or sites. This also helps us, our service providers and partners provide more relevant advertising. For more information regarding third-party ad servers, see “Collection of Information by Third-Party Sites, Ad Servers, and Sponsors” below.</p>

<p>At this time our sites do not recognize automated browser signals regarding tracking mechanisms, which may include “do not track” instructions. You can change your privacy preferences regarding the use of cookies and similar technologies through your browser. You may set your browser to accept all cookies, block certain cookies, require your consent before a cookie is placed in your browser, or block all cookies. Blocking all cookies will affect your online experience and may prevent you from enjoying the full features offered at our sites. Please consult the “Help” section of your browser for more information.</p>

<h3 id="collection-of-personal-data-by-third-party-sites-ad-servers-and-sponsors">Collection of Personal Data by Third-Party Sites, Ad Servers, and Sponsors</h3>

<p>Our site contains links to or integrations with other sites such as Facebook, Twitter, LinkedIn, Instagram etc., whose information practices may be different than ours. Visitors should consult the other sites’ privacy notices as we have no control over information that is submitted to, or collected by, these third parties.</p>

<p>Sites covered by this privacy statement may offer content (e.g., contests, sweepstakes, promotions, games, applications, or social network integrations) that is sponsored by or co-branded with identified third parties. By virtue of these relationships, the third parties may obtain Personal Data that visitors voluntarily submit to participate in the site activity. We have no control over these third parties’ use of this information. The site will notify you at the time of requesting personally identifiable information if these third parties will obtain such information.</p>

<p>We also use the services of reputable third parties to provide us with data collection, reporting, ad response measurement, and site analytics, as well as to assist with delivery of relevant marketing messages and advertisements. These third parties may view, edit or set their own cookies. We, our third party service providers, advertisers and/or partners may also place web beacons for these third parties. The use of these technologies by these third parties is subject to their own privacy policies and is not covered by this privacy statement.</p>

<p>Among other things, these services enable us to generate analytics reports on the usage of our web site. To opt-out of your web site usage being included in our Google Analytics reports, you may follow <a href="https://tools.google.com/dlpage/gaoptout/" target="_blank">these</a> instructions.</p>

<h3 id="international-transfer">International Transfer</h3>

<p>The PGA and our partners operate globally so it may be necessary to transfer your information internationally.  We will process your Personal Data outside the European Economic Area (EEA).  Your information will likely be transferred to and processed in the United States where our central databases operate. The data protection and other laws of other countries, such as the United States, may not be as comprehensive as those in your country.</p>

<h3 id="changing-or-deleting-your-personal-data">Changing or Deleting Your Personal Data</h3>

<p>It is important that the Personal Data we hold about you is accurate and current. Please keep us informed if your Personal Data changes during your relationship with us.</p>

<p>If you are a registered user, you may review, update, correct or delete the Personal Data provided in your registration or account profile by changing information in your profile.</p>

<h3 id="retention-of-personal-data">Retention of Personal Data</h3>

<p>We will endeavor not to keep Personal Data in a form that allows you to be identified for any longer than is reasonably necessary for achieving the permitted purposes. At the end of the applicable retention period, we may destroy, erase from our systems, or anonymize Personal Data as part of such efforts. To determine the appropriate retention period for Personal Data, we consider the amount, nature, and sensitivity of the Personal Data, the potential risk of harm from unauthorized use or disclosure of the Personal Data, the purposes for which we process the Personal Data and whether we can achieve those purposes through other means, and the applicable legal requirements.</p>

<h3 id="changes-to-this-privacy-policy">Changes to this Privacy Policy</h3>

<p>We may change this Privacy Policy from time to time.  When we do, we will let you know by posting the changed Privacy Policy on this page with a new “Effective Date.”  In some cases (for example, if we significantly expand our use or sharing of your Personal Data), we may also tell you about changes by additional means, such as by sending an e-mail to the e-mail address we have on file for you.   In some cases, we may request your consent to the changes.</p>

<p><strong>Compliance.</strong> We regularly review our compliance with our Privacy Policy. When we receive formal written complaints, we will contact the person who made the complaint to follow up. We work with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of Personal Data that we cannot resolve with our users directly.</p>

<p><strong>Your legal rights.</strong> Under certain circumstances, you have rights under data protection laws in relation to your Personal Data. You may have the right to:</p>

<ul>
  <li>
<em>Request access to your Personal Data.</em> This is commonly known as a <strong>“data subject access request”</strong>. This enables you to receive a copy of the Personal Data that we hold about you and to check that we are lawfully processing it.</li>
  <li>
<em>Request correction of the Personal Data that we hold about you.</em> This enables you to have any incomplete or inaccurate data we hold about you corrected, though we may need to verify the accuracy of the new data you provide to us.</li>
  <li>
<em>Request erasure of your Personal Data.</em> This enables you to ask us to delete or remove Personal Data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your Personal Data where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully or where we are required to erase your Personal Data to comply with local law. Note, however, that we may not always be able to comply with your request of erasure for specific legal reasons which will be notified to you, if applicable,</li>
  <li>
<em>Request restriction of processing of your Personal Data.</em> This enables you to ask us to suspend the processing of your Personal Data in the following scenarios: (a) if you want us to establish the data’s accuracy; (b) where our use of the data is unlawful but you do not want us to erase it; (c) where you need us to hold the data even if we no longer require it as you need to establish, exercise or defend legal claims; or (d) you have objected to our use of your data but we need to verify whether we have overriding legitimate grounds to use it.</li>
  <li>
<em>Withdraw consent at any time where we are relying on consent to process your Personal Data.</em> However, this will not affect the lawfulness of any processing carried out before you withdraw your consent. If you withdraw your consent, we may not be able to provide certain products or services to you. We will advise you if this is the case at the time you withdraw your consent.</li>
  <li><em>Request that your Personal Data be given to you, or to another company, in a portable format.</em></li>
  <li>
<em>Object to automated decision making</em> that materially impacts you, <em>direct marketing</em>, and processing for research or statistical purposes</li>
</ul>

<p>If you wish to exercise any of the rights set out above, please contact our data privacy director at:  <a href="mailto:golf@pgahq.com">golf@pgahq.com</a>.   We will endeavor to respond to your inquiry within 30 days.</p>

<p><em>The Privacy Policy posted on this site was updated on September 21, 2018.</em></p>