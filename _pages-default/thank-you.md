---
title: Thank you
hide-title: true
layout: default-jumbo
permalink: /contact-pga/thank-you/
---
<section class="alert-section">
  <div class="container text-center">
    <img data-src="/assets/images/tick-inside-circle.svg" alt="Circle Icon" src="/assets/images/tick-inside-circle.svg">
    <h1 class="my-4">Thank you for getting in touch!</h1>
    <p class="lead">We appreciate you contacting us. We will get back to you shortly.
      <br>
      Have a great day!
    </p>
    <a class="btn btn-info mt-2" href="/">Back to Home</a>
  </div>
</section>