---
title: How to Become A PGA Member
layout: default-jumbo
navbar-secondary: membership
heading: How to Become a PGA Member
intro: Pathways to PGA Membership
---
<div class="jumbotron jumbotron-fluid jumbotron-thin editable">
  <img class="jumbotron-image" src="/uploads/become-a-member-header.jpg" data-src="/uploads/become-a-member-header.jpg" alt="">
  <div class="jumbotron-body text-center text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>How to Become a PGA Member</h1>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="section-indent">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="tiles-wrapper tiles-stretch">
          <div class="benefits-items text-center editable">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h3 class="text-secondary">Pathways to PGA Membership</h3>
                <p class="lead mb-0 text-secondary">There are two primary pathways that lead to PGA Membership. If you are a current or former LPGA Member, you may be eligible for the test-out option. <a href="/articles/lpga-test-out-options/">Read more here</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-footer">
  <div class="container">
    <div class="row  justify-content-center">
      <div class="col-md-6">
        <div class="bg-navy p-5 text-center editable">
          <div class="tiles-icon-header">
            <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewbox="0 0 99 99" version="1.1">
  <title>5C3B5768-79BE-4B49-A916-6137F5C72DFB</title>
  <desc>Created with sketchtool.</desc>
  <defs>
    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
    <filter x="-22.2%" y="-15.8%" width="144.3%" height="144.3%" filterunits="objectBoundingBox" id="filter-2">
      <feoffset dx="0" dy="5" in="SourceAlpha" result="shadowOffsetOuter1"></feoffset>
      <fegaussianblur stddeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></fegaussianblur>
      <fecolormatrix values="0 0 0 0 0 0 0 0 0 0.137254902 0 0 0 0 0.294117647 0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></fecolormatrix>
    </filter>
  </defs>
  <g id="UI" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="PGA_UI_Membership" transform="translate(-367.000000, -534.000000)">
      <g id="Group-4" transform="translate(145.000000, 539.000000)">
        <g id="Card">
          <g id="Group" transform="translate(40.000000, 0.000000)">
            <g id="Group-2">
              <g id="Group-3">
                <g id="White-icon" transform="translate(192.000000, 0.000000)">
                  <g id="icon-education">
                    <use fill="#FFFFFF" fill-rule="evenodd"></use>
                  </g>
                  <g id="path-1-link" fill="#FFFFFF">
                    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
                  </g>
                  <g id="Associate-icon-gold" transform="translate(4.000000, 4.000000)" stroke="#B4975A">
                    <path d="M10.3747173,10.3747173 C24.1925324,-3.44309787 46.5782949,-3.46044015 60.3747173,10.3359822 C74.1711396,24.1324046 74.1537973,46.5181671 60.3359822,60.3359822 C46.5181671,74.1537973 24.1324046,74.1711396 10.3359822,60.3747173 C-3.46044015,46.5782949 -3.44309787,24.1925324 10.3747173,10.3747173 Z" id="icon-membership" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="80,4,4,4"></path>
                    <g id="golf-(1)" transform="translate(23.000000, 18.000000)" stroke-linecap="square" stroke-width="1.75">
                      <path d="M22.5,9.5 L13.5,9.5 L13.5,5.7878265 C13.5,5.0035395 13.9584064,4.291634 14.6723938,3.9670934 L21.6723938,0.7852754 C22.9965935,0.183367 24.5,1.151431 24.5,2.6060083 L24.5,7.5 C24.5,8.6045694 23.6045704,9.5 22.5,9.5 Z" id="Shape"></path>
                      <path d="M13.5,9.5 L13.5,19.5" id="Shape"></path>
                      <path d="M2.5,13.5 L9.5,13.5 L9.5,10.7997465 C9.5,10.0093689 9.0345325,9.2931261 8.3122768,8.9721231 L3.3122768,6.7499008 C1.9897022,6.1620898 0.5,7.1302071 0.5,8.5775242 L0.5,11.5 C0.5,12.6045694 1.3954306,13.5 2.5,13.5 Z" id="Shape"></path>
                      <path d="M9.5,13.5 L9.5,19.5" id="Shape"></path>
                      <rect id="Rectangle-path" x="2.5" y="19.5" width="18" height="4"></rect>
                      <path d="M18,24 L18,34" id="Shape"></path>
                      <path d="M6,34 L6,24.0237228" id="Shape"></path>
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </g>
</svg>
          </div>
          <h3 class="mt-4 mb-3">The PGM Associate Program</h3>
          <p>To become a PGA Member, you'll need to become a registered Associate before completing the PGA Professional Golf Management (PGA PGM) Program--an award-winning educational program designed for aspiring PGA Professionals and focuses on The People,
            The Business and The Game. </p>
          <a href="/how-to-become-a-pga-member/associate-program" class="btn btn-primary mt-1">Learn more</a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="bg-navy p-5 text-center editable">
          <div class="tiles-icon-header">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewbox="0 0 99 99" version="1.1">
  <title>D0E1D5FC-617D-4578-A638-8E337B884CA2</title>
  <desc>Created with sketchtool.</desc>
  <defs>
    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
    <filter x="-22.2%" y="-15.8%" width="144.3%" height="144.3%" filterunits="objectBoundingBox" id="filter-2">
      <feoffset dx="0" dy="5" in="SourceAlpha" result="shadowOffsetOuter1"></feoffset>
      <fegaussianblur stddeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></fegaussianblur>
      <fecolormatrix values="0 0 0 0 0 0 0 0 0 0.137254902 0 0 0 0 0.294117647 0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></fecolormatrix>
    </filter>
  </defs>
  <g id="UI" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="PGA_UI_Membership" transform="translate(-934.000000, -524.000000)">
      <g id="Group-5" transform="translate(712.000000, 529.000000)">
        <g id="Card">
          <g id="Group" transform="translate(40.000000, 0.000000)">
            <g id="Group-2">
              <g id="Group-3">
                <g id="white-icon-2" transform="translate(192.000000, 0.000000)">
                  <g id="icon-education">
                    <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
                    <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>
                  </g>
                  <g id="path-1-link" fill="#FFFFFF">
                    <circle id="path-1" cx="39.5" cy="39.5" r="39.5"></circle>
                  </g>
                  <g id="University-icon-gold" transform="translate(4.000000, 4.000000)" stroke="#B4975A">
                    <path d="M10.3747173,10.3747173 C24.1925324,-3.44309787 46.5782949,-3.46044015 60.3747173,10.3359822 C74.1711396,24.1324046 74.1537973,46.5181671 60.3359822,60.3359822 C46.5181671,74.1537973 24.1324046,74.1711396 10.3359822,60.3747173 C-3.46044015,46.5782949 -3.44309787,24.1925324 10.3747173,10.3747173 Z" id="icon-membership" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="80,4,4,4"></path>
                    <g id="bank" transform="translate(20.000000, 20.000000)" stroke-linecap="square" stroke-width="1.75">
                      <g id="Group-6">
                        <polygon id="Shape" points="15 0.483870968 0.483870968 8.22580645 0.483870968 12.0967742 29.516129 12.0967742 29.516129 8.22580645"></polygon>
                        <path d="M3.38709677,22.7419355 L3.38709677,15" id="Shape"></path>
                        <path d="M11.1290323,22.7419355 L11.1290323,15" id="Shape"></path>
                        <path d="M18.8709677,22.7419355 L18.8709677,15" id="Shape"></path>
                        <path d="M26.6129032,22.7419355 L26.6129032,15" id="Shape"></path>
                        <circle id="Oval" cx="15" cy="7.25806452" r="1.93548387"></circle>
                        <rect id="Rectangle-path" x="0.483870968" y="25.6451613" width="29.0322581" height="3.87096774"></rect>
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </g>
</svg>
          </div>
          <h3 class="mt-4 mb-3">The PGM University Program</h3>
          <p>The PGA Golf Management University Program accredited by The Professional Golfers’Association of America (PGA) is a college degree program designed to attract and educate bright, highly motivated men and women to service all aspects of the industry
            and produce PGA Members.</p>
          <a href="/how-to-become-a-pga-member/university-program" class="btn btn-primary mt-1">Learn more</a>
        </div>
      </div>
    </div>
  </div>
</section>