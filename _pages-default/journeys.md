---
title: Journeys
layout: default-jumbo
header-style-filled: false
hide-secondary-navigation: true
permalink:
hero:
  title: The PGA of America
  image_link: /uploads/about-header.jpg
  lead: Wherever your golf journey is headed, let’s get you there.
journeys_videos:
  - image_link: /uploads/pga-journey-bg4.png
    text: Start Your Journey in Golf
    video_link: https://player.vimeo.com/video/283057516
    button_text: Find a PGA Coach
    button_link: https://www.pga.org/directory?query=&hits=12&idx=MemberFacilityDirectory&page=0&dFR%5Bmember_type_label%5D%5B0%5D=MB&showconnections=false&is_v=1
  - image_link: /uploads/pga-journey-bg3.png
    text: Let's Get You There
    video_link: https://player.vimeo.com/video/271492220
    button_text: Hire a PGA Member
    button_link: https://beta.pga.org/career-services/hire-a-pga-professional/post-an-open-position
  - image_link: /uploads/start-your-journey-in-golf-2.png
    text: Become a Leader in golf
    video_link: https://player.vimeo.com/video/332320323
    button_text: Become a PGA Member
    button_link: https://beta.pga.org/membership
copy:
  title: Your golf journey begins the very first time you pick up a golf club.
  text1: Whether you work in the golf business or are taking your first swings; if you are picking the game back up after some time off or have won at the highest level of competitive golf; every journey is unique and no one can take that journey alone.
  text2: On the course, we can help you on your journey to learning the basics of the game, breaking 100 for the first time, winning your club championship, earning a college scholarship or even competing for Major Championships on the PGA and LPGA Tours. In business it’s no different. We can help clubs elevate the golf experience, raise satisfaction rates, increase play and boost financial performance. The education and knowledge behind every PGA Member allows them to assist you on your golf journey, no matter how big or small. 
  highlight_text: No matter what your goals are within the game, PGA Members are uniquely qualified to help you on your golf journey.
---
<div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
  <ol class="carousel-indicators d-none d-md-flex">
    <li class="active" data-slide-to="0" data-target="#carouselExampleIndicators"></li>
    <li data-slide-to="1" data-target="#carouselExampleIndicators"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active"><img alt="Your Journey Our Business" class="d-none d-block d-md-none w-100 h-80" src="/assets/images/journey-hero-1-mobile.jpg"> <img alt="Your Journey Our Business" class="d-none d-md-block w-100" src="/assets/images/Hero1.jpg"></div>
    <div class="carousel-item"><img alt="Your Journey Our Passion" class="d-none d-block d-md-none w-100" src="/assets/images/journey-hero-2-mobile.jpg"> <img alt="Your Journey Our Passion" class="d-none d-md-block w-100" src="/assets/images/Hero2.jpg"></div>
  </div>
</div><!-- PGA JOURNEYS SECTION -->
<section class="editable pt-0 pb-0">
  <div class="row no-gutters bg-navy py-md-3">
    <div class="col-lg-3 d-lg-block">
      <div class="custom-size">
        <div class="journey-logo"></div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="journeys-container">
        <div class="container">
          <div class="journeys-header">
            <h3 class="title">Wherever your golf journey is headed, let’s get you there.</h3>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <button class="journeys-video" data-target="#pga-journey-1" data-toggle="modal"><img class="w-100" src="/uploads/pga-journey-bg4.png"></button>
              <div aria-hidden="true" aria-labelledby="pga-journey-1" class="modal fade" id="pga-journey-1" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-1">Start Your Journey in Golf</h3><i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/283057516"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p><a class="btn btn-primary btn-block cta-button" href="https://www.pga.org/directory?query=&amp;hits=12&amp;idx=MemberFacilityDirectory&amp;page=0&amp;dFR%5Bmember_type_label%5D%5B0%5D=MB&amp;showconnections=false&amp;is_v=1">Find a PGA Coach</a></p>
            </div>
            <div class="col-md-4 col-sm-12">
              <button class="journeys-video" data-target="#pga-journey-2" data-toggle="modal"><img class="w-100" src="/uploads/pga-journey-bg3.png"></button>
              <div aria-hidden="true" aria-labelledby="pga-journey-2" class="modal fade" id="pga-journey-2" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-2">Let's Get You There</h3><i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/271492220"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p><a class="btn btn-primary btn-block cta-button" href="https://beta.pga.org/career-services/hire-a-pga-professional/post-an-open-position">Hire a PGA Member</a></p>
            </div>
            <div class="col-md-4 col-sm-12">
              <button class="journeys-video" data-target="#pga-journey-3" data-toggle="modal"><img class="w-100" src="/uploads/start-your-journey-in-golf-2.png"></button>
              <div aria-hidden="true" aria-labelledby="pga-journey-3" class="modal fade" id="pga-journey-3" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header align-items-center">
                      <h3 class="modal-title" id="pga-journey-3">Become a Leader in golf</h3><i aria-label="Close" class="close fa fa-times fa-2x text-navy" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/332320323"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p><a class="btn btn-primary btn-block cta-button" href="https://beta.pga.org/membership">Become a PGA Member</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="py-3">
  <div class="container journey-copy">
    <div class="journey-copy-wrapper">
      <div class="mb-4">
        <h1 class="text-secondary pb-sm-3">Your golf journey begins the very first time you pick up a golf club.</h1>
        <ul class="social-share">
          <li>
            <div class="btn-share social-share-label">
              Share
            </div>
          </li>
          <li>
            <a class="btn-share btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://beta.pga.org//journeys/" rel="noopener" target="_blank" title="Share on Facebook"></a>
          </li>
          <li>
            <a class="btn-share btn-twitter" href="https://twitter.com/intent/tweet?text=Journeys&amp;url=https://beta.pga.org//journeys/&amp;via=PGA&amp;related=PGA" rel="noopener" target="_blank" title="Share on Twitter"></a>
          </li>
          <li>
            <a class="btn-share btn-google" href="https://plus.google.com/share?url=https://beta.pga.org//journeys/" rel="noopener" target="_blank" title="Share on Google+"></a>
          </li>
          <li>
            <a class="btn-share btn-addthis" href="https://www.addthis.com/bookmark.php?url=https://beta.pga.org//journeys/&amp;title=Journeys" rel="noopener" target="_blank"></a>
          </li>
        </ul>
      </div>
      <p>Whether you work in the golf business or are taking your first swings; if you are picking the game back up after some time off or have won at the highest level of competitive golf; every journey is unique and no one can take that journey alone.</p>
      <p class="journey-quote">No matter what your goals are within the game, PGA Members are uniquely qualified to help you on your golf journey.</p>
      <p>On the course, we can help you on your journey to learning the basics of the game, breaking 100 for the first time, winning your club championship, earning a college scholarship or even competing for Major Championships on the PGA and LPGA Tours. In business it’s no different. We can help clubs elevate the golf experience, raise satisfaction rates, increase play and boost financial performance. The education and knowledge behind every PGA Member allows them to assist you on your golf journey, no matter how big or small.</p>
      <p class="journey-bold">Wherever your golf journey is headed, let’s get you there.</p>
      <div class="row">
        <div class="col-md-4 col-sm-12">
          <a class="btn btn-info btn-block mb-3 mb-md-0" data-cms-editor-link-style="undefined" href="https://www.pga.org/directory?query=&amp;hits=12&amp;idx=MemberFacilityDirectory&amp;page=0&amp;dFR%5Bmember_type_label%5D%5B0%5D=MB&amp;showconnections=false&amp;is_v=1">Find a PGA Coach</a>
        </div>
        <div class="col-md-4 col-sm-12">
          <a class="btn btn-info btn-block mb-3 mb-md-0" data-cms-editor-link-style="undefined" href="https://beta.pga.org/career-services/hire-a-pga-professional/post-an-open-position">Hire a PGA Member</a>
        </div>
        <div class="col-md-4 col-sm-12">
          <a class="btn btn-info btn-block mb-3 mb-md-0" data-cms-editor-link-style="undefined" href="https://beta.pga.org/membership">Become a PGA Member</a>
        </div>
      </div>
    </div>
  </div>
</section>