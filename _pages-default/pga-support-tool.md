---
title: PGA Support Tool
layout: default-jumbo
permalink: /pga-support-tool/
---
<div class="container mt-5 pt-5"><div class="row"><div class="col-lg-6 col-md-10 mx-auto"><div class="box-shadow-1 bg-white"><div class="text-center p-5">
    <svg width="110" height="110" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><circle id="b" cx="45" cy="45" r="45"></circle><filter x="-19.4%" y="-13.9%" width="138.9%" height="138.9%" filterunits="objectBoundingBox" id="a"><feoffset dy="5" in="SourceAlpha" result="shadowOffsetOuter1"></feoffset><femorphology radius="2" in="SourceAlpha" result="shadowInner"></femorphology><feoffset dy="5" in="shadowInner" result="shadowInner"></feoffset><fecomposite in="shadowOffsetOuter1" in2="shadowInner" operator="out" result="shadowOffsetOuter1"></fecomposite><fegaussianblur stddeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></fegaussianblur><fecolormatrix values="0 0 0 0 0 0 0 0 0 0.137254902 0 0 0 0 0.294117647 0 0 0 0.15 0" in="shadowBlurOuter1"></fecolormatrix></filter></defs><g fill="none" fill-rule="evenodd"><path stroke="#B4975A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M76 67H35l20.5-41z"></path><path d="M54.336 43.6h3.256l-.55 10.296h-2.178L54.336 43.6zm1.628 15.554c-.513 0-.939-.165-1.276-.495a1.64 1.64 0 0 1-.506-1.221c0-.484.169-.887.506-1.21.337-.323.763-.484 1.276-.484.499 0 .913.161 1.243.484.33.323.495.726.495 1.21s-.165.891-.495 1.221c-.33.33-.744.495-1.243.495z" fill="#B4975A"></path><g transform="translate(10 5)"><circle stroke="#B4975A" stroke-width="2" stroke-linejoin="square" cx="45" cy="45" r="44"></circle></g></g></svg>

<h3 class="text-secondary mb-4">PGA Support Tool has been decommissioned.</h3>
<p class="text-secondary">Please note, the website pgasupport.pgalinks.com is no longer available. We apologize for any inconvenience this may cause.</p>
<p class="text-secondary">Please send an email to the Helpdesk with any issues or questions at <a class="text-info" href="mailto:helpdesk@pgahq.com">helpdesk@pgahg.com</a></p> <a class="btn btn-info mt-4" href="mailto:helpdesk@pgahq.com">Email Helpdesk</a>
</div></div></div></div></div>