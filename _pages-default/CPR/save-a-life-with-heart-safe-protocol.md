---
title: Save a Life
layout: default-no-sidebar
permalink:
---
<h3 id="did-you-know-">Did you know …</h3>

<ul>
  <li><strong>Golf courses are the 5th most common place for sudden cardiac arrest?</strong></li>
  <li><strong>92% of Sudden Cardiac Arrest Victims die before reaching the hospital</strong></li>
</ul>

<p>Everyone can save a life so be sure everyone at your facility knows hands-only CPR. It takes less than a minute to learn.</p>

<p><img src="/uploads/hands-on-public.jpg" alt=""></p>

<h4 id="pga-heart-safe-protocol">PGA Heart-Safe Protocol</h4>

<ul>
  <li>
<strong><a href="/Document-Library/hands-only-cpr-poster-public.pdf">Download your</a></strong> “Check, Call, Compress” poster and display next to the AEDs</li>
  <li>Have a minimum two AED’s at your facility</li>
  <li>Watch the hands-only instructional 30-second video below at least once a year</li>
  <li>
<strong><a href="/Document-Library/hands-only-cpr-tent-card-w-brooks-koepka.pdf">Download a tent card</a></strong> featuring 2018 PGA Champion Brooks Koepka</li>
</ul>

<iframe src="https://player.vimeo.com/video/333980808?title=0&amp;byline=0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="640" height="360" frameborder="0"></iframe>

<p><br>For more information, visit <a href="http://" class="cc-active">HandsOnly.org</a>.</p>

<p><img src="/uploads/logo-hand-only-public.jpg" alt=""><br> </p>