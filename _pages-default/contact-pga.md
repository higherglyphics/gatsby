---
title: Contact Us
layout: default-jumbo
---
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="content-default">
        <form class="contact-form px-4" id="contactForm" action="https://formspree.io/membership@pgahq.com" method="POST">
          <div class="alert alert-icon alert-success collapse" role="alert">Thank you for your submission.</div>
          <div class="heading">
            <h1 class="text-secondary mb-4">Contact the PGA</h1>
          </div>
          <p>Please use the form below to e-mail your comments, questions or suggestions to the President of The PGA or a senior staff member at national headquarters.</p>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="firstName">First Name*</label>
                <input type="text" class="form-control" id="firstName" name="firstName" size="60" required="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="lastName">Last Name*</label>
                <input type="text" class="form-control" id="lastName" name="lastName" size="60" required="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 d-flex flex-column justify-content-between">
              <div class="form-group">
                <label for="edit-submitted-email">Email*</label>
                <input type="email" name="_replyto" class="form-control" id="edit-submitted-email" size="60" required="">
              </div>
              <button type="submit" class="btn btn-lg btn-info d-none d-md-block">Submit</button>
            </div>
            <div class="col-md-6">
              <div class="form-group mb-md-0">
                <label for="lastName">Message</label>
                <textarea class="form-control" id="edit-submitted-question" name="question" cols="60" rows="4"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <button type="submit" class="btn btn-lg btn-info d-md-none d-block">Submit</button>
            </div>
            </div>
            <input type="text" name="_gotcha" style="display:none">
            <input type="hidden" name="_next" value="/contact-pga/thank-you/">
            <input type="hidden" name="_subject" value="Contact">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="map-wrapper">
  <div class="tiles-wrapper p-5">
    <div class="tiles-icon-header">
      <img src="/assets/images/icons/icon-pin.svg" width="60">
      <!-- <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 40 51" version="1.1">
  <title>maps-and-flags</title>
  <desc>Created with Sketch.</desc>
  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="maps-and-flags" fill-rule="nonzero">
      <path d="M39.9847236,19.807125 C39.9847236,35.6674277 21.9192965,50.3562246 21.9192965,50.3562246 C20.8637186,51.2145586 19.1361809,51.2145586 18.080603,50.3562246 C18.080603,50.3562246 0.0151758794,35.6674277 0.0151758794,19.807125 C0.0153768844,8.86792383 8.96281407,0 20,0 C31.0371859,0 39.9847236,8.86792383 39.9847236,19.807125 Z" id="Shape" fill="#B4975A"/>
      <path d="M20,29.7722461 C14.4558794,29.7722461 9.94562814,25.301877 9.94562814,19.8072246 C9.94562814,14.3125723 14.4560804,9.84200391 20,9.84200391 C25.5439196,9.84200391 30.0543719,14.312373 30.0543719,19.8070254 C30.0543719,25.3016777 25.5441206,29.7722461 20,29.7722461 Z" id="Shape" fill="#ECD9B3"/>
    </g>
  </g>
</svg> -->
    </div>
    <h3 class="text-secondary">PGA of America Headquarters</h3>
    <p><strong>100 Avenue of the Champions <br>Box 109601 Palm Beach Gardens, FL 33410-9601</strong></p>
    <p><strong>General:</strong> 561-624-8400 or 1-800-477-6465</p>
    <p><strong>Membership Information Services Center (MISC):</strong> 1-800-474-2776</p>
  </div>
  <div id="map"></div>
</div>
<!-- Googple Map Init -->
<script>
  var map;

  function initMap() {
    // Styles a map in night mode.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 26.8367,
        lng: -80.13655
      },
      zoom: 16,
      styles: [{
          elementType: 'geometry',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#746855'
          }]
        },
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{
            color: '#263c3f'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#6b9a76'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{
            color: '#38414e'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#212a37'
          }]
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#9ca5b3'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{
            color: '#746855'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#1f2835'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#f3d19c'
          }]
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{
            color: '#2f3948'
          }]
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{
            color: '#17263c'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#515c6d'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#17263c'
          }]
        }
      ]
    });
    var image = '/assets/images/icons/icon-pin.svg';
    var beachMarker = new google.maps.Marker({
      position: {
        lat: 26.8367,
        lng: -80.13655
      },
      map: map,
      icon: image
    });
  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjGuD4qHG61FEOcAGDxqyt2xIK_hFBI-0&amp;callback=initMap" async="" defer></script>