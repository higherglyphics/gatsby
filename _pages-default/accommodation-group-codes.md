---
title: Accommodation Group Codes
layout: default-no-sidebar
permalink: /accommodation-group-codes/
---
<p>For questions/problems regarding housing for any PGA of America event listed below, please contact <a href="mailto:pgahotels@tournamenthotels.com">pgahotels@tournamenthotels.com</a> or (888) 417-6446, Monday–Friday, 9:00 a.m–5:00 p.m. (ET).</p>

<p><a href="https://pse.tournamenthotels.com/pse/pga" class="btn btn-outline-info" target="_blank">Book your accommodations for each of the Championships listed below.</a></p>

<table>
  <tbody>
    <tr>
      <td><img src="/assets/images/logo-kpmg-2019.jpg" alt=""></td>
      <td>
<a href="https://pse.tournamenthotels.com/pse/pga" target="_blank">65th KPMG Women’s PGA Championship</a><br>Hazeltine National Golf Club<br>Chaska, MN<br>June 18-23</td>
    </tr>
  </tbody>
</table>

<h4> </h4>

<table>
  <tbody>
    <tr>
      <td><img src="/uploads/white-block.jpg" alt="" width="200" height="200"></td>
      <td>
<a href="https://pse.tournamenthotels.com/pse/pga" target="_blank">44th Junior PGA Championship</a><br>Keney Park Golf Course<br>Hartford, CT<br>July 8-12 (Girls)<br>July 30-August 2 (Boys)</td>
    </tr>
  </tbody>
</table>

<h4 id="-1"> </h4>

<table>
  <tbody>
    <tr>
      <td><img src="/assets/images/pga-logo.jpg" alt=""></td>
      <td>
<a href="https://pse.tournamenthotels.com/pse/pga" target="_blank">103rd PGA Annual Meeting</a><br>West Palm Beach, FL<br>November 4-8</td>
    </tr>
  </tbody>
</table>