---
title: PGA Committee Meetings
layout: default-no-sidebar
permalink:
---
<p>Committee Meetings are scheduled for Wednesday, Aug. 7, 2019, at the Hilton Chicago O’Hare Airport Hotel.</p>

<ul>
  <li><strong><a href="/Document-Library/2019-aug7-agenda-committee-meetings2.pdf" target="_blank">Committee Memo</a></strong></li>
  <li><strong><a href="/Document-Library/2019-aug7-chioh-floor-plans.pdf" target="_blank">Hotel Floor Plans</a></strong></li>
  <li><strong><a href="/Document-Library/2019-aug7-committee-expense-report.xlsx" target="_blank">Committee Expense Report</a></strong></li>
</ul>

<p>A room block has been secured at the Hilton Chicago O’Hare Airport.  The room rate is $149.00++ per night. <strong>THE DEADLINE TO BOOK YOUR ROOM IS July 11, 2019. </strong> To book your room, click on the following link:  <a href="https://www.hilton.com/en/hi/groups/personalized/C/CHIOHHH-PGC-20190806/index.jhtml?WT.mc_id=POG" target="_blank">https://www.hilton.com/en/hi/groups/personalized/C/CHIOHHH-PGC-20190806/index.jhtml?WT.mc_id=POG</a></p>

<p><strong>NOTE: Reception and Dinner to be held on Tuesday, Aug. 6 at Gibsons, Rosemont (5464 North River Road, Rosemont, Illinois).</strong> Attire is casual, and buses will depart for the reception and dinner from the Hilton Lobby level at 6:15 p.m.  Attendees may also take Uber/Lyft to and from Gibsons. Dinner begins promptly at 7:00 p.m.</p>

<p>The standard mileage rates for the use of a automobile for (also vans, pickups or panel trucks) for business purposes in 2019 - per the Internal Revenue Service - is 58 cents per mile driven.</p>