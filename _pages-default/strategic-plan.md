---
title: Strategic Plan
jumbotron: true
layout: default-jumbo
body-color: bg-white
navbar-secondary: strategic-plan
permalink: /strategic-plan/
image: /uploads/strategic-plan-header.jpg
partnership_cards:
  - header: Section Partnership
    icon: /assets/images/icons/section-icon.svg
    background: bg-secondary
    lists:
      - Interaction with other members and pros
      - Local seminars and tours
      - Career services at section level
      - Local coaching and player growth
  - header: National Partnership
    icon: /assets/images/icons/national-icon.svg
    background: bg-primary
    lists:
      - Championships and events
      - Career services
      - Coaching and player development
      - Membership services
      - Diversity inclusion
strategic_vision_cards:
  - title: Our constant pursuit of excellence and innovation
    icon: /assets/images/icons/hand-light.svg
  - title: Our culture of teamwork and collaboration
    icon: /assets/images/icons/teamwork-icon.svg
  - title: Our commitment to diversity and inclusion
    icon: /assets/images/icons/group-of-people.svg
measuring_our_performance:
  - title: Operating Criteria
    lists:
      - Member Satisfaction (Net Promoter Score)
      - Unemployment and under-employment rates
      - PGA Professional Compensation Trends
      - Facility Penetration Rates
  - title: Growing the Game and Making a Difference in Opportunities Available in the Game
    lists:
      - Rounds Played
      - Participation Trends – beginner, occasional, core, youth, non-traditional
      - Viewership/Attendance
      - Golfer Spends
  - title: Diversity & Accessibility in the Game
    lists:
      - Golfers – female, minority, other
      - Professionals – female, minority, other
      - Leadership/Staff – female, minority, other
  - title: Public Perception of PGA and Members
    lists:
      - Golfer – Role related to instruction, business and championships
      - Employer – Value of PGA Professional to health of business
  - title: Breadth & Scope of PGA and Member Influence
    lists:
      - Percentage of golfers seek a PGA Professional for instruction
      - Value consumer places on PGA Professional related to facility
      - Media value associated with PSAs, PR and Championships
---
<div class="jumbotron jumbotron-fluid jumbotron-custom-md editable">
  <img alt="" class="jumbotron-image" src="/uploads/strategic-plan-header.jpg">
  <div class="jumbotron-body">
    <div class="container">
      <div class="row">
        <div class="col-8 col-md-7">
          <h1>The PGA of America exists to serve our members and grow the game.</h1>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="py-4">
  <div class="container">
    <div class="mt-4 mb-5 text-navy editable">
      <h2 class="pb-3 h2-responsive">A Shared Commitment to the Member</h2>
    </div><!-- Partnership cards -->
    <div class="row text-white">
      <div class="col-md-6 d-flex mb-5-sm-down">
        <div class="card border-0 w-100 bg-secondary">
          <img class="svg-top" height="90" src="/assets/images/icons/section-icon.svg" width="90">
          <div class="card-body">
            <h3 class="my-4 text-white text-center">Section Partnership</h3>
            <ul class="text-white li-triangle-bullets-white">
              <li class="mb-3">Interaction with other members and pros</li>
              <li class="mb-3">Local seminars and tours</li>
              <li class="mb-3">Career services at section level</li>
              <li class="mb-3">Local coaching and player growth</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6 d-flex">
        <div class="card border-0 w-100 bg-primary">
          <img class="svg-top" height="90" src="/assets/images/icons/national-icon.svg" width="90">
          <div class="card-body">
            <h3 class="my-4 text-white text-center">National Partnership</h3>
            <ul class="text-white li-triangle-bullets-white">
              <li class="mb-3">Championships and events</li>
              <li class="mb-3">Career services</li>
              <li class="mb-3">Coaching and player development</li>
              <li class="mb-3">Membership services</li>
              <li class="mb-3">Diversity inclusion</li>
            </ul>
          </div>
        </div>
      </div>
    </div><!-- Strategic vision and operating criteria -->
    <div class="my-4 text-navy editable">
      <h2 class="h2-responsive">Strategic Vision and Operating Criteria</h2>
    </div>
    <h4 class="text-navy">Strategic Vision</h4>
    <p>We will be defined by:</p>
    <div class="row">
      <div class="col-lg-4 col-md-4 d-flex">
        <div class="card shadow-none w-100">
          <div class="card-body">
            <img alt="Our constant pursuit of excellence and innovation" class="w-100" height="80" src="/assets/images/icons/hand-light.svg" width="80">
            <hr>
            <h6 class="mt-3 mb-1 fw-600 mx-lg-auto w-lg-75">Our constant pursuit of excellence and innovation</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 d-flex">
        <div class="card shadow-none w-100">
          <div class="card-body">
            <img alt="Our culture of teamwork and collaboration" class="w-100" height="80" src="/assets/images/icons/teamwork-icon.svg" width="80">
            <hr>
            <h6 class="mt-3 mb-1 fw-600 mx-lg-auto w-lg-75">Our culture of teamwork and collaboration</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 d-flex">
        <div class="card shadow-none w-100">
          <div class="card-body">
            <img alt="Our commitment to diversity and inclusion" class="w-100" height="80" src="/assets/images/icons/group-of-people.svg" width="80">
            <hr>
            <h6 class="mt-3 mb-1 fw-600 mx-lg-auto w-lg-75">Our commitment to diversity and inclusion</h6>
          </div>
        </div>
      </div>
    </div>
    <h4 class="text-navy">Operating Criteria</h4>
    <ul class="text-navy li-triangle-bullets-primary">
      <li>Protect and enhance the PGA brand.</li>
      <li>Engage and support our Members.</li>
      <li>Develop and retain golfers, promote the game and make it more fun.</li>
      <li>Position the PGA of America and our Members as leaders in golf coaching, business operations and general management.</li>
      <li>Develop national and global growth and influence.</li>
      <li>Strengthen the perception of the PGA of America and our Members as the tangible connection between the game and all that play.</li>
    </ul><!-- Measuring our performance -->
    <div class="mt-5 pt-3 mb-4 text-navy editable">
      <h2 class="h2-responsive">Measuring Our Performance</h2>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h4 class="text-navy">Operating Criteria</h4>
      </div>
      <div class="col-md-6">
        <ul class="text-navy li-triangle-bullets-primary">
          <li>Member Satisfaction (Net Promoter Score)</li>
          <li>Unemployment and under-employment rates</li>
          <li>PGA Professional Compensation Trends</li>
          <li>Facility Penetration Rates</li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h4 class="text-navy">Growing the Game and Making a Difference in Opportunities Available in the Game</h4>
      </div>
      <div class="col-md-6">
        <ul class="text-navy li-triangle-bullets-primary">
          <li>Rounds Played</li>
          <li>Participation Trends – beginner, occasional, core, youth, non-traditional</li>
          <li>Viewership/Attendance</li>
          <li>Golfer Spends</li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h4 class="text-navy">Diversity &amp; Accessibility in the Game</h4>
      </div>
      <div class="col-md-6">
        <ul class="text-navy li-triangle-bullets-primary">
          <li>Golfers – female, minority, other</li>
          <li>Professionals – female, minority, other</li>
          <li>Leadership/Staff – female, minority, other</li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h4 class="text-navy">Public Perception of PGA and Members</h4>
      </div>
      <div class="col-md-6">
        <ul class="text-navy li-triangle-bullets-primary">
          <li>Golfer – Role related to instruction, business and championships</li>
          <li>Employer – Value of PGA Professional to health of business</li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h4 class="text-navy">Breadth &amp; Scope of PGA and Member Influence</h4>
      </div>
      <div class="col-md-6">
        <ul class="text-navy li-triangle-bullets-primary">
          <li>Percentage of golfers seek a PGA Professional for instruction</li>
          <li>Value consumer places on PGA Professional related to facility</li>
          <li>Media value associated with PSAs, PR and Championships</li>
        </ul>
      </div>
    </div>
  </div>
</section>