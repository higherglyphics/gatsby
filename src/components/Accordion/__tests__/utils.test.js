import { labelFormatter, slugify } from '../utils'

describe('utils', () => {
  it('labelFormatter', () => {
    expect(labelFormatter()).toBeDefined()
    expect(labelFormatter('pga-coach')).toEqual('PGA.Coach')
  })
  it('slugify', () => {
    expect(slugify('bat & man')).toEqual('bat-and-man')
  })
})
