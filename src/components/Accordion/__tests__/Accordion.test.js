import React from 'react'
import { shallow } from 'enzyme'
import Accordion from '../index'
import { HeaderLink, ClickableIcon } from '../styled'

const CATEGORY = 'career-advancement'

const makeNode = (name, order) => ({
  fields: {
    article: name,
    category: CATEGORY,
    subCategory: 'build-your-team',
    slug: `/${CATEGORY}/build-your-team/${name}`
  },
  fileAbsolutePath: `/resources/_pages/${CATEGORY}/build-your-team/${name}.md`,
  frontmatter: {
    title: name,
    sidebar_label: name,
    order_number: order,
    layout: 'default'
  }
})

const getProps = (category, slugChild) => ({
  slug: `/parent/${slugChild}/sub-child`,
  parent: 'some parent',
  items: {
    [category]: [
      { node: makeNode('Article First', 1) },
      { node: makeNode('Article Second', 2) }
    ]
  }
})

const props = getProps('someCategory', 'child')

describe('components/Accordion', () => {
  it('should render correct header html and link', () => {
    const wrapper = shallow(<Accordion {...props} />)
    const headerLink = wrapper.find(HeaderLink)
    expect(headerLink.html()).toContain(props.parent)
    expect(headerLink.props().to).toEqual(`/${props.parent}`)
  })
  it('should render default collapsed clickable icon and handle onClick', () => {
    const wrapper = shallow(<Accordion {...props} />)
    const clickableIcon = wrapper.find(ClickableIcon)

    expect(clickableIcon).toHaveLength(1)
    expect(clickableIcon.hasClass('fa fa-angle-down')).toEqual(true)
    clickableIcon.props().onClick()
    expect(wrapper.find(ClickableIcon).hasClass('fa fa-angle-up')).toEqual(true)
  })
  it('should render opened clickable icon when active', () => {
    const props = getProps(CATEGORY, CATEGORY)
    const wrapper = shallow(<Accordion {...props} />)
    const clickableIcon = wrapper.find(ClickableIcon)
    expect(clickableIcon).toHaveLength(1)
    expect(clickableIcon.hasClass('fa fa-angle-up')).toEqual(true)
    clickableIcon.props().onClick()
    expect(wrapper.find(ClickableIcon).hasClass('fa fa-angle-down')).toEqual(true)
  })
})
