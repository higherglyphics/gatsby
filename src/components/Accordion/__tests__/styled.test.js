import React from 'react'
import 'jest-styled-components'
import renderer from 'react-test-renderer'
import { Theme } from '@pga/pga-component-library'
import { ParentLink, ChildLink, LinkWrapper } from '../styled'

describe('components/Accordion', () => {
  it('should render correct styles', () => {
    const props = {
      active: true
    }
    const parentLink = renderer.create(<ParentLink {...props} />).toJSON()
    expect(parentLink).toHaveStyleRule('font-weight', '700')
    expect(parentLink).toHaveStyleRule('color', Theme.colors.primary)
    const childLink = renderer.create(<ChildLink {...props} />).toJSON()
    expect(childLink).toHaveStyleRule('font-weight', '700')
    const linkWrapper = renderer.create(<LinkWrapper {...props} />).toJSON()
    expect(linkWrapper).toHaveStyleRule('background-color', Theme.colors.lightgrey)
    expect(linkWrapper).toHaveStyleRule('border-left', `5px ${Theme.colors.primary} solid`)
  })
})
