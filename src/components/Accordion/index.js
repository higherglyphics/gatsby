import React, { useState, useRef } from 'react'
import { Collapse } from 'reactstrap'
import propTypes from 'prop-types'
import { Theme } from '@pga/pga-component-library'
import { parseSlug, replaceDash } from '../../utils'
import { ParentLink, ChildLink, LinkWrapper, HeaderLink, Container, ClickableIcon } from './styled'
import { sortItemsByField, labelFormatter, sortCategories, calcShowChildLinks } from './utils'

const Accordion = ({ items = {}, parent = '', slug }) => {
  const [activeCategory, activeSubCategory, activeArticle] = parseSlug(slug)
  const sortedItems = sortCategories(items, activeCategory)

  return (
    <Container>
      <HeaderLink to={`/${parent}`}>{replaceDash(parent)}</HeaderLink>
      {
        Object.keys(sortedItems)
          .map((subCategory, i) => {
            const childLinks = sortedItems[subCategory]
            const showChildLinks = calcShowChildLinks(childLinks)
            const active = subCategory === activeSubCategory
            const initial = useRef(true)
            const [isCollapsed, setCollapse] = useState(false)

            if (active && initial.current) {
              setCollapse(true)
              initial.current = false
            }

            return (
              <div style={{ borderTop: `1px solid ${Theme.colors.darkgrey}` }} key={i}>
                <LinkWrapper {...{ active }}>
                  <ParentLink to={`/${parent}/${subCategory}`} {...{ active }}>{labelFormatter(subCategory)}</ParentLink>
                  {
                    showChildLinks ? (
                      isCollapsed
                        ? <ClickableIcon onClick={() => setCollapse(!isCollapsed)} className='fa fa-angle-up' />
                        : <ClickableIcon onClick={() => setCollapse(!isCollapsed)} className='fa fa-angle-down' />
                    ) : null
                  }
                </LinkWrapper>

                {
                  showChildLinks
                    ? (
                      <Collapse isOpen={isCollapsed}>
                        {
                          sortItemsByField('order_number')(childLinks)
                            .map((child, idx) => {
                              const { fields, frontmatter } = child.node
                              const active = activeArticle === fields.article
                              return (
                                <ChildLink to={fields.slug} active={active} key={idx}>{frontmatter.sidebar_label || frontmatter.title}</ChildLink>
                              )
                            })
                        }
                      </Collapse>
                    ) : null
                }
              </div>
            )
          })
      }
    </Container>
  )
}

Accordion.propTypes = {
  items: propTypes.object.isRequired,
  slug: propTypes.string.isRequired,
  parent: propTypes.string.isRequired
}

export default Accordion
