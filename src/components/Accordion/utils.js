import { sortWith, ascend, omit, ifElse, pipe, T, length, lt, path } from 'ramda'
import { parseSlug } from '../../utils'
import labelRenames from '../../../_data/labelRenames'
import uppercaseKeywords from '../../../_data/uppercase-keywords'
import categoryOrder from '../../../_data/category-order'

export const slugify = (text = '') => {
  return text.toString().toLowerCase().trim().replace(/&/g, '-and-').replace(/[\s\W-]+/g, '-')
}

export const labelFormatter = (text = '') => {
  if (labelRenames[text]) return labelRenames[text]
  const result = text
    .split('-')
    .map(word => uppercaseKeywords.includes(word) ? word.toUpperCase() : word)
    .join('-')
    .replace(/-/g, ' ')
  return result
}

export const sortItemsByField = (field = 'order_number') => sortWith([
  ascend(({ node }) => node.frontmatter[field])
])

export const calcShowChildLinks = ifElse(
  arr => arr.length > 1,
  T,
  pipe(
    path(['0', 'node', 'fields', 'slug']),
    parseSlug,
    length,
    lt(2)
  )
)

const orderKeys = (keys, obj) => {
  return keys.reduce((acc, key) => {
    acc[key] = obj[key]
    return acc
  }, {})
}

/**
 * Sorts object based on prop
 const data = { 'first-plan': [1], 'third-plan': [3], 'second-plan': [2] }
 const correctOrder = [ 'first-plan', 'second-plan', 'third-plan' ]
 sortCategories (obj, activeCategory) -> { 'first-plan': [1], 'second-plan': [2], 'third-plan': [3] }
 */

export const sortCategories = (obj, activeCategory) => {
  const correctOrder = categoryOrder[activeCategory]
  if (!correctOrder) return obj
  const validOrder = correctOrder.filter(c => obj[c])
  const orderedData = orderKeys(validOrder, obj)
  const unOrderedData = omit(correctOrder, obj)
  return {
    ...orderedData,
    ...unOrderedData
  }
}
