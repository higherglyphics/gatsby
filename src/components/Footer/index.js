import React from 'react'
import styled from 'styled-components'
import { Footer, footerOptions, optionsFor } from '@pga/pga-component-library'

const FooterContainer = styled.div`
  text-align: left;
`

export default ({ isLoggedIn, me }) =>
  <FooterContainer>
    <Footer
      sections={optionsFor()}
      copyright={footerOptions.copyrightMessage}
    />
  </FooterContainer>
