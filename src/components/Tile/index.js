import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import { Theme, LinkIcon } from '@pga/pga-component-library'

const TileSquare = styled.div`
  margin-bottom: 2rem;
  box-shadow: 0 5px 10px 0 ${Theme.colors.primaryShadow};
  position: relative;
  transition: all .3s;
`

const TileImage = styled.div`
  padding-bottom: 100%;
  margin-bottom: 1rem;
  overflow: hidden;
  position: relative;
  &:after {
    background-image: linear-gradient(0deg, #00234b, #00234b 12%,rgba(0,35,75,.7) 20%,rgba(0,35,75,0) 45%,rgba(0,35,75,0));
    opacity: .9;
    transition: background-image .2s ease-in-out;
    bottom: 0;
    content: "";
    left: 0;
    opacity: .9;
    position: absolute;
    right: 0;
    top: 0;
  }
  
  img {
    animation-delay: .75s;
    animation-name: fade;
    height: 100%;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%,-50%);
  }
`

const TileBody = styled.div`
  display: flex;
  position: absolute;
  bottom: 20px;
  left: 10px;
  right: 10px;
  @media(min-width: 1200px) {
    left: 20px;
    right: 20px;
  }
  h3 {
    color: #fff;
    font-weight: 700;
    line-height: 1.25;
    margin-bottom: 0;
  }
`

const TileLink = styled(Link)`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  top: 0;
  z-index: 10;
`
const LinkIconStyled = styled(LinkIcon)`
  color: white;
  font-size: 1.5rem;
  padding: 0 .25rem;
`

export default ({ image, title, link }) => {
  return (
    <TileSquare>
      <TileImage>
        <img src={image} height={233} alt='' />
      </TileImage>
      <TileBody>
        <h3>{title}&nbsp;<LinkIconStyled /></h3>
      </TileBody>
      <TileLink to={link} />
    </TileSquare>
  )
}
