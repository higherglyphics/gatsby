import React from 'react'
import { shallow } from 'enzyme'
import SubNavigation from '../index'

const props = {
  location: {
    pathname: '/path'
  }
}

describe('components/SubNavigation', () => {
  it('should render without crashing', () => {
    shallow(<SubNavigation {...props} />)
  })
})
