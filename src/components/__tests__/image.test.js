import React from 'react'
import { shallow } from 'enzyme'
import Image, { ImgWrapper } from '../image'
import childImageSharp from '../../../__mocks__/childImageSharp'

describe('components/image', () => {
  it('renders correctly', () => {
    const data = {
      placeholderImage: {
        childImageSharp
      }
    }
    shallow(<Image data={data} />)
    shallow(<ImgWrapper data={data} />)
  })
})
