import React from 'react'
import { shallow } from 'enzyme'
import ImageWithText from '../ImageWithText'

describe('components/ImageWithText', () => {
  it('renders correctly', () => {
    const props = {
      frontmatter: {
        title: 'some title',
        header_image: '/some-image.jpg'
      }
    }
    shallow(<ImageWithText {...props} />)
  })
})
