import React from 'react'
import { shallow } from 'enzyme'
import HeroImage, { SearchButton } from '../HeroImage'
import childImageSharp from '../../../__mocks__/childImageSharp'

const props = {
  childImageSharp
}

window.___navigate = jest.fn()

describe('components/HeroImage', () => {
  beforeEach(() => {
    window.___navigate.mockReset()
  })
  it('should render without crashing', () => {
    shallow(<HeroImage {...props} />)
  })
  it('should handle submit', () => {
    const wrapper = shallow(<HeroImage {...props} />)
    wrapper.find(SearchButton).props().onClick()
    expect(window.___navigate).toHaveBeenCalledTimes(1)
  })
  it('should handle on enter keydown', () => {
    const wrapper = shallow(<HeroImage {...props} />)
    wrapper.find('input').props().onKeyDown({ keyCode: 14 })
    expect(window.___navigate).toHaveBeenCalledTimes(0)
    wrapper.find('input').props().onKeyDown({ keyCode: 13 })
    expect(window.___navigate).toHaveBeenCalledTimes(1)
  })
})
