import React from 'react'
import 'jest-styled-components'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import Layout, { BodyWrapper } from '../layout'

describe('components/layout', () => {
  it('should render without crashing', () => {
    const props = {
      frontmatter: {
        body_color: 'black'
      }
    }
    shallow(<Layout {...props}>child</Layout>)
  })
  it('should have correct styles', () => {
    const tree = renderer.create(<BodyWrapper bgColor='white' />).toJSON()
    expect(tree).toHaveStyleRule('background-color', 'white')
  })
})
