import React from 'react'
import { shallow } from 'enzyme'
import Tile from '../Tile'

describe('components/Tile', () => {
  it('should render without crashing', () => {
    const props = { link: '/some-link', name: 'some title', image: '/image.jpg' }
    shallow(<Tile {...props} />)
  })
})
