import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Header from './Header'
import SubNavigation from './SubNavigation'
import Footer from './Footer'
import GlobalStyles from '../styles/global'

export const BodyWrapper = styled.div`
  background-color: ${props => props.bgColor || 'white'};
`

const Layout = ({ children, frontmatter = {}, bgColor, ...rest }) => (
  <BodyWrapper bgColor={bgColor || frontmatter.body_color}>
    <Header />
    <SubNavigation {...rest} />
    <div>
      <main>{children}</main>
      <Footer />
    </div>
    <GlobalStyles />
  </BodyWrapper>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
