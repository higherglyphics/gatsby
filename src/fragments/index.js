import { graphql } from 'gatsby'

export const markdownFrontmatterFragment = graphql`
  fragment MarkdownFrontmatter on MarkdownRemark {
    fileAbsolutePath
    fields {
      slug
      category
      subCategory
      article
    }
    frontmatter {
      title
      layout
      sidebar_label
      order_number
    }
  }
`
