export const replaceDash = (string = '') => string.replace(/-/g, ' ')

export const parseSlug = (slug = '') => {
  return slug
    .split('/')
    .filter(s => s)
}
