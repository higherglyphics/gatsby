import { replaceDash, parseSlug } from '../index'

describe('utils', () => {
  it('replaceDash', () => {
    expect(replaceDash()).toEqual('')
    expect(replaceDash('add-space')).toEqual('add space')
    expect(replaceDash('multi---space')).toEqual('multi   space')
    expect(replaceDash('nospace')).toEqual('nospace')
  })
  it('parseSlug', () => {
    expect(parseSlug()).toEqual([])
    expect(parseSlug('/parent/child')).toEqual(['parent', 'child'])
    expect(parseSlug('/parent/child/sub-child')).toEqual(['parent', 'child', 'sub-child'])
    expect(parseSlug('parent')).toEqual(['parent'])
    expect(parseSlug('/multi//slash///slug')).toEqual(['multi', 'slash', 'slug'])
  })
})
