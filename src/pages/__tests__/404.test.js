import React from 'react'
import { shallow } from 'enzyme'
import NotFoundPage from '../404'

describe('pages/404', () => {
  it('should render without crashing', () => {
    shallow(<NotFoundPage />)
  })
})
