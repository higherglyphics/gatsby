import React from 'react'

export default class extends React.Component {
  componentDidMount () {
    window.location.assign(`${process.env.REACT_APP_AUTH_FLOW_BASE_URI}/login?return_to=${window.location}`)
  }

  render () {
    return null
  }
}
