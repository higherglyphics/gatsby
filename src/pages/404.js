import React from 'react'
import { NotFound } from '@pga/pga-component-library'

import Layout from '../components/layout'
import SEO from '../components/seo'

export default () => {
  console.log(process.env.NODE_ENV)
  return (
    <Layout>
      <SEO title='404: Not found' />
      <div>its 404!!!</div>
      <NotFound />
    </Layout>
  )
}
