import React from 'react'
import { shallow } from 'enzyme'
import TileFactory, { Tile } from '../Tile'

const props = {
  data: {
    title: 'some title',
    link: '/some-link',
    child: [
      {
        title: 'some title',
        link: '/some-link',
        image: 'some-image.jpg'
      }
    ]
  }
}

describe('factory/Tile', () => {
  it('should render without crashing', () => {
    shallow(<TileFactory {...props} />)
    shallow(<Tile data={props.data.child[0]} />)
  })
})
