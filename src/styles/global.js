import { createGlobalStyle } from 'styled-components'
import { Theme } from '@pga/pga-component-library'

const GlobalStyle = createGlobalStyle`
  h1, h2, h3, h4, h5 {
    font-family: ${Theme.fonts.PlayfairDisplay};
    color: ${Theme.colors.primary};
  }
  p {
    color: ${Theme.colors.primary};
  }
  /* p, a {
    font-family: ${Theme.fonts.Montserrat};
    font-size: 14px;
    line-height: 23px;
  }
  li {
    font-family: ${Theme.fonts.Montserrat};
    font-size: 15px;
    line-height: 23px;
  } */
  /* img {
    height: auto;
    max-width: 100%;
  } */
`

export default GlobalStyle
