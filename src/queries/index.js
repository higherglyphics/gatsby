import { gql } from 'apollo-boost'

export const SUBSCRIPTION_QUERY = gql`
  {
    me {
      id
      education {
        subscriptions {
          productId
          startDate
          endDate
          description
        }
      }
    }
  }
`

export const REGISTER_MUTATION = gql`
  mutation Register($id: ID!, $productCode: EDLevelCode!) {
    register(id: $id, productCode: $productCode) {
      id
    }
  }
`
