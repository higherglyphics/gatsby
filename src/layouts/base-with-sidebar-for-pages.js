import React from 'react'
import { groupBy, isEmpty, pathOr, omit } from 'ramda'
import Layout from '../components/layout'
import Accordion from '../components/Accordion'
import { Heading } from '../components/Typography'
import ImageWithText from '../components/ImageWithText'
import { parseSlug } from '../utils'

const bySubCategory = groupBy(edge => edge.node.fields.subCategory)
const isIndex = node => node.fileAbsolutePath.includes('index.md')

export default ({ pageContext, location, data, children }) => {
  const [category] = parseSlug(location.pathname)

  if (!data.allMarkdownRemark || isEmpty(data.allMarkdownRemark)) return <p>No data available.</p>

  const edges = data.categoryMarkdowns.edges.filter(edge => !isIndex(edge.node))
  const subCategoryList = omit(['null'])(bySubCategory(edges))
  const frontmatter = pathOr({}, ['allMarkdownRemark', 'edges', '0', 'node', 'frontmatter'])(data)

  return (
    <Layout {...{ location }}>
      <div className='container'>
        <div className='py-3'>
          <div className='row'>
            <div className='col-lg-4'>
              <Accordion parent={category} items={subCategoryList} slug={location.pathname} />
            </div>
            <div className='col-lg-8'>
              <div className='layout-sub-category'>
                {frontmatter.header_image ? <ImageWithText {...{ frontmatter }} /> : <Heading>{frontmatter.title}</Heading>}
                {children}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
