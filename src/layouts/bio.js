import React from 'react'
import styled from 'styled-components'
import Layout from '../components/layout'

const Card = styled.div`
  background-color: #fff;
  box-shadow: 0 5px 10px 0 rgba(0, 35, 75, 0.15);
  padding: 50px;
  margin-top: 20px;
  margin-bottom: 50px;
`

export default ({ pageContext, location, data }) => {
  const { frontmatter, html } = pageContext

  return (
    <Layout {...{ location, bgColor: '#e2e2e2' }}>
      <div className='container mt-4'>
        <a className='text-info' href='/leadership'> <i className='icon icon-arrow-left mr-1' />Back to list</a>
        <Card>
          <div className='mb-4'>
            <h1 className='text-secondary'>{frontmatter.title}</h1>
            <h5>{frontmatter.position}</h5>
          </div>
          <img className='float-left mr-4 mb-4' src={frontmatter.image_path} alt={frontmatter.title} height={225} />
          <div dangerouslySetInnerHTML={{ __html: html || '' }} />
        </Card>
      </div>
    </Layout>
  )
}
