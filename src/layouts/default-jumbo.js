import React from 'react'
import Layout from '../components/layout'

export default ({ pageContext, location }) => {
  const { html } = pageContext

  return (
    <Layout {...{ location, subNavFixed: true, bgColor: '#e2e2e2' }}>
      <div dangerouslySetInnerHTML={{ __html: html || '' }} />
    </Layout>
  )
}
