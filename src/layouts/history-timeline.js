import React from 'react'
import { graphql } from 'gatsby'

// import ScrollMagic from 'scrollmagic'
// import { TweenMax, TimelineMax } from 'gsap'
// import { ScrollMagicPluginGsap } from 'scrollmagic-plugin-gsap'

import Layout from '../components/layout'
import SEO from '../components/seo'

import '../styles/history-timeline.css'

// const windowExists = () => typeof ScrollMagicPluginGsap === 'function'

// if (windowExists()) {
//   ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax)
// }

// const sceneMaker = function (itemId) {
//   if (windowExists()) {
//     const controller = new ScrollMagic.Controller()
//     const triggerElement = `#item${itemId}`
//     const tween = TweenMax.to(triggerElement, 1, { className: `+=active` })
//     const scene = new ScrollMagic
//       .Scene({ triggerElement, duration: 300, offset: -200 })
//       .setTween(tween)
//       .addTo(controller)

//     return scene
//   }
// }

const parseNode = ({ content }) => {
  content.forEach(item => {
    // sceneMaker(item.title)
  })
}

export default ({ pageContext, location, data }) => {
  const { html, frontmatter } = pageContext

  setTimeout(() => {
    data.allHistoryTimelineYaml.edges.map(({ node }) => parseNode(node))
  }, 200)

  return (
    <Layout {...{ location, frontmatter }}>
      <SEO title={'history'} />

      <div className='layout-default'>
        <div className='col-lg-12 p-0'>
          <div id='md-content'className='content'>
            <div dangerouslySetInnerHTML={{ __html: html || '' }} />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`
{
  allHistoryTimelineYaml {
    edges {
      node {
        id
        content {
          image
          text
          title
        }
        period
      }
    }
  }
}
`
