import React from 'react'
import { shallow } from 'enzyme'
import Base from '../base'
import Default from '../default'
import DefaultNoSidebar from '../default-no-sidebar'
import DefaultNoTitle from '../default-no-title'
import BaseWithSidebar from '../base-with-sidebar'
import BaseWithSidebarForPages from '../base-with-sidebar-for-pages'
import Category, { TileItem } from '../category'
import Faq from '../faq'
import Home from '../home'
import Layout from '../../components/layout'
import Accordion from '../../components/Accordion'
import { Heading } from '../../components/Typography'
import childImageSharpMock from '../../../__mocks__/childImageSharp'
import markdownRemarkMock from '../../../__mocks__/markdownRemark'

const props = {
  pageContext: {
    html: '<div>testing</div>',
    frontmatter: {
      title: 'some title',
      tiles: [
        { link: '/some-link', title: 'some title', image: '/image.jpg' }
      ]
    }
  },
  data: {},
  location: {
    pathname: '/some/slug'
  }
}

describe('layouts', () => {
  it('should render all basic layouts', () => {
    shallow(<Base {...props} />)
    shallow(<Default />)
    shallow(<DefaultNoSidebar {...props} />)
    shallow(<DefaultNoTitle {...props} />)
    shallow(<Faq />)
  })
  it('should render home layout', () => {
    const allProps = {
      ...props,
      data: {
        file: {
          childImageSharpMock
        }
      }
    }
    shallow(<Home {...allProps} />)
  })
  it('should render category layout', () => {
    shallow(<Category {...props} />)
    shallow(<TileItem {...props} />)
  })
  it('should render base with sidebar layout', () => {
    expect.assertions(4)
    const wrapperNoData = shallow(<BaseWithSidebar {...props} />)
    expect(wrapperNoData.html()).toContain('No data available.')
    const withMarkdownData = {
      ...props,
      data: {
        allMarkdownRemark: {
          edges: [
            { node: markdownRemarkMock }
          ]
        }
      }
    }
    const wrapper = shallow(<BaseWithSidebar {...withMarkdownData} />)
    expect(wrapper.find(Layout)).toHaveLength(1)
    expect(wrapper.find(Accordion)).toHaveLength(1)
    expect(wrapper.find(Heading)).toHaveLength(1)
  })
  it('should render base with sidebar for pages layout', () => {
    const wrapperNoData = shallow(<BaseWithSidebarForPages {...props} />)
    expect(wrapperNoData.html()).toContain('No data available.')
    const withMarkdownData = {
      ...props,
      data: {
        allMarkdownRemark: {
          edges: [
            { node: markdownRemarkMock }
          ]
        },
        categoryMarkdowns: {
          edges: [
            { node: markdownRemarkMock }
          ]
        }
      }
    }
    const wrapper = shallow(<BaseWithSidebarForPages {...withMarkdownData} />)
    expect(wrapper.find(Layout)).toHaveLength(1)
    expect(wrapper.find(Accordion)).toHaveLength(1)
    expect(wrapper.find(Heading)).toHaveLength(1)
  })
})
