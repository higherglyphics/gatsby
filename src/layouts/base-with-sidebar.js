import React from 'react'
import { groupBy, isEmpty, omit } from 'ramda'
import Layout from '../components/layout'
import Accordion from '../components/Accordion'
import { Heading } from '../components/Typography'
import ImageWithText from '../components/ImageWithText'

const bySubCategory = groupBy(edge => edge.node.fields.subCategory)
const isIndex = node => node.fileAbsolutePath.includes('index.md')

export default ({ pageContext, location, data }) => {
  const { category, html, frontmatter, slug } = pageContext
  if (!data.allMarkdownRemark || isEmpty(data.allMarkdownRemark)) return <p>No data available.</p>
  const { allMarkdownRemark } = data

  const edges = allMarkdownRemark.edges.filter(edge => !isIndex(edge.node))
  const subCategoryList = omit(['null'])(bySubCategory(edges))

  return (
    <Layout {...{ location }}>
      <div className='container'>
        <div className='py-3'>
          <div className='row'>
            <div className='col-lg-4'>
              <Accordion parent={category} items={subCategoryList} slug={slug} />
            </div>
            <div className='col-lg-8'>
              <div className='layout-sub-category'>
                {frontmatter.header_image ? <ImageWithText {...{ frontmatter }} /> : <Heading>{frontmatter.title}</Heading>}
                <div dangerouslySetInnerHTML={{ __html: html || '' }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
