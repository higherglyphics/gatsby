---
title: Seth Waugh
position: Chief Executive Officer
image_path: /uploads/executives/Seth Waugh_cropped.jpg
navbar-secondary: leadership
layout: bio
---

Seth Waugh, former CEO of Deutsche Bank Americas, joined the PGA of America as Chief Executive Officer in September 2018. Waugh guides the business and overall strategy of one of the world’s largest sports organizations, serving the Association’s nearly 29,000 PGA Professionals.

Since becoming CEO, the PGA announced a transformational $500 million partnership with Omni Stillwater Woods, the City of Frisco, its Economic and Community Development Corporations and the Frisco Independent School District to relocate PGA Headquarters to Frisco, Texas—site of 26 future PGA of America Championships. Cadillac was also named as the Official Vehicle of the PGA of America,

PGA Championship, KitchenAid Senior PGA Championship and KPMG Women’s PGA Championship. Additionally, the PGA rolled out a partnership with Mission Hills Group and PacificPine Sports Group to launch three PGA Golf Academies in China. Waugh served a prior three-year term as an Independent Director on the PGA of America Board of Directors.

Waugh also previously served as a Senior Advisor and then a Managing Director at Silver Lake, a global leader in technology investing. He continues on as a Senior Advisor to the firm. In 2016, he was appointed Non-Executive Chairman‎ of Alex. Brown, following the sale of Deutsche Bank’s Private Client Services division to Raymond James. Waugh also served in the management of Florida East Coast Industries in 2014.

He spent 13 years at Deutsche Bank, including 10 years as CEO of the Americas, overseeing an unprecedented time of growth for the bank in the region. Waugh also served as Chairman of the Deutsche Bank Americas Advisory Board, an external strategic advisory team conceived under his leadership. During this time, he worked with the PGA TOUR to create the former Deutsche Bank Championship (now the Dell Technologies Championship) in Boston. Prior to joining Deutsche Bank, Waugh was CEO of Quantitative Financial Strategies (QFS). He also spent 11 years in various leadership roles at Merrill Lynch, culminating in serving as Co-Head of Global Debt Markets. Earlier in his career, Waugh managed the Corporate Bond and International Trading desks at Salomon Brothers.

Waugh sits on the Board of Franklin Templeton Advisors and the Advisory Board of Workday, Inc. He also served on the FINRA Board of Governors until 2015. Waugh’s philanthropic endeavors have included serving on the boards of the World Trade Center Memorial Foundation; YMCA of Greater New York; Multiple Sclerosis Society of Greater New York; Executive Committee of Partnership for New York City; St. Vincent’s Services of Brooklyn; Local Initiatives Support Corporation; and Harlem Village Academies, as well as serving as President of the Board of the Lawrenceville School, and Trustee at Wake Forest University. For several years, he championed the Women on Wall Street Conference, which seeks to advance opportunities for women in the financial services sector.

Waugh holds a B.A. in Economics and English from Amherst College. Seth and his wife, Jane, reside in North Palm Beach, Florida. Together, they have five children, one of whom, Clancy, played golf at Wake Forest and SMU, and is now trying his hand in the professional game.