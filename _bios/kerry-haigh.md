---
title: Kerry Haigh
position: Chief Championships Officer
image_path: /uploads/executives/Kerry_Haigh.jpg
navbar-secondary: leadership
layout: bio
---
Kerry Haigh, the Chief Championships Officer of the PGA of America, is responsible for the overall operation, administration and golf course setup for the PGA Championship, Ryder Cup, Senior PGA Championship presented by KitchenAid, KPMG Women’s PGA Championship and PGA Grand Slam of Golf.

Born in Doncaster, England, Haigh played golf to a scratch handicap at age 17, before concentrating on golf administration as a career.

Following graduation from the University of Leeds, he worked for The Professional Golfers’ Association in Great Britain, organizing events throughout Europe for four years (1981-1984). In 1984, he was hired by the LPGA as a Tournament Official for the LPGA Tour (1984-1988) and worked more than 30 tournaments a year.  In 1988, he was hired by Kemper Sports to be the Tournament Director for the 1989 PGA Championship held at Kemper Lakes outside of Chicago.

Following the 1989 PGA Championship, he was hired by the PGA of America to oversee the entire Championship Department. He was promoted to Managing Director in 2004, and to his current position in 2013. Haigh is one of the foremost experts on the Rules of Golf.

Haigh is married to former LPGA player, Denise Strebig, and has two daughters. The couple lives in Jupiter, Florida.
