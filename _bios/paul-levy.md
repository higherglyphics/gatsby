---
title: Paul K. Levy
position: Honorary President
image_path: /uploads/executives/Paul_Levy.jpg
navbar-secondary: leadership
layout: bio
---

Following a two-year term as the 40th PGA President, Paul K. Levy was named PGA Honorary President at the 2018 PGA Annual Meeting. Previously, he also served two-year terms each as PGA Vice President and PGA Secretary.

Levy is the President and CEO of PKL Golf Management and Club Services specializing in operational management and consulting services to all aspects of the golf and club industry. Previously, he was President of Club Operations and Development for Sunrise Company, where he served for more than 18 years—in which time he developed and managed several properties in California, Texas, Nevada and Colorado. Additionally, he was the CEO and General Manager at Toscana Country Club in Indian Wells, California.

Levy earned PGA Membership in 1986, and has served in a leadership capacity at both the Section and national levels since 1992. A member of the Southern California PGA Section, Levy brought a deep commitment to enhancing the skills and employment opportunities of PGA Professionals while serving as PGA President.

From 2004-2008, Levy was a member of the PGA Board of Control. He has served on several National Committees dating back to the early 1990s. Levy was president of the Southern Texas PGA Section from 1998-2000, and earned the 2000 Southern Texas PGA Golf Professional of the Year Award. He has also been named as a three-time Section PGA Junior Golf Leader recipient (1992, ’93, ’98); the 1999 Section Bill Strausbaugh Award winner; and the 1997 Section Merchandiser of the Year for Public Facilities. Additionally, he chaired every major committee of the Section at one time in his tenure as a Section leader. From 2007-2012, Levy was elected as an Independent Director on the Southern California PGA Board of Directors.

From 1999-2004, Levy served as General Manager and PGA Director of Golf at Royal Oaks Country Club in Houston; and was the Senior Vice President of Club Operations for Sunrise Company (the developer of Royal Oaks Country Club), overseeing properties in Nevada, California, Colorado and Texas. In 2004, Levy moved to Southern California to oversee the development of Toscana.

In 1992, he founded PKL Golf Group Company, a golf management and development company, and served as its President and CEO. He is also a Member of the CMAA.

A native of New Orleans, Levy is a 1983 graduate of Louisiana State University, where he was a three-year member of the Tigers golf team. &nbsp;&nbsp;

Levy and his wife, Heidi, live in Indian Wells. They are the parents of three sons: Nathan, Kristopher and Travis; two daughters: Nakayla and Destiny; and have five grandchildren.