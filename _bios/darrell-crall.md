---
title: Darrell Crall
position: Chief Operating Officer
image_path: /uploads/executives/Darrell_Crall.jpg
navbar-secondary: leadership
layout: bio
---

Darrell Crall is the PGA of America’s Chief Operating Officer. He directs the day-to-day operations at PGA Headquarters and is charged with oversight of the PGA’s organizational growth, strategic plan, performance goals and priorities, and annual budget execution.&nbsp;<br>&nbsp;<br>His core responsibilities include management of the following departments: Coaching and Player Development, Inclusion & Diversity, Facilities, Global Enterprises, Human Resources, Innovation, People and PGA REACH. He also serves as CEO of Nextgengolf, the PGA’s newly acquired subsidiary that is designed to enhance player development programming and grow the game across emerging generations.

Crall was the driving force behind the creation of PGA Jr. League, Drive Chip & Putt and the American Development Model, and he was responsible for executing the transformational business deal that led to the recent announcement of PGA Headquarters moving to Frisco, Texas, in 2022. In addition, he currently serves as a WE ARE GOLF Board Member, a Trustee for the Environmental Institute for Golf, and a Member of the University of North Texas Sport and Entertainment Management Program Advisory Board.

Crall joined the PGA in 2011, as Senior Director of Golf 2.0, to grow the game and business of golf. Previously, he spent 16 years as Executive Director of the Northern Texas PGA Section, where he transformed the organization’s business into a national leader in the charitable, junior golf and tournament operations arenas.&nbsp;

Crall was a scholarship athlete on the Duke University golf team and served as Captain. He graduated with a degree in History.