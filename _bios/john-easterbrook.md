---
title: 'John Easterbrook, PGA'
position: Chief Membership Officer
image_path: /uploads/executives/John_Easterbrook.jpg
navbar-secondary: leadership
layout: bio
---

John Easterbrook Jr., PGA joined the PGA of America in January 2017 as the first Chief Membership Officer and will continue to direct the core PGA Member-focused areas of the Association—including PGA &nbsp; Career Services, Education, Member Services and Section Business Operations. &nbsp; In addition, he oversees PGA Golf Properties, which includes PGA Golf Club in Port St. Lucie, Florida; and Valhalla Golf Club in Louisville, Kentucky.

A more than 30-year member of the PGA, Easterbrook delivers a critically important perspective to the PGA of America’s leadership team, focused on creating enhanced career opportunities for the PGA’s Membership and to develop PGA Professionals worldwide.

For 20 years, he served as Executive Vice President and Chief Operating Officer at Troon, the largest third-party management company in the world. Easterbrook was responsible for the day-to-day operations of a vast global business that included more than 15,000 associates (including more than 450 PGA Professionals) at 280 courses 33 countries and 37 states.&nbsp; In addition, he was instrumental in the growth of the company’s portfolio and oversaw the development of Troon’s operating standards and financial benchmarking. As a member of the senior executive committee, Easterbrook also directed field operations, sales and marketing. Prior to Troon, Easterbrook served in executive roles in the golf divisions at Hyatt Hotels and Marriott International.

The grandson of a PGA Professional and the son of a college football coach, Easterbrook earned a bachelor’s degree in finance from the University of Wyoming, where he played on the golf team.

Easterbrook and his wife, Lori, have three daughters: Alexandra, Kelsey and Morgan.