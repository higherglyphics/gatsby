---
title: 'John Lindert, PGA'
position: Secretary
image_path: /uploads/pga-am-11-09-18-850.jpg
navbar-secondary: leadership
layout: bio
---

John Lindert, PGA, of Grand Ledge, Michigan, was elected to succeed Richerson as PGA Secretary. Lindert, a PGA Member for more than 35 years, has served on the PGA Board of Directors as District 5 Director since 2016. He is the PGA Director of Golf/Chief Operating Officer at The Country Club of Lansing, Michigan.

Lindert was president of the Michigan PGA Section from 2010-11 and has served on four national PGA committees: Governance Task Force chair (2017); Membership (2008-present); Board of Control (2009-13); and Special Awards (2006-07). Lindert was the 2009 Michigan PGA Golf Professional of the Year and recipient of the Section Horton Smith Award (2015); Player Development Award (2013); the Section Merchandiser of the Year-Private Facilities (2010); and recipient of the Section Bill Strausbaugh Award (2008).

He has competed five times in the PGA Professional Championship. In addition, Lindert was the Toledo Chapter Stroke Play Champion and competed on the University of Arizona Golf Team. Among his community endeavors, Lindert has served on The First Tee of Mid-Michigan Board of Directors since 2007.