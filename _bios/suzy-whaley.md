---
title: Suzy Whaley
position: President
image_path: /uploads/whaley-suzy.jpg
navbar-secondary: leadership
layout: bio
---

The first woman ever elected to serve as an Officer of the PGA of America, PGA President and PGA Master Professional Suzy Whaley is the PGA Director of Instruction for the Country Club at Mirasol in Palm Beach Gardens, Florida. She is also the PGA Director of Instruction for Suzy Whaley Golf. After serving two years as PGA Secretary, and two years as Vice President, Whaley was elected as the PGA of America's first female President at the PGA Annual Meeting in November 2018.

Recognized in 2019 as one of GOLF’s “Top 100 Teachers in America,” Whaley's resume includes numerous other teaching awards: Golf Digest Top 50 Instructor, LPGA Top 50 Instructor, two-time Connecticut PGA Teacher of the Year, 10-time Golf Digest State Teacher of the Year and U.S. Kids Golf Master Kids Teacher. She is a five-time PGA Jr. League Championship finals Coach and has instructed more than 300 children to collegiate golf. Previously, she enjoyed a five-year tenure at Jim Flick Golf Schools, before becoming the Head Golf Professional at Blue Fox Run in Avon, Connecticut, in 2002. From 2004-06, she worked as an LPGA golf commentator for ESPN, and began her own instruction and coaching business, Suzy Whaley Golf, after ESPN.

A dual member of the PGA of America and LPGA Teaching & Club Professional division, Whaley was a LPGA Tour member in 1990 and 1993. She famously qualified and participated in the 2003 Greater Hartford Open, becoming the first woman since Babe Zaharias in 1945 to qualify for a PGA Tour event. This occurred after Whaley became the first PGA of America woman professional to win the Connecticut PGA Championship. Whaley also is a three-time Connecticut Women’s Open Champion; National LPGA Teaching & Club Professional winner; Connecticut PGA Section Champion; Connecticut PGA Club Professional Champion; and a two-time LPGA Northeast T&CP Section Champion. She competed in both the 2002 and 2005 PGA Professional Championships. Whaley also recently competed in the USGA Senior Women's Open, as well at the LPGA Senior Women’s Championship.

A 2016 Greater Syracuse Sports Hall of Fame inductee, Whaley also serves on both the ANNIKA Foundation Board and the PGA Tour Policy Board. In addition, she is an Honorary Director for the First Tee of Connecticut. Whaley was honored with the 2017 Betsy Rawls Award from the American Junior Golf Association for her service, dedication and contributions to women’s golf. She was also named a 2015 Sports Business Journal “Game Changer,” cited as a woman leader who has had a major impact on sports business. Whaley also received the 2015 Margo Dydek Award from the WNBA’s Connecticut Sun, for her ability to engage, challenge and inspire, while serving as a role model.

A 1989 graduate of the University of North Carolina with a Bachelor’s Degree in economics, Whaley and her husband, Bill, are both PGA Members. Bill is the National Director of Golf for the PGA Tour Properties Division. They have two daughters, Jennifer and Kelly.