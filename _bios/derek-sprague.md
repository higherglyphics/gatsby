---
title: Derek Sprague
position: Honorary President
image_path: /uploads/executives/Derek_Sprague.jpg
navbar-secondary: leadership
layout: bio
---
PGA Honorary President Derek Sprague is the General Manager at TPC Sawgrass. He was elected Honorary President, after serving as PGA President from 2014-2016.

Through Sprague’s leadership, the PGA of America was instrumental in growing the game through player development and youth programs such as PGA Jr. League and the Drive, Chip & Putt Championship. In addition, he co-chaired the highly successful Ryder Cup Task Force, which created a blueprint for success in the event. Sprague previously served the Association as PGA Vice President and PGA Secretary.

For 27 years, Sprague was the General Manager and Director of Golf at Malone (N.Y.) Golf Club, his hometown course, where he was involved with all aspects of the facility’s operations. In 2016, Sprague was named Managing Director of Liberty National Golf Club in Jersey City, New Jersey.

Since 1998, Sprague has held leadership roles at both the national and Section levels. He was a member of the PGA Board of Directors from 2007-10, as well as a number of key PGA Committees. Sprague began service on the Northeastern New York PGA Board of Directors in 1998, and served as Section President from 2003-2004.

He turned professional in 1989, beginning his career at Malone Golf Club. Sprague was elected to PGA membership in 1993.

He was named the 2005 and 2008 Northeastern New York PGA Golf Professional of the Year. Sprague is also a four-time (2000-03) Section Bill Strausbaugh Award winner; two-time Section Merchandiser of the Year for Public Facilities (1998-99); and the 2006 Section President’s Plaque award recipient.

Born in Malone, Sprague followed the lead of his late father in devoting himself to his community by serving from 2003-2013 as a member of the Board of Education of the Malone Central School District.

Sprague is a 1988 graduate of James Madison University, in Harrisonburg, Va., where he competed on the golf team and earned a Bachelor of Business Administration Degree in Marketing. In 2014, Sprague became the fourth inductee into the School of Hospitality, Sport and Recreation Management Hall of Fame at James Madison University.

Derek and his wife, Jennifer, are the parents of two college-age children: a daughter, Alexandra; and a son, Davis.
