---
title: Jim Richerson
position: Vice President
image_path: /uploads/executives/Jim_Richerson.jpg
navbar-secondary: leadership
layout: bio
---

PGA Vice President Jim Richerson is the Senior Vice President of Operations for Troon&reg;, the leader in golf course management, development, and marketing. Richerson is responsible for helping to oversee and grow Troon’s portfolio of managed facilities throughout its related brands: Troon Golf, Honours Golf, Troon Priv&eacute; (the private club operating division of Troon) and Troon International. Troon is the world’s largest golf management company, with more than 275 golf courses in its portfolio.

He was elected Secretary as a member of the Wisconsin PGA Section in 2016, when he became the first Wisconsin PGA Member to be elected as an Officer of the Association. Richerson was electedPGA Vice President at the 2018 PGA Annual Meeting in Indian Wells, California.

A member of the national PGA Board of Directors from 2013-15, serving District 6, Richerson also served on the Wisconsin PGA Section Board of Directors, where he was Secretary from 2015-16; was Chair of the Wisconsin PGA Education Committee; and was a Board Member of the Wisconsin State Golf Association.

During his term as District 6 Director, Richerson served as chair of the National Properties Committee; as a member of the National Budget and National Investment Committees; and continues as a [PGA.com](http://PGA.com) Advisory Board member. He was recipient of the 2013 and 2016 Wisconsin PGA Golf Professional of the Year Award; the Section’s 2011 Horton Smith Award; and 2012 Bill Strausbaugh Award.&nbsp;

Richerson learned the game from his father, Bill, and was elected to PGA Membership in 1995. Jim was a four-year letterman and captain of the golf team at William Jewell College in Liberty, Missouri.

Richerson spent the first 18 years of his career affiliated with the Marriott and Ritz-Carlton Hotel & Golf Corporation, holding various executive and management positions at golf properties in six states.&nbsp;

Previous to his work at Troon, he served as Kohler Co.’s General Manager & Director of Golf for more than 10 years, overseeing the operations at Whistling Straits & Blackwolf Run in Wisconsin and The Duke’s Course in St. Andrews, Scotland.

Richerson also played an integral role for Kohler Properties in conducting major golf championships: He was an Executive Committee Member of the 2007 U.S. Senior Open; the 2010 PGA Championship; the 2012 U.S. Women’s Open, where he also served as General Chair; and the 2015 PGA Championship.

Richerson and wife, Kristi, are parents of a daughter, Mary-Claire.