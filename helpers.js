const { markdownFilterGlobQuery } = require('./utils/queries')

const getMarkdownToHtmlPlugin = (collections) => {
  const ROOT_DIR = `${__dirname}`

  const fsPlugin = collections.map(({ name }) => ({
    resolve: 'gatsby-source-filesystem',
    options: {
      name,
      path: `${ROOT_DIR}/${name}`
    }
  }))

  const mdToHtmlPlugin = collections.map(({ name }) => ({
    resolve: 'markdown-to-html',
    options: {
      name,
      path: `${ROOT_DIR}/${name}`,
      templatePath: './src/layouts',
      template: 'default',
      query: markdownFilterGlobQuery
    }
  }))

  return [
    ...fsPlugin,
    ...mdToHtmlPlugin
  ]
}

exports.getMarkdownToHtmlPlugin = getMarkdownToHtmlPlugin
