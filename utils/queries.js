exports.markdownFilterGlobQuery = glob => `{
  markdownPages: allMarkdownRemark(
    filter: {
      fileAbsolutePath: {
        glob: "${glob}"
      }
    }
  ) {
    edges {
      node {
        html
        frontmatter {
          title
          layout
          order_number
          sidebar_label
          permalink
          order
          position
          image_path
        }
        fileAbsolutePath
        fields {
          slug
        }
      }
    }
  }
}`
