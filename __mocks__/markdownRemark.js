export default {
  'fileAbsolutePath': '/resources/_pages/member-benefits/golf-retirement-plus/golf-retirement-plus-forms.md',
  'fields': {
    'slug': '/member-benefits/golf-retirement-plus/golf-retirement-plus-forms',
    'category': 'member-benefits',
    'subCategory': 'golf-retirement-plus',
    'article': 'golf-retirement-plus-forms'
  },
  'frontmatter': {
    'title': 'Golf Retirement Plus™ Forms',
    'layout': 'default',
    'sidebar_label': 'Forms',
    'order_number': 30
  }
}
