import React from 'react'
import navigationMock from './navigation'

const gatsby = jest.requireActual('gatsby')

module.exports = {
  ...gatsby,
  graphql: jest.fn(),
  Link: jest.fn().mockImplementation(
    ({
      activeClassName,
      activeStyle,
      getProps,
      innerRef,
      ref,
      replace,
      to,
      ...rest
    }) =>
      React.createElement('a', {
        ...rest,
        href: to
      })
  ),
  StaticQuery: jest.fn(),
  useStaticQuery: jest.fn(() => ({
    site: {
      siteMetadata: {
        title: 'pga title',
        description: 'pga description'
      }
    },
    allNavigationYaml: {
      edges: navigationMock
    },
    allNavigationMobileYaml: {
      edges: navigationMock
    },
    allNavigationSecondaryYaml: {
      edges: navigationMock
    }
  }))
}
