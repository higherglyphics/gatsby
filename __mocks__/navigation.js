export default [
  {
    'node': {
      'id': '1',
      'label': 'My Account',
      'to': 'https://account.pga.org/',
      'root': null,
      'external': true
    }
  },
  {
    'node': {
      'id': '2',
      'label': 'Resources',
      'to': '/',
      'root': true,
      'external': false
    }
  }
]
