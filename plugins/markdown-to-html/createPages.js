const { resolve } = require('path')
const { allMarkdownPages } = require('./queries')
const Utils = require('./utils')

// const debug = require('debug')('plugin: markdown-to-html')

module.exports = async ({ graphql, actions }, userOptions) => {
  const options = {
    ...Utils.defaultOptions,
    ...userOptions
  }
  const { createPage } = actions
  const markdownPath = resolve(process.cwd(), options.path)
  const glob = `${markdownPath}/**`

  const query = userOptions.query ? userOptions.query(glob) : allMarkdownPages(glob)
  const { data } = await graphql(query)

  const { edges } = data.markdownPages
  const items = edges.filter(({ node }) => !node.fileAbsolutePath.includes('_defaults.md'))

  for (let i = items.length; i--;) {
    const { fields, frontmatter, html } = items[i].node
    const { slug } = fields
    const [category, subCategory] = Utils.parseSlug(slug)

    const component = await Utils.findLayout(frontmatter.layout, options)

    // debug('creating page with slug: ', slug)

    createPage({
      path: slug,
      component,
      context: {
        slug,
        frontmatter,
        html,
        category,
        subCategory,
        categoryRegex: `/${category}/`,
        subCategoryRegex: `/${subCategory}/`
      }
    })
  }
}
