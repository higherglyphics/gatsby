const allMarkdownPages = glob => `{
  markdownPages: allMarkdownRemark(
    filter: {
      fileAbsolutePath: {
        glob: "${glob}"
      }
    }
  ) {
    edges {
      node {
        html
        frontmatter {
          title
          layout
          order_number
          sidebar_label
          permalink
          order
        }
        fileAbsolutePath
        fields {
          slug
        }
      }
    }
  }
}`

module.exports = {
  allMarkdownPages
}
