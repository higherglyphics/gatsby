const { resolve, join } = require('path')

const cwd = process.cwd()

const defaultOptions = {
  path: './src/pages',
  templatePath: './src/layouts',
  template: 'default'
}

const findLayout = (override, { template, templatePath }) => {
  if (override) {
    return resolve(cwd, join(templatePath, `${override}.js`))
  }
  return resolve(templatePath, `${template}.js`)
}

/**
 * Input: /parent/child/sub-child
 * Output: [parent, child, sub-child]
 */
const parseSlug = (slug = '') => {
  return slug
    .split('/')
    .filter(s => s)
}

/**
 * Ignore _defaults.md files in _collection/_default.md
 */

const isDefault = (node) => node.fileAbsolutePath.includes('_defaults.md')
const ignoreDefaultsMd = (node) => isDefault(node)

module.exports = {
  defaultOptions,
  findLayout,
  parseSlug,
  ignoreDefaultsMd
}
