const path = require('path')
const { defaultOptions, parseSlug } = require('./utils')

const debug = require('debug')('plugin: markdown-to-html')

const mdExt = /.(md|markdown)$/
const cwd = process.cwd()

const notMarkdown = node => node.internal.type !== `MarkdownRemark`
const isDefault = node => path.parse(node.fileAbsolutePath).base === '_defaults.md'

module.exports = async ({ node, actions }, userOptions) => {
  if (notMarkdown(node) || isDefault(node)) return

  const options = {
    ...defaultOptions,
    ...userOptions
  }
  const markdownPath = path.resolve(cwd, options.path)
  if (node.fileAbsolutePath.indexOf(markdownPath) !== 0) return
  const { createNodeField } = actions
  const { permalink } = node.frontmatter
  const slug = permalink || node.fileAbsolutePath
    .replace(markdownPath, ``)
    .replace(mdExt, ``)
  createNodeField({
    node,
    name: `slug`,
    value: slug
  })

  const [category, subCategory, article] = parseSlug(slug)

  // debug('[category, subCategory, article]', [category, subCategory, article])

  createNodeField({
    node,
    name: `category`,
    value: category
  })
  createNodeField({
    node,
    name: 'subCategory',
    value: subCategory
  })
  createNodeField({
    node,
    name: 'article',
    value: article
  })
}
