# gatsby-plugin-markdown-to-html

Gatsby plugin to convert markdown files into html pages

## Install

```bash
npm install --save gatsby-plugin-markdown-to-html
```

## Usage

In `gatsby-config.js`:

```javascript
plugins: [
  'gatsby-plugin-markdown-to-html',
]
```

Example markdown page in `./src/pages/contact-us.md`:

```markdown
---
title: Contact Us
layout: some-layout
---

```

This plugin will create a page at `/contact-us/` and use the `./src/layouts/some-layout.js` component as the template.

A default layout can be specified in case the markdown file does not contain a layout value:

```javascript
plugins: [
   {
      resolve: 'gatsby-plugin-markdown-to-html',
      options: {
         path: '../__pages', // Path to markdown files to be converted to pages
         templatePath: './src/layouts', // Path to page templates
         template: 'default', // Default template to use if none is supplied
      }
   }
]